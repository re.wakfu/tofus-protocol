package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeTypesItemsExchangerDescriptionForUserMessage(
  objectType: Int,
  itemTypeDescriptions: List[BidExchangerObjectInfo]
) extends Message(5752)

object ExchangeTypesItemsExchangerDescriptionForUserMessage {
  implicit val codec: Codec[ExchangeTypesItemsExchangerDescriptionForUserMessage] =
    new Codec[ExchangeTypesItemsExchangerDescriptionForUserMessage] {
      def decode: Get[ExchangeTypesItemsExchangerDescriptionForUserMessage] =
        for {
          objectType <- int.decode
          itemTypeDescriptions <- list(ushort, Codec[BidExchangerObjectInfo]).decode
        } yield ExchangeTypesItemsExchangerDescriptionForUserMessage(objectType, itemTypeDescriptions)

      def encode(value: ExchangeTypesItemsExchangerDescriptionForUserMessage): ByteVector =
        int.encode(value.objectType) ++
        list(ushort, Codec[BidExchangerObjectInfo]).encode(value.itemTypeDescriptions)
    }
}
