package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MoodSmileyRequestMessage(
  smileyId: Short
) extends Message(6192)

object MoodSmileyRequestMessage {
  implicit val codec: Codec[MoodSmileyRequestMessage] =
    new Codec[MoodSmileyRequestMessage] {
      def decode: Get[MoodSmileyRequestMessage] =
        for {
          smileyId <- varShort.decode
        } yield MoodSmileyRequestMessage(smileyId)

      def encode(value: MoodSmileyRequestMessage): ByteVector =
        varShort.encode(value.smileyId)
    }
}
