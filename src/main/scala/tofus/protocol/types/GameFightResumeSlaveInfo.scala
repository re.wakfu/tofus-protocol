package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightResumeSlaveInfo(
  slaveId: Double,
  spellCooldowns: List[GameFightSpellCooldown],
  summonCount: Byte,
  bombCount: Byte
) extends ProtocolType {
  override val protocolId = 364
}

object GameFightResumeSlaveInfo {
  implicit val codec: Codec[GameFightResumeSlaveInfo] =
    new Codec[GameFightResumeSlaveInfo] {
      def decode: Get[GameFightResumeSlaveInfo] =
        for {
          slaveId <- double.decode
          spellCooldowns <- list(ushort, Codec[GameFightSpellCooldown]).decode
          summonCount <- byte.decode
          bombCount <- byte.decode
        } yield GameFightResumeSlaveInfo(slaveId, spellCooldowns, summonCount, bombCount)

      def encode(value: GameFightResumeSlaveInfo): ByteVector =
        double.encode(value.slaveId) ++
        list(ushort, Codec[GameFightSpellCooldown]).encode(value.spellCooldowns) ++
        byte.encode(value.summonCount) ++
        byte.encode(value.bombCount)
    }
}
