package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryEntryPlayerInfo(
  playerId: Long,
  playerName: String,
  alignmentSide: Byte,
  breed: Byte,
  sex: Boolean,
  isInWorkshop: Boolean,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  status: PlayerStatus
) extends ProtocolType {
  override val protocolId = 194
}

object JobCrafterDirectoryEntryPlayerInfo {
  implicit val codec: Codec[JobCrafterDirectoryEntryPlayerInfo] =
    new Codec[JobCrafterDirectoryEntryPlayerInfo] {
      def decode: Get[JobCrafterDirectoryEntryPlayerInfo] =
        for {
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
          alignmentSide <- byte.decode
          breed <- byte.decode
          sex <- bool.decode
          isInWorkshop <- bool.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          status <- Codec[PlayerStatus].decode
        } yield JobCrafterDirectoryEntryPlayerInfo(playerId, playerName, alignmentSide, breed, sex, isInWorkshop, worldX, worldY, mapId, subAreaId, status)

      def encode(value: JobCrafterDirectoryEntryPlayerInfo): ByteVector =
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName) ++
        byte.encode(value.alignmentSide) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        bool.encode(value.isInWorkshop) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        Codec[PlayerStatus].encode(value.status)
    }
}
