package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameServerInformations(
  flags0: Byte,
  id: Short,
  `type`: Byte,
  status: Byte,
  completion: Byte,
  charactersCount: Byte,
  charactersSlots: Byte,
  date: Double
) extends ProtocolType {
  override val protocolId = 25
}

object GameServerInformations {
  implicit val codec: Codec[GameServerInformations] =
    new Codec[GameServerInformations] {
      def decode: Get[GameServerInformations] =
        for {
          flags0 <- byte.decode
          id <- varShort.decode
          `type` <- byte.decode
          status <- byte.decode
          completion <- byte.decode
          charactersCount <- byte.decode
          charactersSlots <- byte.decode
          date <- double.decode
        } yield GameServerInformations(flags0, id, `type`, status, completion, charactersCount, charactersSlots, date)

      def encode(value: GameServerInformations): ByteVector =
        byte.encode(value.flags0) ++
        varShort.encode(value.id) ++
        byte.encode(value.`type`) ++
        byte.encode(value.status) ++
        byte.encode(value.completion) ++
        byte.encode(value.charactersCount) ++
        byte.encode(value.charactersSlots) ++
        double.encode(value.date)
    }
}
