package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryRemoveMessage(
  jobId: Byte,
  playerId: Long
) extends Message(5653)

object JobCrafterDirectoryRemoveMessage {
  implicit val codec: Codec[JobCrafterDirectoryRemoveMessage] =
    new Codec[JobCrafterDirectoryRemoveMessage] {
      def decode: Get[JobCrafterDirectoryRemoveMessage] =
        for {
          jobId <- byte.decode
          playerId <- varLong.decode
        } yield JobCrafterDirectoryRemoveMessage(jobId, playerId)

      def encode(value: JobCrafterDirectoryRemoveMessage): ByteVector =
        byte.encode(value.jobId) ++
        varLong.encode(value.playerId)
    }
}
