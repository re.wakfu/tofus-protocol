package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidPriceMessage(
  genericId: Short,
  averagePrice: Long
) extends Message(5755)

object ExchangeBidPriceMessage {
  implicit val codec: Codec[ExchangeBidPriceMessage] =
    new Codec[ExchangeBidPriceMessage] {
      def decode: Get[ExchangeBidPriceMessage] =
        for {
          genericId <- varShort.decode
          averagePrice <- varLong.decode
        } yield ExchangeBidPriceMessage(genericId, averagePrice)

      def encode(value: ExchangeBidPriceMessage): ByteVector =
        varShort.encode(value.genericId) ++
        varLong.encode(value.averagePrice)
    }
}
