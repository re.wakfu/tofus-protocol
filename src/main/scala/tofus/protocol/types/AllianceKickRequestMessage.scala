package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceKickRequestMessage(
  kickedId: Int
) extends Message(6400)

object AllianceKickRequestMessage {
  implicit val codec: Codec[AllianceKickRequestMessage] =
    new Codec[AllianceKickRequestMessage] {
      def decode: Get[AllianceKickRequestMessage] =
        for {
          kickedId <- varInt.decode
        } yield AllianceKickRequestMessage(kickedId)

      def encode(value: AllianceKickRequestMessage): ByteVector =
        varInt.encode(value.kickedId)
    }
}
