package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StorageInventoryContentMessage(
  objects: List[ObjectItem],
  kamas: Long
) extends Message(5646)

object StorageInventoryContentMessage {
  implicit val codec: Codec[StorageInventoryContentMessage] =
    new Codec[StorageInventoryContentMessage] {
      def decode: Get[StorageInventoryContentMessage] =
        for {
          objects <- list(ushort, Codec[ObjectItem]).decode
          kamas <- varLong.decode
        } yield StorageInventoryContentMessage(objects, kamas)

      def encode(value: StorageInventoryContentMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.objects) ++
        varLong.encode(value.kamas)
    }
}
