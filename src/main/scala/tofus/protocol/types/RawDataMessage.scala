package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class RawDataMessage(
  content: ByteVector
) extends Message(6253)

object RawDataMessage {
  implicit val codec: Codec[RawDataMessage] =
    new Codec[RawDataMessage] {
      def decode: Get[RawDataMessage] =
        for {
          content <- bytes(varInt).decode
        } yield RawDataMessage(content)

      def encode(value: RawDataMessage): ByteVector =
        bytes(varInt).encode(value.content)
    }
}
