package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntitiesPreset(
  id: Short,
  iconId: Short,
  entityIds: List[Short]
) extends Preset {
  override val protocolId = 545
}

object EntitiesPreset {
  implicit val codec: Codec[EntitiesPreset] =
    new Codec[EntitiesPreset] {
      def decode: Get[EntitiesPreset] =
        for {
          id <- short.decode
          iconId <- short.decode
          entityIds <- list(ushort, varShort).decode
        } yield EntitiesPreset(id, iconId, entityIds)

      def encode(value: EntitiesPreset): ByteVector =
        short.encode(value.id) ++
        short.encode(value.iconId) ++
        list(ushort, varShort).encode(value.entityIds)
    }
}
