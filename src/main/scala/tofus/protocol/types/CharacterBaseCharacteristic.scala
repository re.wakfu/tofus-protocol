package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterBaseCharacteristic(
  base: Short,
  additionnal: Short,
  objectsAndMountBonus: Short,
  alignGiftBonus: Short,
  contextModif: Short
) extends ProtocolType {
  override val protocolId = 4
}

object CharacterBaseCharacteristic {
  implicit val codec: Codec[CharacterBaseCharacteristic] =
    new Codec[CharacterBaseCharacteristic] {
      def decode: Get[CharacterBaseCharacteristic] =
        for {
          base <- varShort.decode
          additionnal <- varShort.decode
          objectsAndMountBonus <- varShort.decode
          alignGiftBonus <- varShort.decode
          contextModif <- varShort.decode
        } yield CharacterBaseCharacteristic(base, additionnal, objectsAndMountBonus, alignGiftBonus, contextModif)

      def encode(value: CharacterBaseCharacteristic): ByteVector =
        varShort.encode(value.base) ++
        varShort.encode(value.additionnal) ++
        varShort.encode(value.objectsAndMountBonus) ++
        varShort.encode(value.alignGiftBonus) ++
        varShort.encode(value.contextModif)
    }
}
