package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightLoot(
  objects: List[Int],
  kamas: Long
) extends ProtocolType {
  override val protocolId = 41
}

object FightLoot {
  implicit val codec: Codec[FightLoot] =
    new Codec[FightLoot] {
      def decode: Get[FightLoot] =
        for {
          objects <- list(ushort, varInt).decode
          kamas <- varLong.decode
        } yield FightLoot(objects, kamas)

      def encode(value: FightLoot): ByteVector =
        list(ushort, varInt).encode(value.objects) ++
        varLong.encode(value.kamas)
    }
}
