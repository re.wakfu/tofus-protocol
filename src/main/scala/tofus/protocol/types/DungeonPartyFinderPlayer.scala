package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderPlayer(
  playerId: Long,
  playerName: String,
  breed: Byte,
  sex: Boolean,
  level: Short
) extends ProtocolType {
  override val protocolId = 373
}

object DungeonPartyFinderPlayer {
  implicit val codec: Codec[DungeonPartyFinderPlayer] =
    new Codec[DungeonPartyFinderPlayer] {
      def decode: Get[DungeonPartyFinderPlayer] =
        for {
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
          level <- varShort.decode
        } yield DungeonPartyFinderPlayer(playerId, playerName, breed, sex, level)

      def encode(value: DungeonPartyFinderPlayer): ByteVector =
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        varShort.encode(value.level)
    }
}
