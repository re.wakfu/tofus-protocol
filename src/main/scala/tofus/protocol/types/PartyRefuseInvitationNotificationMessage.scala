package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyRefuseInvitationNotificationMessage(
  partyId: Int,
  guestId: Long
) extends Message(5596)

object PartyRefuseInvitationNotificationMessage {
  implicit val codec: Codec[PartyRefuseInvitationNotificationMessage] =
    new Codec[PartyRefuseInvitationNotificationMessage] {
      def decode: Get[PartyRefuseInvitationNotificationMessage] =
        for {
          partyId <- varInt.decode
          guestId <- varLong.decode
        } yield PartyRefuseInvitationNotificationMessage(partyId, guestId)

      def encode(value: PartyRefuseInvitationNotificationMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.guestId)
    }
}
