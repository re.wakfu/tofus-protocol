package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountInformationsForPaddock(
  modelId: Short,
  name: String,
  ownerName: String
) extends ProtocolType {
  override val protocolId = 184
}

object MountInformationsForPaddock {
  implicit val codec: Codec[MountInformationsForPaddock] =
    new Codec[MountInformationsForPaddock] {
      def decode: Get[MountInformationsForPaddock] =
        for {
          modelId <- varShort.decode
          name <- utf8(ushort).decode
          ownerName <- utf8(ushort).decode
        } yield MountInformationsForPaddock(modelId, name, ownerName)

      def encode(value: MountInformationsForPaddock): ByteVector =
        varShort.encode(value.modelId) ++
        utf8(ushort).encode(value.name) ++
        utf8(ushort).encode(value.ownerName)
    }
}
