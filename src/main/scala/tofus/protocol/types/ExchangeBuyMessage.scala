package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBuyMessage(
  objectToBuyId: Int,
  quantity: Int
) extends Message(5774)

object ExchangeBuyMessage {
  implicit val codec: Codec[ExchangeBuyMessage] =
    new Codec[ExchangeBuyMessage] {
      def decode: Get[ExchangeBuyMessage] =
        for {
          objectToBuyId <- varInt.decode
          quantity <- varInt.decode
        } yield ExchangeBuyMessage(objectToBuyId, quantity)

      def encode(value: ExchangeBuyMessage): ByteVector =
        varInt.encode(value.objectToBuyId) ++
        varInt.encode(value.quantity)
    }
}
