package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTeamLightInformations(
  teamId: Byte,
  leaderId: Double,
  teamSide: Byte,
  teamTypeId: Byte,
  nbWaves: Byte,
  flags0: Byte,
  teamMembersCount: Byte,
  meanLevel: Int
) extends AbstractFightTeamInformations {
  override val protocolId = 115
}

object FightTeamLightInformations {
  implicit val codec: Codec[FightTeamLightInformations] =
    new Codec[FightTeamLightInformations] {
      def decode: Get[FightTeamLightInformations] =
        for {
          teamId <- byte.decode
          leaderId <- double.decode
          teamSide <- byte.decode
          teamTypeId <- byte.decode
          nbWaves <- byte.decode
          flags0 <- byte.decode
          teamMembersCount <- byte.decode
          meanLevel <- varInt.decode
        } yield FightTeamLightInformations(teamId, leaderId, teamSide, teamTypeId, nbWaves, flags0, teamMembersCount, meanLevel)

      def encode(value: FightTeamLightInformations): ByteVector =
        byte.encode(value.teamId) ++
        double.encode(value.leaderId) ++
        byte.encode(value.teamSide) ++
        byte.encode(value.teamTypeId) ++
        byte.encode(value.nbWaves) ++
        byte.encode(value.flags0) ++
        byte.encode(value.teamMembersCount) ++
        varInt.encode(value.meanLevel)
    }
}
