package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCharacteristicForPreset(
  keyword: String,
  base: Short,
  additionnal: Short,
  stuff: Short
) extends SimpleCharacterCharacteristicForPreset {
  override val protocolId = 539
}

object CharacterCharacteristicForPreset {
  implicit val codec: Codec[CharacterCharacteristicForPreset] =
    new Codec[CharacterCharacteristicForPreset] {
      def decode: Get[CharacterCharacteristicForPreset] =
        for {
          keyword <- utf8(ushort).decode
          base <- varShort.decode
          additionnal <- varShort.decode
          stuff <- varShort.decode
        } yield CharacterCharacteristicForPreset(keyword, base, additionnal, stuff)

      def encode(value: CharacterCharacteristicForPreset): ByteVector =
        utf8(ushort).encode(value.keyword) ++
        varShort.encode(value.base) ++
        varShort.encode(value.additionnal) ++
        varShort.encode(value.stuff)
    }
}
