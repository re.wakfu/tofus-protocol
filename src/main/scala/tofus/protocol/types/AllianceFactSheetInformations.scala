package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceFactSheetInformations(
  allianceId: Int,
  allianceTag: String,
  allianceName: String,
  allianceEmblem: GuildEmblem,
  creationDate: Int
) extends AllianceInformations {
  override val protocolId = 421
}

object AllianceFactSheetInformations {
  implicit val codec: Codec[AllianceFactSheetInformations] =
    new Codec[AllianceFactSheetInformations] {
      def decode: Get[AllianceFactSheetInformations] =
        for {
          allianceId <- varInt.decode
          allianceTag <- utf8(ushort).decode
          allianceName <- utf8(ushort).decode
          allianceEmblem <- Codec[GuildEmblem].decode
          creationDate <- int.decode
        } yield AllianceFactSheetInformations(allianceId, allianceTag, allianceName, allianceEmblem, creationDate)

      def encode(value: AllianceFactSheetInformations): ByteVector =
        varInt.encode(value.allianceId) ++
        utf8(ushort).encode(value.allianceTag) ++
        utf8(ushort).encode(value.allianceName) ++
        Codec[GuildEmblem].encode(value.allianceEmblem) ++
        int.encode(value.creationDate)
    }
}
