package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EditHavenBagCancelRequestMessage(

) extends Message(6619)

object EditHavenBagCancelRequestMessage {
  implicit val codec: Codec[EditHavenBagCancelRequestMessage] =
    Codec.const(EditHavenBagCancelRequestMessage())
}
