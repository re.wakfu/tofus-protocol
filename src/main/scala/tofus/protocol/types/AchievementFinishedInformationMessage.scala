package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementFinishedInformationMessage(
  achievement: AchievementAchievedRewardable,
  name: String,
  playerId: Long
) extends Message(6381)

object AchievementFinishedInformationMessage {
  implicit val codec: Codec[AchievementFinishedInformationMessage] =
    new Codec[AchievementFinishedInformationMessage] {
      def decode: Get[AchievementFinishedInformationMessage] =
        for {
          achievement <- Codec[AchievementAchievedRewardable].decode
          name <- utf8(ushort).decode
          playerId <- varLong.decode
        } yield AchievementFinishedInformationMessage(achievement, name, playerId)

      def encode(value: AchievementFinishedInformationMessage): ByteVector =
        Codec[AchievementAchievedRewardable].encode(value.achievement) ++
        utf8(ushort).encode(value.name) ++
        varLong.encode(value.playerId)
    }
}
