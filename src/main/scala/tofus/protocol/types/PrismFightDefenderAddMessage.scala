package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightDefenderAddMessage(
  subAreaId: Short,
  fightId: Short,
  defender: CharacterMinimalPlusLookInformations
) extends Message(5895)

object PrismFightDefenderAddMessage {
  implicit val codec: Codec[PrismFightDefenderAddMessage] =
    new Codec[PrismFightDefenderAddMessage] {
      def decode: Get[PrismFightDefenderAddMessage] =
        for {
          subAreaId <- varShort.decode
          fightId <- varShort.decode
          defender <- Codec[CharacterMinimalPlusLookInformations].decode
        } yield PrismFightDefenderAddMessage(subAreaId, fightId, defender)

      def encode(value: PrismFightDefenderAddMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        varShort.encode(value.fightId) ++
        Codec[CharacterMinimalPlusLookInformations].encode(value.defender)
    }
}
