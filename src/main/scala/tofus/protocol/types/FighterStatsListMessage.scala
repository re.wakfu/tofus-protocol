package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FighterStatsListMessage(
  stats: CharacterCharacteristicsInformations
) extends Message(6322)

object FighterStatsListMessage {
  implicit val codec: Codec[FighterStatsListMessage] =
    new Codec[FighterStatsListMessage] {
      def decode: Get[FighterStatsListMessage] =
        for {
          stats <- Codec[CharacterCharacteristicsInformations].decode
        } yield FighterStatsListMessage(stats)

      def encode(value: FighterStatsListMessage): ByteVector =
        Codec[CharacterCharacteristicsInformations].encode(value.stats)
    }
}
