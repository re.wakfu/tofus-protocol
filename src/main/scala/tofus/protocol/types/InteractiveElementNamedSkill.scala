package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveElementNamedSkill(
  skillId: Int,
  skillInstanceUid: Int,
  nameId: Int
) extends InteractiveElementSkill {
  override val protocolId = 220
}

object InteractiveElementNamedSkill {
  implicit val codec: Codec[InteractiveElementNamedSkill] =
    new Codec[InteractiveElementNamedSkill] {
      def decode: Get[InteractiveElementNamedSkill] =
        for {
          skillId <- varInt.decode
          skillInstanceUid <- int.decode
          nameId <- varInt.decode
        } yield InteractiveElementNamedSkill(skillId, skillInstanceUid, nameId)

      def encode(value: InteractiveElementNamedSkill): ByteVector =
        varInt.encode(value.skillId) ++
        int.encode(value.skillInstanceUid) ++
        varInt.encode(value.nameId)
    }
}
