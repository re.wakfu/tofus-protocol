package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameRolePlayGroupMonsterInformations extends GameRolePlayActorInformations

final case class ConcreteGameRolePlayGroupMonsterInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  flags0: Byte,
  staticInfos: GroupMonsterStaticInformations,
  lootShare: Byte,
  alignmentSide: Byte
) extends GameRolePlayGroupMonsterInformations {
  override val protocolId = 160
}

object ConcreteGameRolePlayGroupMonsterInformations {
  implicit val codec: Codec[ConcreteGameRolePlayGroupMonsterInformations] =  
    new Codec[ConcreteGameRolePlayGroupMonsterInformations] {
      def decode: Get[ConcreteGameRolePlayGroupMonsterInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          flags0 <- byte.decode
          staticInfos <- Codec[GroupMonsterStaticInformations].decode
          lootShare <- byte.decode
          alignmentSide <- byte.decode
        } yield ConcreteGameRolePlayGroupMonsterInformations(contextualId, disposition, look, flags0, staticInfos, lootShare, alignmentSide)

      def encode(value: ConcreteGameRolePlayGroupMonsterInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        byte.encode(value.flags0) ++
        Codec[GroupMonsterStaticInformations].encode(value.staticInfos) ++
        byte.encode(value.lootShare) ++
        byte.encode(value.alignmentSide)
    }
}

object GameRolePlayGroupMonsterInformations {
  implicit val codec: Codec[GameRolePlayGroupMonsterInformations] =
    new Codec[GameRolePlayGroupMonsterInformations] {
      def decode: Get[GameRolePlayGroupMonsterInformations] =
        ushort.decode.flatMap {
          case 160 => Codec[ConcreteGameRolePlayGroupMonsterInformations].decode
          case 464 => Codec[GameRolePlayGroupMonsterWaveInformations].decode
        }

      def encode(value: GameRolePlayGroupMonsterInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameRolePlayGroupMonsterInformations => Codec[ConcreteGameRolePlayGroupMonsterInformations].encode(i)
          case i: GameRolePlayGroupMonsterWaveInformations => Codec[GameRolePlayGroupMonsterWaveInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
