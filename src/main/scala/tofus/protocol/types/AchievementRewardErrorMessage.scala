package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AchievementRewardErrorMessage(
  achievementId: Short
) extends Message(6375)

object AchievementRewardErrorMessage {
  implicit val codec: Codec[AchievementRewardErrorMessage] =
    new Codec[AchievementRewardErrorMessage] {
      def decode: Get[AchievementRewardErrorMessage] =
        for {
          achievementId <- short.decode
        } yield AchievementRewardErrorMessage(achievementId)

      def encode(value: AchievementRewardErrorMessage): ByteVector =
        short.encode(value.achievementId)
    }
}
