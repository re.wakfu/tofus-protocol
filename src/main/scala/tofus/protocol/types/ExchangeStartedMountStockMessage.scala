package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedMountStockMessage(
  objectsInfos: List[ObjectItem]
) extends Message(5984)

object ExchangeStartedMountStockMessage {
  implicit val codec: Codec[ExchangeStartedMountStockMessage] =
    new Codec[ExchangeStartedMountStockMessage] {
      def decode: Get[ExchangeStartedMountStockMessage] =
        for {
          objectsInfos <- list(ushort, Codec[ObjectItem]).decode
        } yield ExchangeStartedMountStockMessage(objectsInfos)

      def encode(value: ExchangeStartedMountStockMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.objectsInfos)
    }
}
