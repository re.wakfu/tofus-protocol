package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NicknameAcceptedMessage(

) extends Message(5641)

object NicknameAcceptedMessage {
  implicit val codec: Codec[NicknameAcceptedMessage] =
    Codec.const(NicknameAcceptedMessage())
}
