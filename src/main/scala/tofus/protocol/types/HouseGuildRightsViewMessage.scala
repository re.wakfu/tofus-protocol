package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseGuildRightsViewMessage(
  houseId: Int,
  instanceId: Int
) extends Message(5700)

object HouseGuildRightsViewMessage {
  implicit val codec: Codec[HouseGuildRightsViewMessage] =
    new Codec[HouseGuildRightsViewMessage] {
      def decode: Get[HouseGuildRightsViewMessage] =
        for {
          houseId <- varInt.decode
          instanceId <- int.decode
        } yield HouseGuildRightsViewMessage(houseId, instanceId)

      def encode(value: HouseGuildRightsViewMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.instanceId)
    }
}
