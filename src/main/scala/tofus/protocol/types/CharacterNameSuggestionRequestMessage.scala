package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterNameSuggestionRequestMessage(

) extends Message(162)

object CharacterNameSuggestionRequestMessage {
  implicit val codec: Codec[CharacterNameSuggestionRequestMessage] =
    Codec.const(CharacterNameSuggestionRequestMessage())
}
