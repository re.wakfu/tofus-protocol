package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobLevelUpMessage(
  newLevel: Short,
  jobsDescription: JobDescription
) extends Message(5656)

object JobLevelUpMessage {
  implicit val codec: Codec[JobLevelUpMessage] =
    new Codec[JobLevelUpMessage] {
      def decode: Get[JobLevelUpMessage] =
        for {
          newLevel <- ubyte.decode
          jobsDescription <- Codec[JobDescription].decode
        } yield JobLevelUpMessage(newLevel, jobsDescription)

      def encode(value: JobLevelUpMessage): ByteVector =
        ubyte.encode(value.newLevel) ++
        Codec[JobDescription].encode(value.jobsDescription)
    }
}
