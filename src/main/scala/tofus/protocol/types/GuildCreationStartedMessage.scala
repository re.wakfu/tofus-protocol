package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildCreationStartedMessage(

) extends Message(5920)

object GuildCreationStartedMessage {
  implicit val codec: Codec[GuildCreationStartedMessage] =
    Codec.const(GuildCreationStartedMessage())
}
