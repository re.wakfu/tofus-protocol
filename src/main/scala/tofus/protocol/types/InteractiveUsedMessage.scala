package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveUsedMessage(
  entityId: Long,
  elemId: Int,
  skillId: Short,
  duration: Short,
  canMove: Boolean
) extends Message(5745)

object InteractiveUsedMessage {
  implicit val codec: Codec[InteractiveUsedMessage] =
    new Codec[InteractiveUsedMessage] {
      def decode: Get[InteractiveUsedMessage] =
        for {
          entityId <- varLong.decode
          elemId <- varInt.decode
          skillId <- varShort.decode
          duration <- varShort.decode
          canMove <- bool.decode
        } yield InteractiveUsedMessage(entityId, elemId, skillId, duration, canMove)

      def encode(value: InteractiveUsedMessage): ByteVector =
        varLong.encode(value.entityId) ++
        varInt.encode(value.elemId) ++
        varShort.encode(value.skillId) ++
        varShort.encode(value.duration) ++
        bool.encode(value.canMove)
    }
}
