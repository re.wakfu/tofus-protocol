package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectDuration(
  actionId: Short,
  days: Short,
  hours: Byte,
  minutes: Byte
) extends ObjectEffect {
  override val protocolId = 75
}

object ObjectEffectDuration {
  implicit val codec: Codec[ObjectEffectDuration] =
    new Codec[ObjectEffectDuration] {
      def decode: Get[ObjectEffectDuration] =
        for {
          actionId <- varShort.decode
          days <- varShort.decode
          hours <- byte.decode
          minutes <- byte.decode
        } yield ObjectEffectDuration(actionId, days, hours, minutes)

      def encode(value: ObjectEffectDuration): ByteVector =
        varShort.encode(value.actionId) ++
        varShort.encode(value.days) ++
        byte.encode(value.hours) ++
        byte.encode(value.minutes)
    }
}
