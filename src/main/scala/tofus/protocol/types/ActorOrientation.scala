package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ActorOrientation(
  id: Double,
  direction: Byte
) extends ProtocolType {
  override val protocolId = 353
}

object ActorOrientation {
  implicit val codec: Codec[ActorOrientation] =
    new Codec[ActorOrientation] {
      def decode: Get[ActorOrientation] =
        for {
          id <- double.decode
          direction <- byte.decode
        } yield ActorOrientation(id, direction)

      def encode(value: ActorOrientation): ByteVector =
        double.encode(value.id) ++
        byte.encode(value.direction)
    }
}
