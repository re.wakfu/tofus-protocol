package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportHavenBagRequestMessage(
  guestId: Long
) extends Message(6647)

object TeleportHavenBagRequestMessage {
  implicit val codec: Codec[TeleportHavenBagRequestMessage] =
    new Codec[TeleportHavenBagRequestMessage] {
      def decode: Get[TeleportHavenBagRequestMessage] =
        for {
          guestId <- varLong.decode
        } yield TeleportHavenBagRequestMessage(guestId)

      def encode(value: TeleportHavenBagRequestMessage): ByteVector =
        varLong.encode(value.guestId)
    }
}
