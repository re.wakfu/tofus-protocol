package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFactsRequestMessage(
  guildId: Int
) extends Message(6404)

object GuildFactsRequestMessage {
  implicit val codec: Codec[GuildFactsRequestMessage] =
    new Codec[GuildFactsRequestMessage] {
      def decode: Get[GuildFactsRequestMessage] =
        for {
          guildId <- varInt.decode
        } yield GuildFactsRequestMessage(guildId)

      def encode(value: GuildFactsRequestMessage): ByteVector =
        varInt.encode(value.guildId)
    }
}
