package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpellsPreset(
  id: Short,
  spells: List[SpellForPreset]
) extends Preset {
  override val protocolId = 519
}

object SpellsPreset {
  implicit val codec: Codec[SpellsPreset] =
    new Codec[SpellsPreset] {
      def decode: Get[SpellsPreset] =
        for {
          id <- short.decode
          spells <- list(ushort, Codec[SpellForPreset]).decode
        } yield SpellsPreset(id, spells)

      def encode(value: SpellsPreset): ByteVector =
        short.encode(value.id) ++
        list(ushort, Codec[SpellForPreset]).encode(value.spells)
    }
}
