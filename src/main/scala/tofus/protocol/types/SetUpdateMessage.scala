package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SetUpdateMessage(
  setId: Short,
  setObjects: List[Short],
  setEffects: List[ObjectEffect]
) extends Message(5503)

object SetUpdateMessage {
  implicit val codec: Codec[SetUpdateMessage] =
    new Codec[SetUpdateMessage] {
      def decode: Get[SetUpdateMessage] =
        for {
          setId <- varShort.decode
          setObjects <- list(ushort, varShort).decode
          setEffects <- list(ushort, Codec[ObjectEffect]).decode
        } yield SetUpdateMessage(setId, setObjects, setEffects)

      def encode(value: SetUpdateMessage): ByteVector =
        varShort.encode(value.setId) ++
        list(ushort, varShort).encode(value.setObjects) ++
        list(ushort, Codec[ObjectEffect]).encode(value.setEffects)
    }
}
