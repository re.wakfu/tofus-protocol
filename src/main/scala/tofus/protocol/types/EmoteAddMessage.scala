package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmoteAddMessage(
  emoteId: Short
) extends Message(5644)

object EmoteAddMessage {
  implicit val codec: Codec[EmoteAddMessage] =
    new Codec[EmoteAddMessage] {
      def decode: Get[EmoteAddMessage] =
        for {
          emoteId <- ubyte.decode
        } yield EmoteAddMessage(emoteId)

      def encode(value: EmoteAddMessage): ByteVector =
        ubyte.encode(value.emoteId)
    }
}
