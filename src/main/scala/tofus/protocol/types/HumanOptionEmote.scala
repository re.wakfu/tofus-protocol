package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionEmote(
  emoteId: Short,
  emoteStartTime: Double
) extends HumanOption {
  override val protocolId = 407
}

object HumanOptionEmote {
  implicit val codec: Codec[HumanOptionEmote] =
    new Codec[HumanOptionEmote] {
      def decode: Get[HumanOptionEmote] =
        for {
          emoteId <- ubyte.decode
          emoteStartTime <- double.decode
        } yield HumanOptionEmote(emoteId, emoteStartTime)

      def encode(value: HumanOptionEmote): ByteVector =
        ubyte.encode(value.emoteId) ++
        double.encode(value.emoteStartTime)
    }
}
