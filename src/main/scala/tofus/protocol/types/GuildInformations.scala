package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GuildInformations extends BasicGuildInformations

final case class ConcreteGuildInformations(
  guildId: Int,
  guildName: String,
  guildLevel: Short,
  guildEmblem: GuildEmblem
) extends GuildInformations {
  override val protocolId = 127
}

object ConcreteGuildInformations {
  implicit val codec: Codec[ConcreteGuildInformations] =  
    new Codec[ConcreteGuildInformations] {
      def decode: Get[ConcreteGuildInformations] =
        for {
          guildId <- varInt.decode
          guildName <- utf8(ushort).decode
          guildLevel <- ubyte.decode
          guildEmblem <- Codec[GuildEmblem].decode
        } yield ConcreteGuildInformations(guildId, guildName, guildLevel, guildEmblem)

      def encode(value: ConcreteGuildInformations): ByteVector =
        varInt.encode(value.guildId) ++
        utf8(ushort).encode(value.guildName) ++
        ubyte.encode(value.guildLevel) ++
        Codec[GuildEmblem].encode(value.guildEmblem)
    }
}

object GuildInformations {
  implicit val codec: Codec[GuildInformations] =
    new Codec[GuildInformations] {
      def decode: Get[GuildInformations] =
        ushort.decode.flatMap {
          case 127 => Codec[ConcreteGuildInformations].decode
          case 423 => Codec[GuildInsiderFactSheetInformations].decode
          case 424 => Codec[ConcreteGuildFactSheetInformations].decode
          case 420 => Codec[GuildInAllianceInformations].decode
          case 422 => Codec[AlliancedGuildFactSheetInformations].decode
        }

      def encode(value: GuildInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGuildInformations => Codec[ConcreteGuildInformations].encode(i)
          case i: GuildInsiderFactSheetInformations => Codec[GuildInsiderFactSheetInformations].encode(i)
          case i: ConcreteGuildFactSheetInformations => Codec[ConcreteGuildFactSheetInformations].encode(i)
          case i: GuildInAllianceInformations => Codec[GuildInAllianceInformations].encode(i)
          case i: AlliancedGuildFactSheetInformations => Codec[AlliancedGuildFactSheetInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
