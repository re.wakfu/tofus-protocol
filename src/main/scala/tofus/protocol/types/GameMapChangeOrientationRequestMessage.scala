package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapChangeOrientationRequestMessage(
  direction: Byte
) extends Message(945)

object GameMapChangeOrientationRequestMessage {
  implicit val codec: Codec[GameMapChangeOrientationRequestMessage] =
    new Codec[GameMapChangeOrientationRequestMessage] {
      def decode: Get[GameMapChangeOrientationRequestMessage] =
        for {
          direction <- byte.decode
        } yield GameMapChangeOrientationRequestMessage(direction)

      def encode(value: GameMapChangeOrientationRequestMessage): ByteVector =
        byte.encode(value.direction)
    }
}
