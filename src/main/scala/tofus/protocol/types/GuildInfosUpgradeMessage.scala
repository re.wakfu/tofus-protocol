package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInfosUpgradeMessage(
  maxTaxCollectorsCount: Byte,
  taxCollectorsCount: Byte,
  taxCollectorLifePoints: Short,
  taxCollectorDamagesBonuses: Short,
  taxCollectorPods: Short,
  taxCollectorProspecting: Short,
  taxCollectorWisdom: Short,
  boostPoints: Short,
  spellId: List[Short],
  spellLevel: List[Short]
) extends Message(5636)

object GuildInfosUpgradeMessage {
  implicit val codec: Codec[GuildInfosUpgradeMessage] =
    new Codec[GuildInfosUpgradeMessage] {
      def decode: Get[GuildInfosUpgradeMessage] =
        for {
          maxTaxCollectorsCount <- byte.decode
          taxCollectorsCount <- byte.decode
          taxCollectorLifePoints <- varShort.decode
          taxCollectorDamagesBonuses <- varShort.decode
          taxCollectorPods <- varShort.decode
          taxCollectorProspecting <- varShort.decode
          taxCollectorWisdom <- varShort.decode
          boostPoints <- varShort.decode
          spellId <- list(ushort, varShort).decode
          spellLevel <- list(ushort, short).decode
        } yield GuildInfosUpgradeMessage(maxTaxCollectorsCount, taxCollectorsCount, taxCollectorLifePoints, taxCollectorDamagesBonuses, taxCollectorPods, taxCollectorProspecting, taxCollectorWisdom, boostPoints, spellId, spellLevel)

      def encode(value: GuildInfosUpgradeMessage): ByteVector =
        byte.encode(value.maxTaxCollectorsCount) ++
        byte.encode(value.taxCollectorsCount) ++
        varShort.encode(value.taxCollectorLifePoints) ++
        varShort.encode(value.taxCollectorDamagesBonuses) ++
        varShort.encode(value.taxCollectorPods) ++
        varShort.encode(value.taxCollectorProspecting) ++
        varShort.encode(value.taxCollectorWisdom) ++
        varShort.encode(value.boostPoints) ++
        list(ushort, varShort).encode(value.spellId) ++
        list(ushort, short).encode(value.spellLevel)
    }
}
