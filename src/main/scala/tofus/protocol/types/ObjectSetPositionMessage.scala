package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectSetPositionMessage(
  objectUID: Int,
  position: Short,
  quantity: Int
) extends Message(3021)

object ObjectSetPositionMessage {
  implicit val codec: Codec[ObjectSetPositionMessage] =
    new Codec[ObjectSetPositionMessage] {
      def decode: Get[ObjectSetPositionMessage] =
        for {
          objectUID <- varInt.decode
          position <- short.decode
          quantity <- varInt.decode
        } yield ObjectSetPositionMessage(objectUID, position, quantity)

      def encode(value: ObjectSetPositionMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        short.encode(value.position) ++
        varInt.encode(value.quantity)
    }
}
