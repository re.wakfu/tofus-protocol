package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStoppedMessage(
  id: Long
) extends Message(6589)

object ExchangeStoppedMessage {
  implicit val codec: Codec[ExchangeStoppedMessage] =
    new Codec[ExchangeStoppedMessage] {
      def decode: Get[ExchangeStoppedMessage] =
        for {
          id <- varLong.decode
        } yield ExchangeStoppedMessage(id)

      def encode(value: ExchangeStoppedMessage): ByteVector =
        varLong.encode(value.id)
    }
}
