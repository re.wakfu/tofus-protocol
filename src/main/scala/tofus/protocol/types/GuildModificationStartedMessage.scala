package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildModificationStartedMessage(
  flags0: Byte
) extends Message(6324)

object GuildModificationStartedMessage {
  implicit val codec: Codec[GuildModificationStartedMessage] =
    new Codec[GuildModificationStartedMessage] {
      def decode: Get[GuildModificationStartedMessage] =
        for {
          flags0 <- byte.decode
        } yield GuildModificationStartedMessage(flags0)

      def encode(value: GuildModificationStartedMessage): ByteVector =
        byte.encode(value.flags0)
    }
}
