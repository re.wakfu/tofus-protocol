package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiBuyValidationMessage(
  action: Byte,
  code: Short,
  amount: Long,
  email: String
) extends Message(6841)

object HaapiBuyValidationMessage {
  implicit val codec: Codec[HaapiBuyValidationMessage] =
    new Codec[HaapiBuyValidationMessage] {
      def decode: Get[HaapiBuyValidationMessage] =
        for {
          action <- byte.decode
          code <- varShort.decode
          amount <- varLong.decode
          email <- utf8(ushort).decode
        } yield HaapiBuyValidationMessage(action, code, amount, email)

      def encode(value: HaapiBuyValidationMessage): ByteVector =
        byte.encode(value.action) ++
        varShort.encode(value.code) ++
        varLong.encode(value.amount) ++
        utf8(ushort).encode(value.email)
    }
}
