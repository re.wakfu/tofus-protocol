package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismSettingsRequestMessage(
  subAreaId: Short,
  startDefenseTime: Byte
) extends Message(6437)

object PrismSettingsRequestMessage {
  implicit val codec: Codec[PrismSettingsRequestMessage] =
    new Codec[PrismSettingsRequestMessage] {
      def decode: Get[PrismSettingsRequestMessage] =
        for {
          subAreaId <- varShort.decode
          startDefenseTime <- byte.decode
        } yield PrismSettingsRequestMessage(subAreaId, startDefenseTime)

      def encode(value: PrismSettingsRequestMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        byte.encode(value.startDefenseTime)
    }
}
