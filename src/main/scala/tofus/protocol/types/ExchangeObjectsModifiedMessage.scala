package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectsModifiedMessage(
  remote: Boolean,
  `object`: List[ObjectItem]
) extends Message(6533)

object ExchangeObjectsModifiedMessage {
  implicit val codec: Codec[ExchangeObjectsModifiedMessage] =
    new Codec[ExchangeObjectsModifiedMessage] {
      def decode: Get[ExchangeObjectsModifiedMessage] =
        for {
          remote <- bool.decode
          `object` <- list(ushort, Codec[ObjectItem]).decode
        } yield ExchangeObjectsModifiedMessage(remote, `object`)

      def encode(value: ExchangeObjectsModifiedMessage): ByteVector =
        bool.encode(value.remote) ++
        list(ushort, Codec[ObjectItem]).encode(value.`object`)
    }
}
