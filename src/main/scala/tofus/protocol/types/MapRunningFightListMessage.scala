package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapRunningFightListMessage(
  fights: List[FightExternalInformations]
) extends Message(5743)

object MapRunningFightListMessage {
  implicit val codec: Codec[MapRunningFightListMessage] =
    new Codec[MapRunningFightListMessage] {
      def decode: Get[MapRunningFightListMessage] =
        for {
          fights <- list(ushort, Codec[FightExternalInformations]).decode
        } yield MapRunningFightListMessage(fights)

      def encode(value: MapRunningFightListMessage): ByteVector =
        list(ushort, Codec[FightExternalInformations]).encode(value.fights)
    }
}
