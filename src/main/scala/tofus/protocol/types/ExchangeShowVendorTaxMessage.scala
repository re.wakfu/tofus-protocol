package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeShowVendorTaxMessage(

) extends Message(5783)

object ExchangeShowVendorTaxMessage {
  implicit val codec: Codec[ExchangeShowVendorTaxMessage] =
    Codec.const(ExchangeShowVendorTaxMessage())
}
