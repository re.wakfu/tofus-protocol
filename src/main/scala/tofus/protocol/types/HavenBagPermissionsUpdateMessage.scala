package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagPermissionsUpdateMessage(
  permissions: Int
) extends Message(6713)

object HavenBagPermissionsUpdateMessage {
  implicit val codec: Codec[HavenBagPermissionsUpdateMessage] =
    new Codec[HavenBagPermissionsUpdateMessage] {
      def decode: Get[HavenBagPermissionsUpdateMessage] =
        for {
          permissions <- int.decode
        } yield HavenBagPermissionsUpdateMessage(permissions)

      def encode(value: HavenBagPermissionsUpdateMessage): ByteVector =
        int.encode(value.permissions)
    }
}
