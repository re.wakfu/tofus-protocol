package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightMinimalStatsPreparation(
  lifePoints: Int,
  maxLifePoints: Int,
  baseMaxLifePoints: Int,
  permanentDamagePercent: Int,
  shieldPoints: Int,
  actionPoints: Short,
  maxActionPoints: Short,
  movementPoints: Short,
  maxMovementPoints: Short,
  summoner: Double,
  summoned: Boolean,
  neutralElementResistPercent: Short,
  earthElementResistPercent: Short,
  waterElementResistPercent: Short,
  airElementResistPercent: Short,
  fireElementResistPercent: Short,
  neutralElementReduction: Short,
  earthElementReduction: Short,
  waterElementReduction: Short,
  airElementReduction: Short,
  fireElementReduction: Short,
  criticalDamageFixedResist: Short,
  pushDamageFixedResist: Short,
  pvpNeutralElementResistPercent: Short,
  pvpEarthElementResistPercent: Short,
  pvpWaterElementResistPercent: Short,
  pvpAirElementResistPercent: Short,
  pvpFireElementResistPercent: Short,
  pvpNeutralElementReduction: Short,
  pvpEarthElementReduction: Short,
  pvpWaterElementReduction: Short,
  pvpAirElementReduction: Short,
  pvpFireElementReduction: Short,
  dodgePALostProbability: Short,
  dodgePMLostProbability: Short,
  tackleBlock: Short,
  tackleEvade: Short,
  fixedDamageReflection: Short,
  invisibilityState: Byte,
  meleeDamageReceivedPercent: Short,
  rangedDamageReceivedPercent: Short,
  weaponDamageReceivedPercent: Short,
  spellDamageReceivedPercent: Short,
  initiative: Int
) extends GameFightMinimalStats {
  override val protocolId = 360
}

object GameFightMinimalStatsPreparation {
  implicit val codec: Codec[GameFightMinimalStatsPreparation] =
    new Codec[GameFightMinimalStatsPreparation] {
      def decode: Get[GameFightMinimalStatsPreparation] =
        for {
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          baseMaxLifePoints <- varInt.decode
          permanentDamagePercent <- varInt.decode
          shieldPoints <- varInt.decode
          actionPoints <- varShort.decode
          maxActionPoints <- varShort.decode
          movementPoints <- varShort.decode
          maxMovementPoints <- varShort.decode
          summoner <- double.decode
          summoned <- bool.decode
          neutralElementResistPercent <- varShort.decode
          earthElementResistPercent <- varShort.decode
          waterElementResistPercent <- varShort.decode
          airElementResistPercent <- varShort.decode
          fireElementResistPercent <- varShort.decode
          neutralElementReduction <- varShort.decode
          earthElementReduction <- varShort.decode
          waterElementReduction <- varShort.decode
          airElementReduction <- varShort.decode
          fireElementReduction <- varShort.decode
          criticalDamageFixedResist <- varShort.decode
          pushDamageFixedResist <- varShort.decode
          pvpNeutralElementResistPercent <- varShort.decode
          pvpEarthElementResistPercent <- varShort.decode
          pvpWaterElementResistPercent <- varShort.decode
          pvpAirElementResistPercent <- varShort.decode
          pvpFireElementResistPercent <- varShort.decode
          pvpNeutralElementReduction <- varShort.decode
          pvpEarthElementReduction <- varShort.decode
          pvpWaterElementReduction <- varShort.decode
          pvpAirElementReduction <- varShort.decode
          pvpFireElementReduction <- varShort.decode
          dodgePALostProbability <- varShort.decode
          dodgePMLostProbability <- varShort.decode
          tackleBlock <- varShort.decode
          tackleEvade <- varShort.decode
          fixedDamageReflection <- varShort.decode
          invisibilityState <- byte.decode
          meleeDamageReceivedPercent <- varShort.decode
          rangedDamageReceivedPercent <- varShort.decode
          weaponDamageReceivedPercent <- varShort.decode
          spellDamageReceivedPercent <- varShort.decode
          initiative <- varInt.decode
        } yield GameFightMinimalStatsPreparation(lifePoints, maxLifePoints, baseMaxLifePoints, permanentDamagePercent, shieldPoints, actionPoints, maxActionPoints, movementPoints, maxMovementPoints, summoner, summoned, neutralElementResistPercent, earthElementResistPercent, waterElementResistPercent, airElementResistPercent, fireElementResistPercent, neutralElementReduction, earthElementReduction, waterElementReduction, airElementReduction, fireElementReduction, criticalDamageFixedResist, pushDamageFixedResist, pvpNeutralElementResistPercent, pvpEarthElementResistPercent, pvpWaterElementResistPercent, pvpAirElementResistPercent, pvpFireElementResistPercent, pvpNeutralElementReduction, pvpEarthElementReduction, pvpWaterElementReduction, pvpAirElementReduction, pvpFireElementReduction, dodgePALostProbability, dodgePMLostProbability, tackleBlock, tackleEvade, fixedDamageReflection, invisibilityState, meleeDamageReceivedPercent, rangedDamageReceivedPercent, weaponDamageReceivedPercent, spellDamageReceivedPercent, initiative)

      def encode(value: GameFightMinimalStatsPreparation): ByteVector =
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varInt.encode(value.baseMaxLifePoints) ++
        varInt.encode(value.permanentDamagePercent) ++
        varInt.encode(value.shieldPoints) ++
        varShort.encode(value.actionPoints) ++
        varShort.encode(value.maxActionPoints) ++
        varShort.encode(value.movementPoints) ++
        varShort.encode(value.maxMovementPoints) ++
        double.encode(value.summoner) ++
        bool.encode(value.summoned) ++
        varShort.encode(value.neutralElementResistPercent) ++
        varShort.encode(value.earthElementResistPercent) ++
        varShort.encode(value.waterElementResistPercent) ++
        varShort.encode(value.airElementResistPercent) ++
        varShort.encode(value.fireElementResistPercent) ++
        varShort.encode(value.neutralElementReduction) ++
        varShort.encode(value.earthElementReduction) ++
        varShort.encode(value.waterElementReduction) ++
        varShort.encode(value.airElementReduction) ++
        varShort.encode(value.fireElementReduction) ++
        varShort.encode(value.criticalDamageFixedResist) ++
        varShort.encode(value.pushDamageFixedResist) ++
        varShort.encode(value.pvpNeutralElementResistPercent) ++
        varShort.encode(value.pvpEarthElementResistPercent) ++
        varShort.encode(value.pvpWaterElementResistPercent) ++
        varShort.encode(value.pvpAirElementResistPercent) ++
        varShort.encode(value.pvpFireElementResistPercent) ++
        varShort.encode(value.pvpNeutralElementReduction) ++
        varShort.encode(value.pvpEarthElementReduction) ++
        varShort.encode(value.pvpWaterElementReduction) ++
        varShort.encode(value.pvpAirElementReduction) ++
        varShort.encode(value.pvpFireElementReduction) ++
        varShort.encode(value.dodgePALostProbability) ++
        varShort.encode(value.dodgePMLostProbability) ++
        varShort.encode(value.tackleBlock) ++
        varShort.encode(value.tackleEvade) ++
        varShort.encode(value.fixedDamageReflection) ++
        byte.encode(value.invisibilityState) ++
        varShort.encode(value.meleeDamageReceivedPercent) ++
        varShort.encode(value.rangedDamageReceivedPercent) ++
        varShort.encode(value.weaponDamageReceivedPercent) ++
        varShort.encode(value.spellDamageReceivedPercent) ++
        varInt.encode(value.initiative)
    }
}
