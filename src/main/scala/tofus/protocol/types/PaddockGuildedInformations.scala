package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockGuildedInformations(
  price: Long,
  locked: Boolean,
  deserted: Boolean,
  guildInfo: ConcreteGuildInformations
) extends PaddockBuyableInformations {
  override val protocolId = 508
}

object PaddockGuildedInformations {
  implicit val codec: Codec[PaddockGuildedInformations] =
    new Codec[PaddockGuildedInformations] {
      def decode: Get[PaddockGuildedInformations] =
        for {
          price <- varLong.decode
          locked <- bool.decode
          deserted <- bool.decode
          guildInfo <- Codec[ConcreteGuildInformations].decode
        } yield PaddockGuildedInformations(price, locked, deserted, guildInfo)

      def encode(value: PaddockGuildedInformations): ByteVector =
        varLong.encode(value.price) ++
        bool.encode(value.locked) ++
        bool.encode(value.deserted) ++
        Codec[ConcreteGuildInformations].encode(value.guildInfo)
    }
}
