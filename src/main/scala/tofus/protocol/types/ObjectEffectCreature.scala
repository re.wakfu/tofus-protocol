package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ObjectEffectCreature extends ObjectEffect

final case class ConcreteObjectEffectCreature(
  actionId: Short,
  monsterFamilyId: Short
) extends ObjectEffectCreature {
  override val protocolId = 71
}

object ConcreteObjectEffectCreature {
  implicit val codec: Codec[ConcreteObjectEffectCreature] =  
    new Codec[ConcreteObjectEffectCreature] {
      def decode: Get[ConcreteObjectEffectCreature] =
        for {
          actionId <- varShort.decode
          monsterFamilyId <- varShort.decode
        } yield ConcreteObjectEffectCreature(actionId, monsterFamilyId)

      def encode(value: ConcreteObjectEffectCreature): ByteVector =
        varShort.encode(value.actionId) ++
        varShort.encode(value.monsterFamilyId)
    }
}

object ObjectEffectCreature {
  implicit val codec: Codec[ObjectEffectCreature] =
    new Codec[ObjectEffectCreature] {
      def decode: Get[ObjectEffectCreature] =
        ushort.decode.flatMap {
          case 71 => Codec[ConcreteObjectEffectCreature].decode
          case 81 => Codec[ObjectEffectLadder].decode
        }

      def encode(value: ObjectEffectCreature): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteObjectEffectCreature => Codec[ConcreteObjectEffectCreature].encode(i)
          case i: ObjectEffectLadder => Codec[ObjectEffectLadder].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
