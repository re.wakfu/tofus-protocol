package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitleSelectedMessage(
  titleId: Short
) extends Message(6366)

object TitleSelectedMessage {
  implicit val codec: Codec[TitleSelectedMessage] =
    new Codec[TitleSelectedMessage] {
      def decode: Get[TitleSelectedMessage] =
        for {
          titleId <- varShort.decode
        } yield TitleSelectedMessage(titleId)

      def encode(value: TitleSelectedMessage): ByteVector =
        varShort.encode(value.titleId)
    }
}
