package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitleLostMessage(
  titleId: Short
) extends Message(6371)

object TitleLostMessage {
  implicit val codec: Codec[TitleLostMessage] =
    new Codec[TitleLostMessage] {
      def decode: Get[TitleLostMessage] =
        for {
          titleId <- varShort.decode
        } yield TitleLostMessage(titleId)

      def encode(value: TitleLostMessage): ByteVector =
        varShort.encode(value.titleId)
    }
}
