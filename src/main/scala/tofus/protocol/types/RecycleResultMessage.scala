package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class RecycleResultMessage(
  nuggetsForPrism: Int,
  nuggetsForPlayer: Int
) extends Message(6601)

object RecycleResultMessage {
  implicit val codec: Codec[RecycleResultMessage] =
    new Codec[RecycleResultMessage] {
      def decode: Get[RecycleResultMessage] =
        for {
          nuggetsForPrism <- varInt.decode
          nuggetsForPlayer <- varInt.decode
        } yield RecycleResultMessage(nuggetsForPrism, nuggetsForPlayer)

      def encode(value: RecycleResultMessage): ByteVector =
        varInt.encode(value.nuggetsForPrism) ++
        varInt.encode(value.nuggetsForPlayer)
    }
}
