package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FriendInformations extends AbstractContactInformations

final case class ConcreteFriendInformations(
  accountId: Int,
  accountName: String,
  playerState: Byte,
  lastConnection: Short,
  achievementPoints: Int,
  leagueId: Short,
  ladderPosition: Int
) extends FriendInformations {
  override val protocolId = 78
}

object ConcreteFriendInformations {
  implicit val codec: Codec[ConcreteFriendInformations] =  
    new Codec[ConcreteFriendInformations] {
      def decode: Get[ConcreteFriendInformations] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
          playerState <- byte.decode
          lastConnection <- varShort.decode
          achievementPoints <- int.decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
        } yield ConcreteFriendInformations(accountId, accountName, playerState, lastConnection, achievementPoints, leagueId, ladderPosition)

      def encode(value: ConcreteFriendInformations): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName) ++
        byte.encode(value.playerState) ++
        varShort.encode(value.lastConnection) ++
        int.encode(value.achievementPoints) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition)
    }
}

object FriendInformations {
  implicit val codec: Codec[FriendInformations] =
    new Codec[FriendInformations] {
      def decode: Get[FriendInformations] =
        ushort.decode.flatMap {
          case 78 => Codec[ConcreteFriendInformations].decode
          case 92 => Codec[FriendOnlineInformations].decode
        }

      def encode(value: FriendInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFriendInformations => Codec[ConcreteFriendInformations].encode(i)
          case i: FriendOnlineInformations => Codec[FriendOnlineInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
