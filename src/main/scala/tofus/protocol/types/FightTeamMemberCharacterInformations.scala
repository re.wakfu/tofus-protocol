package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightTeamMemberCharacterInformations extends FightTeamMemberInformations

final case class ConcreteFightTeamMemberCharacterInformations(
  id: Double,
  name: String,
  level: Short
) extends FightTeamMemberCharacterInformations {
  override val protocolId = 13
}

object ConcreteFightTeamMemberCharacterInformations {
  implicit val codec: Codec[ConcreteFightTeamMemberCharacterInformations] =  
    new Codec[ConcreteFightTeamMemberCharacterInformations] {
      def decode: Get[ConcreteFightTeamMemberCharacterInformations] =
        for {
          id <- double.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
        } yield ConcreteFightTeamMemberCharacterInformations(id, name, level)

      def encode(value: ConcreteFightTeamMemberCharacterInformations): ByteVector =
        double.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level)
    }
}

object FightTeamMemberCharacterInformations {
  implicit val codec: Codec[FightTeamMemberCharacterInformations] =
    new Codec[FightTeamMemberCharacterInformations] {
      def decode: Get[FightTeamMemberCharacterInformations] =
        ushort.decode.flatMap {
          case 13 => Codec[ConcreteFightTeamMemberCharacterInformations].decode
          case 426 => Codec[FightTeamMemberWithAllianceCharacterInformations].decode
        }

      def encode(value: FightTeamMemberCharacterInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightTeamMemberCharacterInformations => Codec[ConcreteFightTeamMemberCharacterInformations].encode(i)
          case i: FightTeamMemberWithAllianceCharacterInformations => Codec[FightTeamMemberWithAllianceCharacterInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
