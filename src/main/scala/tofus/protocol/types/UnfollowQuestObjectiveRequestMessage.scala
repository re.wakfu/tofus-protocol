package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class UnfollowQuestObjectiveRequestMessage(
  questId: Short,
  objectiveId: Short
) extends Message(6723)

object UnfollowQuestObjectiveRequestMessage {
  implicit val codec: Codec[UnfollowQuestObjectiveRequestMessage] =
    new Codec[UnfollowQuestObjectiveRequestMessage] {
      def decode: Get[UnfollowQuestObjectiveRequestMessage] =
        for {
          questId <- varShort.decode
          objectiveId <- short.decode
        } yield UnfollowQuestObjectiveRequestMessage(questId, objectiveId)

      def encode(value: UnfollowQuestObjectiveRequestMessage): ByteVector =
        varShort.encode(value.questId) ++
        short.encode(value.objectiveId)
    }
}
