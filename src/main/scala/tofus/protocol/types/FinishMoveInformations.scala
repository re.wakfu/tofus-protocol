package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FinishMoveInformations(
  finishMoveId: Int,
  finishMoveState: Boolean
) extends ProtocolType {
  override val protocolId = 506
}

object FinishMoveInformations {
  implicit val codec: Codec[FinishMoveInformations] =
    new Codec[FinishMoveInformations] {
      def decode: Get[FinishMoveInformations] =
        for {
          finishMoveId <- int.decode
          finishMoveState <- bool.decode
        } yield FinishMoveInformations(finishMoveId, finishMoveState)

      def encode(value: FinishMoveInformations): ByteVector =
        int.encode(value.finishMoveId) ++
        bool.encode(value.finishMoveState)
    }
}
