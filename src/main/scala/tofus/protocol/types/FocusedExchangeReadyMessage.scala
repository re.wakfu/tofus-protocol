package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FocusedExchangeReadyMessage(
  ready: Boolean,
  step: Short,
  focusActionId: Int
) extends Message(6701)

object FocusedExchangeReadyMessage {
  implicit val codec: Codec[FocusedExchangeReadyMessage] =
    new Codec[FocusedExchangeReadyMessage] {
      def decode: Get[FocusedExchangeReadyMessage] =
        for {
          ready <- bool.decode
          step <- varShort.decode
          focusActionId <- varInt.decode
        } yield FocusedExchangeReadyMessage(ready, step, focusActionId)

      def encode(value: FocusedExchangeReadyMessage): ByteVector =
        bool.encode(value.ready) ++
        varShort.encode(value.step) ++
        varInt.encode(value.focusActionId)
    }
}
