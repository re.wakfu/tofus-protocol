package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OrnamentGainedMessage(
  ornamentId: Short
) extends Message(6368)

object OrnamentGainedMessage {
  implicit val codec: Codec[OrnamentGainedMessage] =
    new Codec[OrnamentGainedMessage] {
      def decode: Get[OrnamentGainedMessage] =
        for {
          ornamentId <- short.decode
        } yield OrnamentGainedMessage(ornamentId)

      def encode(value: OrnamentGainedMessage): ByteVector =
        short.encode(value.ornamentId)
    }
}
