package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaLeagueRewardsMessage(
  seasonId: Short,
  leagueId: Short,
  ladderPosition: Int,
  endSeasonReward: Boolean
) extends Message(6785)

object GameRolePlayArenaLeagueRewardsMessage {
  implicit val codec: Codec[GameRolePlayArenaLeagueRewardsMessage] =
    new Codec[GameRolePlayArenaLeagueRewardsMessage] {
      def decode: Get[GameRolePlayArenaLeagueRewardsMessage] =
        for {
          seasonId <- varShort.decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
          endSeasonReward <- bool.decode
        } yield GameRolePlayArenaLeagueRewardsMessage(seasonId, leagueId, ladderPosition, endSeasonReward)

      def encode(value: GameRolePlayArenaLeagueRewardsMessage): ByteVector =
        varShort.encode(value.seasonId) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition) ++
        bool.encode(value.endSeasonReward)
    }
}
