package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentificationMessage(
  flags0: Byte,
  version: Version,
  lang: String,
  credentials: ByteVector,
  serverId: Short,
  sessionOptionalSalt: Long,
  failedAttempts: List[Short]
) extends Message(4)

object IdentificationMessage {
  implicit val codec: Codec[IdentificationMessage] =
    new Codec[IdentificationMessage] {
      def decode: Get[IdentificationMessage] =
        for {
          flags0 <- byte.decode
          version <- Codec[Version].decode
          lang <- utf8(ushort).decode
          credentials <- bytes(varInt).decode
          serverId <- short.decode
          sessionOptionalSalt <- varLong.decode
          failedAttempts <- list(ushort, varShort).decode
        } yield IdentificationMessage(flags0, version, lang, credentials, serverId, sessionOptionalSalt, failedAttempts)

      def encode(value: IdentificationMessage): ByteVector =
        byte.encode(value.flags0) ++
        Codec[Version].encode(value.version) ++
        utf8(ushort).encode(value.lang) ++
        bytes(varInt).encode(value.credentials) ++
        short.encode(value.serverId) ++
        varLong.encode(value.sessionOptionalSalt) ++
        list(ushort, varShort).encode(value.failedAttempts)
    }
}
