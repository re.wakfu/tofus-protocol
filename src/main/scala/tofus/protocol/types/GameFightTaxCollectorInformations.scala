package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTaxCollectorInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  firstNameId: Short,
  lastNameId: Short,
  level: Short
) extends GameFightAIInformations {
  override val protocolId = 48
}

object GameFightTaxCollectorInformations {
  implicit val codec: Codec[GameFightTaxCollectorInformations] =
    new Codec[GameFightTaxCollectorInformations] {
      def decode: Get[GameFightTaxCollectorInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
          level <- ubyte.decode
        } yield GameFightTaxCollectorInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, firstNameId, lastNameId, level)

      def encode(value: GameFightTaxCollectorInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId) ++
        ubyte.encode(value.level)
    }
}
