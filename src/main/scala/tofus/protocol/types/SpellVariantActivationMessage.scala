package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpellVariantActivationMessage(
  spellId: Short,
  result: Boolean
) extends Message(6705)

object SpellVariantActivationMessage {
  implicit val codec: Codec[SpellVariantActivationMessage] =
    new Codec[SpellVariantActivationMessage] {
      def decode: Get[SpellVariantActivationMessage] =
        for {
          spellId <- varShort.decode
          result <- bool.decode
        } yield SpellVariantActivationMessage(spellId, result)

      def encode(value: SpellVariantActivationMessage): ByteVector =
        varShort.encode(value.spellId) ++
        bool.encode(value.result)
    }
}
