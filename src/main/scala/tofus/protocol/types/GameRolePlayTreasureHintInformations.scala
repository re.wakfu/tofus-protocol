package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayTreasureHintInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  npcId: Short
) extends GameRolePlayActorInformations {
  override val protocolId = 471
}

object GameRolePlayTreasureHintInformations {
  implicit val codec: Codec[GameRolePlayTreasureHintInformations] =
    new Codec[GameRolePlayTreasureHintInformations] {
      def decode: Get[GameRolePlayTreasureHintInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          npcId <- varShort.decode
        } yield GameRolePlayTreasureHintInformations(contextualId, disposition, look, npcId)

      def encode(value: GameRolePlayTreasureHintInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        varShort.encode(value.npcId)
    }
}
