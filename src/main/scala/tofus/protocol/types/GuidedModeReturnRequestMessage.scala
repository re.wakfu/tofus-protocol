package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuidedModeReturnRequestMessage(

) extends Message(6088)

object GuidedModeReturnRequestMessage {
  implicit val codec: Codec[GuidedModeReturnRequestMessage] =
    Codec.const(GuidedModeReturnRequestMessage())
}
