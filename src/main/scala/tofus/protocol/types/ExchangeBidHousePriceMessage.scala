package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHousePriceMessage(
  genId: Short
) extends Message(5805)

object ExchangeBidHousePriceMessage {
  implicit val codec: Codec[ExchangeBidHousePriceMessage] =
    new Codec[ExchangeBidHousePriceMessage] {
      def decode: Get[ExchangeBidHousePriceMessage] =
        for {
          genId <- varShort.decode
        } yield ExchangeBidHousePriceMessage(genId)

      def encode(value: ExchangeBidHousePriceMessage): ByteVector =
        varShort.encode(value.genId)
    }
}
