package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildChangeMemberParametersMessage(
  memberId: Long,
  rank: Short,
  experienceGivenPercent: Byte,
  rights: Int
) extends Message(5549)

object GuildChangeMemberParametersMessage {
  implicit val codec: Codec[GuildChangeMemberParametersMessage] =
    new Codec[GuildChangeMemberParametersMessage] {
      def decode: Get[GuildChangeMemberParametersMessage] =
        for {
          memberId <- varLong.decode
          rank <- varShort.decode
          experienceGivenPercent <- byte.decode
          rights <- varInt.decode
        } yield GuildChangeMemberParametersMessage(memberId, rank, experienceGivenPercent, rights)

      def encode(value: GuildChangeMemberParametersMessage): ByteVector =
        varLong.encode(value.memberId) ++
        varShort.encode(value.rank) ++
        byte.encode(value.experienceGivenPercent) ++
        varInt.encode(value.rights)
    }
}
