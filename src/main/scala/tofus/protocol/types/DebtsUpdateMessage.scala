package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DebtsUpdateMessage(
  action: Byte,
  debts: List[DebtInformation]
) extends Message(6865)

object DebtsUpdateMessage {
  implicit val codec: Codec[DebtsUpdateMessage] =
    new Codec[DebtsUpdateMessage] {
      def decode: Get[DebtsUpdateMessage] =
        for {
          action <- byte.decode
          debts <- list(ushort, Codec[DebtInformation]).decode
        } yield DebtsUpdateMessage(action, debts)

      def encode(value: DebtsUpdateMessage): ByteVector =
        byte.encode(value.action) ++
        list(ushort, Codec[DebtInformation]).encode(value.debts)
    }
}
