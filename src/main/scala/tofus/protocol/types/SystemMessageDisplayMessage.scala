package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SystemMessageDisplayMessage(
  hangUp: Boolean,
  msgId: Short,
  parameters: List[String]
) extends Message(189)

object SystemMessageDisplayMessage {
  implicit val codec: Codec[SystemMessageDisplayMessage] =
    new Codec[SystemMessageDisplayMessage] {
      def decode: Get[SystemMessageDisplayMessage] =
        for {
          hangUp <- bool.decode
          msgId <- varShort.decode
          parameters <- list(ushort, utf8(ushort)).decode
        } yield SystemMessageDisplayMessage(hangUp, msgId, parameters)

      def encode(value: SystemMessageDisplayMessage): ByteVector =
        bool.encode(value.hangUp) ++
        varShort.encode(value.msgId) ++
        list(ushort, utf8(ushort)).encode(value.parameters)
    }
}
