package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportRequestMessage(
  sourceType: Byte,
  destinationType: Byte,
  mapId: Double
) extends Message(5961)

object TeleportRequestMessage {
  implicit val codec: Codec[TeleportRequestMessage] =
    new Codec[TeleportRequestMessage] {
      def decode: Get[TeleportRequestMessage] =
        for {
          sourceType <- byte.decode
          destinationType <- byte.decode
          mapId <- double.decode
        } yield TeleportRequestMessage(sourceType, destinationType, mapId)

      def encode(value: TeleportRequestMessage): ByteVector =
        byte.encode(value.sourceType) ++
        byte.encode(value.destinationType) ++
        double.encode(value.mapId)
    }
}
