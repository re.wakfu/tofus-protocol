package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameRolePlayActorInformations extends GameContextActorInformations

final case class ConcreteGameRolePlayActorInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook
) extends GameRolePlayActorInformations {
  override val protocolId = 141
}

object ConcreteGameRolePlayActorInformations {
  implicit val codec: Codec[ConcreteGameRolePlayActorInformations] =  
    new Codec[ConcreteGameRolePlayActorInformations] {
      def decode: Get[ConcreteGameRolePlayActorInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
        } yield ConcreteGameRolePlayActorInformations(contextualId, disposition, look)

      def encode(value: ConcreteGameRolePlayActorInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look)
    }
}

object GameRolePlayActorInformations {
  implicit val codec: Codec[GameRolePlayActorInformations] =
    new Codec[GameRolePlayActorInformations] {
      def decode: Get[GameRolePlayActorInformations] =
        ushort.decode.flatMap {
          case 141 => Codec[ConcreteGameRolePlayActorInformations].decode
          case 464 => Codec[GameRolePlayGroupMonsterWaveInformations].decode
          case 160 => Codec[ConcreteGameRolePlayGroupMonsterInformations].decode
          case 467 => Codec[GameRolePlayPortalInformations].decode
          case 383 => Codec[GameRolePlayNpcWithQuestInformations].decode
          case 156 => Codec[ConcreteGameRolePlayNpcInformations].decode
          case 3 => Codec[GameRolePlayMutantInformations].decode
          case 36 => Codec[GameRolePlayCharacterInformations].decode
          case 159 => Codec[ConcreteGameRolePlayHumanoidInformations].decode
          case 180 => Codec[GameRolePlayMountInformations].decode
          case 129 => Codec[GameRolePlayMerchantInformations].decode
          case 154 => Codec[ConcreteGameRolePlayNamedActorInformations].decode
          case 161 => Codec[GameRolePlayPrismInformations].decode
          case 471 => Codec[GameRolePlayTreasureHintInformations].decode
          case 148 => Codec[GameRolePlayTaxCollectorInformations].decode
        }

      def encode(value: GameRolePlayActorInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameRolePlayActorInformations => Codec[ConcreteGameRolePlayActorInformations].encode(i)
          case i: GameRolePlayGroupMonsterWaveInformations => Codec[GameRolePlayGroupMonsterWaveInformations].encode(i)
          case i: ConcreteGameRolePlayGroupMonsterInformations => Codec[ConcreteGameRolePlayGroupMonsterInformations].encode(i)
          case i: GameRolePlayPortalInformations => Codec[GameRolePlayPortalInformations].encode(i)
          case i: GameRolePlayNpcWithQuestInformations => Codec[GameRolePlayNpcWithQuestInformations].encode(i)
          case i: ConcreteGameRolePlayNpcInformations => Codec[ConcreteGameRolePlayNpcInformations].encode(i)
          case i: GameRolePlayMutantInformations => Codec[GameRolePlayMutantInformations].encode(i)
          case i: GameRolePlayCharacterInformations => Codec[GameRolePlayCharacterInformations].encode(i)
          case i: ConcreteGameRolePlayHumanoidInformations => Codec[ConcreteGameRolePlayHumanoidInformations].encode(i)
          case i: GameRolePlayMountInformations => Codec[GameRolePlayMountInformations].encode(i)
          case i: GameRolePlayMerchantInformations => Codec[GameRolePlayMerchantInformations].encode(i)
          case i: ConcreteGameRolePlayNamedActorInformations => Codec[ConcreteGameRolePlayNamedActorInformations].encode(i)
          case i: GameRolePlayPrismInformations => Codec[GameRolePlayPrismInformations].encode(i)
          case i: GameRolePlayTreasureHintInformations => Codec[GameRolePlayTreasureHintInformations].encode(i)
          case i: GameRolePlayTaxCollectorInformations => Codec[GameRolePlayTaxCollectorInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
