package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdentifiedEntityDispositionInformations(
  cellId: Short,
  direction: Byte,
  id: Double
) extends EntityDispositionInformations {
  override val protocolId = 107
}

object IdentifiedEntityDispositionInformations {
  implicit val codec: Codec[IdentifiedEntityDispositionInformations] =
    new Codec[IdentifiedEntityDispositionInformations] {
      def decode: Get[IdentifiedEntityDispositionInformations] =
        for {
          cellId <- short.decode
          direction <- byte.decode
          id <- double.decode
        } yield IdentifiedEntityDispositionInformations(cellId, direction, id)

      def encode(value: IdentifiedEntityDispositionInformations): ByteVector =
        short.encode(value.cellId) ++
        byte.encode(value.direction) ++
        double.encode(value.id)
    }
}
