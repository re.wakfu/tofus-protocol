package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorMovementMessage(
  movementType: Byte,
  basicInfos: TaxCollectorBasicInformations,
  playerId: Long,
  playerName: String
) extends Message(5633)

object TaxCollectorMovementMessage {
  implicit val codec: Codec[TaxCollectorMovementMessage] =
    new Codec[TaxCollectorMovementMessage] {
      def decode: Get[TaxCollectorMovementMessage] =
        for {
          movementType <- byte.decode
          basicInfos <- Codec[TaxCollectorBasicInformations].decode
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
        } yield TaxCollectorMovementMessage(movementType, basicInfos, playerId, playerName)

      def encode(value: TaxCollectorMovementMessage): ByteVector =
        byte.encode(value.movementType) ++
        Codec[TaxCollectorBasicInformations].encode(value.basicInfos) ++
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName)
    }
}
