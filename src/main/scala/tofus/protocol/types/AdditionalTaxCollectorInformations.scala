package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AdditionalTaxCollectorInformations(
  collectorCallerName: String,
  date: Int
) extends ProtocolType {
  override val protocolId = 165
}

object AdditionalTaxCollectorInformations {
  implicit val codec: Codec[AdditionalTaxCollectorInformations] =
    new Codec[AdditionalTaxCollectorInformations] {
      def decode: Get[AdditionalTaxCollectorInformations] =
        for {
          collectorCallerName <- utf8(ushort).decode
          date <- int.decode
        } yield AdditionalTaxCollectorInformations(collectorCallerName, date)

      def encode(value: AdditionalTaxCollectorInformations): ByteVector =
        utf8(ushort).encode(value.collectorCallerName) ++
        int.encode(value.date)
    }
}
