package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildSpellUpgradeRequestMessage(
  spellId: Int
) extends Message(5699)

object GuildSpellUpgradeRequestMessage {
  implicit val codec: Codec[GuildSpellUpgradeRequestMessage] =
    new Codec[GuildSpellUpgradeRequestMessage] {
      def decode: Get[GuildSpellUpgradeRequestMessage] =
        for {
          spellId <- int.decode
        } yield GuildSpellUpgradeRequestMessage(spellId)

      def encode(value: GuildSpellUpgradeRequestMessage): ByteVector =
        int.encode(value.spellId)
    }
}
