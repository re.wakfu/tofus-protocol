package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaUpdatePlayerInfosMessage(
  solo: ArenaRankInfos
) extends Message(6301)

object GameRolePlayArenaUpdatePlayerInfosMessage {
  implicit val codec: Codec[GameRolePlayArenaUpdatePlayerInfosMessage] =
    new Codec[GameRolePlayArenaUpdatePlayerInfosMessage] {
      def decode: Get[GameRolePlayArenaUpdatePlayerInfosMessage] =
        for {
          solo <- Codec[ArenaRankInfos].decode
        } yield GameRolePlayArenaUpdatePlayerInfosMessage(solo)

      def encode(value: GameRolePlayArenaUpdatePlayerInfosMessage): ByteVector =
        Codec[ArenaRankInfos].encode(value.solo)
    }
}
