package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CompassUpdatePvpSeekMessage(
  `type`: Byte,
  coords: MapCoordinates,
  memberId: Long,
  memberName: String
) extends Message(6013)

object CompassUpdatePvpSeekMessage {
  implicit val codec: Codec[CompassUpdatePvpSeekMessage] =
    new Codec[CompassUpdatePvpSeekMessage] {
      def decode: Get[CompassUpdatePvpSeekMessage] =
        for {
          `type` <- byte.decode
          coords <- Codec[MapCoordinates].decode
          memberId <- varLong.decode
          memberName <- utf8(ushort).decode
        } yield CompassUpdatePvpSeekMessage(`type`, coords, memberId, memberName)

      def encode(value: CompassUpdatePvpSeekMessage): ByteVector =
        byte.encode(value.`type`) ++
        Codec[MapCoordinates].encode(value.coords) ++
        varLong.encode(value.memberId) ++
        utf8(ushort).encode(value.memberName)
    }
}
