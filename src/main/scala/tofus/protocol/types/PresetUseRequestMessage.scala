package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetUseRequestMessage(
  presetId: Short
) extends Message(6759)

object PresetUseRequestMessage {
  implicit val codec: Codec[PresetUseRequestMessage] =
    new Codec[PresetUseRequestMessage] {
      def decode: Get[PresetUseRequestMessage] =
        for {
          presetId <- short.decode
        } yield PresetUseRequestMessage(presetId)

      def encode(value: PresetUseRequestMessage): ByteVector =
        short.encode(value.presetId)
    }
}
