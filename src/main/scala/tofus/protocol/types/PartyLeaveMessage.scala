package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyLeaveMessage(
  partyId: Int
) extends Message(5594)

object PartyLeaveMessage {
  implicit val codec: Codec[PartyLeaveMessage] =
    new Codec[PartyLeaveMessage] {
      def decode: Get[PartyLeaveMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyLeaveMessage(partyId)

      def encode(value: PartyLeaveMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
