package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextMoveMultipleElementsMessage(
  movements: List[EntityMovementInformations]
) extends Message(254)

object GameContextMoveMultipleElementsMessage {
  implicit val codec: Codec[GameContextMoveMultipleElementsMessage] =
    new Codec[GameContextMoveMultipleElementsMessage] {
      def decode: Get[GameContextMoveMultipleElementsMessage] =
        for {
          movements <- list(ushort, Codec[EntityMovementInformations]).decode
        } yield GameContextMoveMultipleElementsMessage(movements)

      def encode(value: GameContextMoveMultipleElementsMessage): ByteVector =
        list(ushort, Codec[EntityMovementInformations]).encode(value.movements)
    }
}
