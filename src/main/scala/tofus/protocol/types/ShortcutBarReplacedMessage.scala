package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarReplacedMessage(
  barType: Byte,
  shortcut: Shortcut
) extends Message(6706)

object ShortcutBarReplacedMessage {
  implicit val codec: Codec[ShortcutBarReplacedMessage] =
    new Codec[ShortcutBarReplacedMessage] {
      def decode: Get[ShortcutBarReplacedMessage] =
        for {
          barType <- byte.decode
          shortcut <- Codec[Shortcut].decode
        } yield ShortcutBarReplacedMessage(barType, shortcut)

      def encode(value: ShortcutBarReplacedMessage): ByteVector =
        byte.encode(value.barType) ++
        Codec[Shortcut].encode(value.shortcut)
    }
}
