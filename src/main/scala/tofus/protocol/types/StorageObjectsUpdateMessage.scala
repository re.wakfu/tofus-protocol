package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StorageObjectsUpdateMessage(
  objectList: List[ObjectItem]
) extends Message(6036)

object StorageObjectsUpdateMessage {
  implicit val codec: Codec[StorageObjectsUpdateMessage] =
    new Codec[StorageObjectsUpdateMessage] {
      def decode: Get[StorageObjectsUpdateMessage] =
        for {
          objectList <- list(ushort, Codec[ObjectItem]).decode
        } yield StorageObjectsUpdateMessage(objectList)

      def encode(value: StorageObjectsUpdateMessage): ByteVector =
        list(ushort, Codec[ObjectItem]).encode(value.objectList)
    }
}
