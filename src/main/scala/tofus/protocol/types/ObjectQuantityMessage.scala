package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectQuantityMessage(
  objectUID: Int,
  quantity: Int,
  origin: Byte
) extends Message(3023)

object ObjectQuantityMessage {
  implicit val codec: Codec[ObjectQuantityMessage] =
    new Codec[ObjectQuantityMessage] {
      def decode: Get[ObjectQuantityMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
          origin <- byte.decode
        } yield ObjectQuantityMessage(objectUID, quantity, origin)

      def encode(value: ObjectQuantityMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity) ++
        byte.encode(value.origin)
    }
}
