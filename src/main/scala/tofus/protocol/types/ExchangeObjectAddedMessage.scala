package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectAddedMessage(
  remote: Boolean,
  `object`: ObjectItem
) extends Message(5516)

object ExchangeObjectAddedMessage {
  implicit val codec: Codec[ExchangeObjectAddedMessage] =
    new Codec[ExchangeObjectAddedMessage] {
      def decode: Get[ExchangeObjectAddedMessage] =
        for {
          remote <- bool.decode
          `object` <- Codec[ObjectItem].decode
        } yield ExchangeObjectAddedMessage(remote, `object`)

      def encode(value: ExchangeObjectAddedMessage): ByteVector =
        bool.encode(value.remote) ++
        Codec[ObjectItem].encode(value.`object`)
    }
}
