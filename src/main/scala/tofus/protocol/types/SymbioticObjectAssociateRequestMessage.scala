package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SymbioticObjectAssociateRequestMessage(
  symbioteUID: Int,
  symbiotePos: Short,
  hostUID: Int,
  hostPos: Short
) extends Message(6522)

object SymbioticObjectAssociateRequestMessage {
  implicit val codec: Codec[SymbioticObjectAssociateRequestMessage] =
    new Codec[SymbioticObjectAssociateRequestMessage] {
      def decode: Get[SymbioticObjectAssociateRequestMessage] =
        for {
          symbioteUID <- varInt.decode
          symbiotePos <- ubyte.decode
          hostUID <- varInt.decode
          hostPos <- ubyte.decode
        } yield SymbioticObjectAssociateRequestMessage(symbioteUID, symbiotePos, hostUID, hostPos)

      def encode(value: SymbioticObjectAssociateRequestMessage): ByteVector =
        varInt.encode(value.symbioteUID) ++
        ubyte.encode(value.symbiotePos) ++
        varInt.encode(value.hostUID) ++
        ubyte.encode(value.hostPos)
    }
}
