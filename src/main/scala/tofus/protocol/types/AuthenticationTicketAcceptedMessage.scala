package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AuthenticationTicketAcceptedMessage(

) extends Message(111)

object AuthenticationTicketAcceptedMessage {
  implicit val codec: Codec[AuthenticationTicketAcceptedMessage] =
    Codec.const(AuthenticationTicketAcceptedMessage())
}
