package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyMemberRemoveMessage(
  partyId: Int,
  leavingPlayerId: Long
) extends Message(5579)

object PartyMemberRemoveMessage {
  implicit val codec: Codec[PartyMemberRemoveMessage] =
    new Codec[PartyMemberRemoveMessage] {
      def decode: Get[PartyMemberRemoveMessage] =
        for {
          partyId <- varInt.decode
          leavingPlayerId <- varLong.decode
        } yield PartyMemberRemoveMessage(partyId, leavingPlayerId)

      def encode(value: PartyMemberRemoveMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.leavingPlayerId)
    }
}
