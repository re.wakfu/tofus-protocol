package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameContextActorPositionInformations extends ProtocolType

final case class ConcreteGameContextActorPositionInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations
) extends GameContextActorPositionInformations {
  override val protocolId = 566
}

object ConcreteGameContextActorPositionInformations {
  implicit val codec: Codec[ConcreteGameContextActorPositionInformations] =  
    new Codec[ConcreteGameContextActorPositionInformations] {
      def decode: Get[ConcreteGameContextActorPositionInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
        } yield ConcreteGameContextActorPositionInformations(contextualId, disposition)

      def encode(value: ConcreteGameContextActorPositionInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition)
    }
}

object GameContextActorPositionInformations {
  implicit val codec: Codec[GameContextActorPositionInformations] =
    new Codec[GameContextActorPositionInformations] {
      def decode: Get[GameContextActorPositionInformations] =
        ushort.decode.flatMap {
          case 566 => Codec[ConcreteGameContextActorPositionInformations].decode
          case 46 => Codec[GameFightCharacterInformations].decode
          case 50 => Codec[GameFightMutantInformations].decode
          case 158 => Codec[ConcreteGameFightFighterNamedInformations].decode
          case 203 => Codec[GameFightMonsterWithAlignmentInformations].decode
          case 29 => Codec[ConcreteGameFightMonsterInformations].decode
          case 48 => Codec[GameFightTaxCollectorInformations].decode
          case 151 => Codec[ConcreteGameFightAIInformations].decode
          case 551 => Codec[GameFightEntityInformation].decode
          case 143 => Codec[ConcreteGameFightFighterInformations].decode
          case 464 => Codec[GameRolePlayGroupMonsterWaveInformations].decode
          case 160 => Codec[ConcreteGameRolePlayGroupMonsterInformations].decode
          case 467 => Codec[GameRolePlayPortalInformations].decode
          case 383 => Codec[GameRolePlayNpcWithQuestInformations].decode
          case 156 => Codec[ConcreteGameRolePlayNpcInformations].decode
          case 3 => Codec[GameRolePlayMutantInformations].decode
          case 36 => Codec[GameRolePlayCharacterInformations].decode
          case 159 => Codec[ConcreteGameRolePlayHumanoidInformations].decode
          case 180 => Codec[GameRolePlayMountInformations].decode
          case 129 => Codec[GameRolePlayMerchantInformations].decode
          case 154 => Codec[ConcreteGameRolePlayNamedActorInformations].decode
          case 161 => Codec[GameRolePlayPrismInformations].decode
          case 471 => Codec[GameRolePlayTreasureHintInformations].decode
          case 148 => Codec[GameRolePlayTaxCollectorInformations].decode
          case 141 => Codec[ConcreteGameRolePlayActorInformations].decode
          case 150 => Codec[ConcreteGameContextActorInformations].decode
        }

      def encode(value: GameContextActorPositionInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameContextActorPositionInformations => Codec[ConcreteGameContextActorPositionInformations].encode(i)
          case i: GameFightCharacterInformations => Codec[GameFightCharacterInformations].encode(i)
          case i: GameFightMutantInformations => Codec[GameFightMutantInformations].encode(i)
          case i: ConcreteGameFightFighterNamedInformations => Codec[ConcreteGameFightFighterNamedInformations].encode(i)
          case i: GameFightMonsterWithAlignmentInformations => Codec[GameFightMonsterWithAlignmentInformations].encode(i)
          case i: ConcreteGameFightMonsterInformations => Codec[ConcreteGameFightMonsterInformations].encode(i)
          case i: GameFightTaxCollectorInformations => Codec[GameFightTaxCollectorInformations].encode(i)
          case i: ConcreteGameFightAIInformations => Codec[ConcreteGameFightAIInformations].encode(i)
          case i: GameFightEntityInformation => Codec[GameFightEntityInformation].encode(i)
          case i: ConcreteGameFightFighterInformations => Codec[ConcreteGameFightFighterInformations].encode(i)
          case i: GameRolePlayGroupMonsterWaveInformations => Codec[GameRolePlayGroupMonsterWaveInformations].encode(i)
          case i: ConcreteGameRolePlayGroupMonsterInformations => Codec[ConcreteGameRolePlayGroupMonsterInformations].encode(i)
          case i: GameRolePlayPortalInformations => Codec[GameRolePlayPortalInformations].encode(i)
          case i: GameRolePlayNpcWithQuestInformations => Codec[GameRolePlayNpcWithQuestInformations].encode(i)
          case i: ConcreteGameRolePlayNpcInformations => Codec[ConcreteGameRolePlayNpcInformations].encode(i)
          case i: GameRolePlayMutantInformations => Codec[GameRolePlayMutantInformations].encode(i)
          case i: GameRolePlayCharacterInformations => Codec[GameRolePlayCharacterInformations].encode(i)
          case i: ConcreteGameRolePlayHumanoidInformations => Codec[ConcreteGameRolePlayHumanoidInformations].encode(i)
          case i: GameRolePlayMountInformations => Codec[GameRolePlayMountInformations].encode(i)
          case i: GameRolePlayMerchantInformations => Codec[GameRolePlayMerchantInformations].encode(i)
          case i: ConcreteGameRolePlayNamedActorInformations => Codec[ConcreteGameRolePlayNamedActorInformations].encode(i)
          case i: GameRolePlayPrismInformations => Codec[GameRolePlayPrismInformations].encode(i)
          case i: GameRolePlayTreasureHintInformations => Codec[GameRolePlayTreasureHintInformations].encode(i)
          case i: GameRolePlayTaxCollectorInformations => Codec[GameRolePlayTaxCollectorInformations].encode(i)
          case i: ConcreteGameRolePlayActorInformations => Codec[ConcreteGameRolePlayActorInformations].encode(i)
          case i: ConcreteGameContextActorInformations => Codec[ConcreteGameContextActorInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
