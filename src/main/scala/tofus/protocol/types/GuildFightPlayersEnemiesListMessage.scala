package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightPlayersEnemiesListMessage(
  fightId: Double,
  playerInfo: List[ConcreteCharacterMinimalPlusLookInformations]
) extends Message(5928)

object GuildFightPlayersEnemiesListMessage {
  implicit val codec: Codec[GuildFightPlayersEnemiesListMessage] =
    new Codec[GuildFightPlayersEnemiesListMessage] {
      def decode: Get[GuildFightPlayersEnemiesListMessage] =
        for {
          fightId <- double.decode
          playerInfo <- list(ushort, Codec[ConcreteCharacterMinimalPlusLookInformations]).decode
        } yield GuildFightPlayersEnemiesListMessage(fightId, playerInfo)

      def encode(value: GuildFightPlayersEnemiesListMessage): ByteVector =
        double.encode(value.fightId) ++
        list(ushort, Codec[ConcreteCharacterMinimalPlusLookInformations]).encode(value.playerInfo)
    }
}
