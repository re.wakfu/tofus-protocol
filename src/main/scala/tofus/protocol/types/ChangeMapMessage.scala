package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChangeMapMessage(
  mapId: Double,
  autopilot: Boolean
) extends Message(221)

object ChangeMapMessage {
  implicit val codec: Codec[ChangeMapMessage] =
    new Codec[ChangeMapMessage] {
      def decode: Get[ChangeMapMessage] =
        for {
          mapId <- double.decode
          autopilot <- bool.decode
        } yield ChangeMapMessage(mapId, autopilot)

      def encode(value: ChangeMapMessage): ByteVector =
        double.encode(value.mapId) ++
        bool.encode(value.autopilot)
    }
}
