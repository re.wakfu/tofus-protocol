package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceModificationNameAndTagValidMessage(
  allianceName: String,
  allianceTag: String
) extends Message(6449)

object AllianceModificationNameAndTagValidMessage {
  implicit val codec: Codec[AllianceModificationNameAndTagValidMessage] =
    new Codec[AllianceModificationNameAndTagValidMessage] {
      def decode: Get[AllianceModificationNameAndTagValidMessage] =
        for {
          allianceName <- utf8(ushort).decode
          allianceTag <- utf8(ushort).decode
        } yield AllianceModificationNameAndTagValidMessage(allianceName, allianceTag)

      def encode(value: AllianceModificationNameAndTagValidMessage): ByteVector =
        utf8(ushort).encode(value.allianceName) ++
        utf8(ushort).encode(value.allianceTag)
    }
}
