package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterNameSuggestionSuccessMessage(
  suggestion: String
) extends Message(5544)

object CharacterNameSuggestionSuccessMessage {
  implicit val codec: Codec[CharacterNameSuggestionSuccessMessage] =
    new Codec[CharacterNameSuggestionSuccessMessage] {
      def decode: Get[CharacterNameSuggestionSuccessMessage] =
        for {
          suggestion <- utf8(ushort).decode
        } yield CharacterNameSuggestionSuccessMessage(suggestion)

      def encode(value: CharacterNameSuggestionSuccessMessage): ByteVector =
        utf8(ushort).encode(value.suggestion)
    }
}
