package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTeamMemberMonsterInformations(
  id: Double,
  monsterId: Int,
  grade: Byte
) extends FightTeamMemberInformations {
  override val protocolId = 6
}

object FightTeamMemberMonsterInformations {
  implicit val codec: Codec[FightTeamMemberMonsterInformations] =
    new Codec[FightTeamMemberMonsterInformations] {
      def decode: Get[FightTeamMemberMonsterInformations] =
        for {
          id <- double.decode
          monsterId <- int.decode
          grade <- byte.decode
        } yield FightTeamMemberMonsterInformations(id, monsterId, grade)

      def encode(value: FightTeamMemberMonsterInformations): ByteVector =
        double.encode(value.id) ++
        int.encode(value.monsterId) ++
        byte.encode(value.grade)
    }
}
