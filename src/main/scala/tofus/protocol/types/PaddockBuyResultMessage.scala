package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockBuyResultMessage(
  paddockId: Double,
  bought: Boolean,
  realPrice: Long
) extends Message(6516)

object PaddockBuyResultMessage {
  implicit val codec: Codec[PaddockBuyResultMessage] =
    new Codec[PaddockBuyResultMessage] {
      def decode: Get[PaddockBuyResultMessage] =
        for {
          paddockId <- double.decode
          bought <- bool.decode
          realPrice <- varLong.decode
        } yield PaddockBuyResultMessage(paddockId, bought, realPrice)

      def encode(value: PaddockBuyResultMessage): ByteVector =
        double.encode(value.paddockId) ++
        bool.encode(value.bought) ++
        varLong.encode(value.realPrice)
    }
}
