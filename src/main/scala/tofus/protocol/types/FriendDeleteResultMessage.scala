package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendDeleteResultMessage(
  success: Boolean,
  name: String
) extends Message(5601)

object FriendDeleteResultMessage {
  implicit val codec: Codec[FriendDeleteResultMessage] =
    new Codec[FriendDeleteResultMessage] {
      def decode: Get[FriendDeleteResultMessage] =
        for {
          success <- bool.decode
          name <- utf8(ushort).decode
        } yield FriendDeleteResultMessage(success, name)

      def encode(value: FriendDeleteResultMessage): ByteVector =
        bool.encode(value.success) ++
        utf8(ushort).encode(value.name)
    }
}
