package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceFactsErrorMessage(
  allianceId: Int
) extends Message(6423)

object AllianceFactsErrorMessage {
  implicit val codec: Codec[AllianceFactsErrorMessage] =
    new Codec[AllianceFactsErrorMessage] {
      def decode: Get[AllianceFactsErrorMessage] =
        for {
          allianceId <- varInt.decode
        } yield AllianceFactsErrorMessage(allianceId)

      def encode(value: AllianceFactsErrorMessage): ByteVector =
        varInt.encode(value.allianceId)
    }
}
