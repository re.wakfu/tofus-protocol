package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectUseMultipleMessage(
  objectUID: Int,
  quantity: Int
) extends Message(6234)

object ObjectUseMultipleMessage {
  implicit val codec: Codec[ObjectUseMultipleMessage] =
    new Codec[ObjectUseMultipleMessage] {
      def decode: Get[ObjectUseMultipleMessage] =
        for {
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ObjectUseMultipleMessage(objectUID, quantity)

      def encode(value: ObjectUseMultipleMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
