package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDeathMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double
) extends Message(1099)

object GameActionFightDeathMessage {
  implicit val codec: Codec[GameActionFightDeathMessage] =
    new Codec[GameActionFightDeathMessage] {
      def decode: Get[GameActionFightDeathMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
        } yield GameActionFightDeathMessage(actionId, sourceId, targetId)

      def encode(value: GameActionFightDeathMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId)
    }
}
