package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightersInformation(
  subAreaId: Short,
  waitingForHelpInfo: ProtectedEntityWaitingForHelpInfo,
  allyCharactersInformations: List[CharacterMinimalPlusLookInformations],
  enemyCharactersInformations: List[CharacterMinimalPlusLookInformations]
) extends ProtocolType {
  override val protocolId = 443
}

object PrismFightersInformation {
  implicit val codec: Codec[PrismFightersInformation] =
    new Codec[PrismFightersInformation] {
      def decode: Get[PrismFightersInformation] =
        for {
          subAreaId <- varShort.decode
          waitingForHelpInfo <- Codec[ProtectedEntityWaitingForHelpInfo].decode
          allyCharactersInformations <- list(ushort, Codec[CharacterMinimalPlusLookInformations]).decode
          enemyCharactersInformations <- list(ushort, Codec[CharacterMinimalPlusLookInformations]).decode
        } yield PrismFightersInformation(subAreaId, waitingForHelpInfo, allyCharactersInformations, enemyCharactersInformations)

      def encode(value: PrismFightersInformation): ByteVector =
        varShort.encode(value.subAreaId) ++
        Codec[ProtectedEntityWaitingForHelpInfo].encode(value.waitingForHelpInfo) ++
        list(ushort, Codec[CharacterMinimalPlusLookInformations]).encode(value.allyCharactersInformations) ++
        list(ushort, Codec[CharacterMinimalPlusLookInformations]).encode(value.enemyCharactersInformations)
    }
}
