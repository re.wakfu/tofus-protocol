package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccountHouseInformations(
  houseId: Int,
  modelId: Short,
  houseInfos: HouseInstanceInformations,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short
) extends HouseInformations {
  override val protocolId = 390
}

object AccountHouseInformations {
  implicit val codec: Codec[AccountHouseInformations] =
    new Codec[AccountHouseInformations] {
      def decode: Get[AccountHouseInformations] =
        for {
          houseId <- varInt.decode
          modelId <- varShort.decode
          houseInfos <- Codec[HouseInstanceInformations].decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
        } yield AccountHouseInformations(houseId, modelId, houseInfos, worldX, worldY, mapId, subAreaId)

      def encode(value: AccountHouseInformations): ByteVector =
        varInt.encode(value.houseId) ++
        varShort.encode(value.modelId) ++
        Codec[HouseInstanceInformations].encode(value.houseInfos) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId)
    }
}
