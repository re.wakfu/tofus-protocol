package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportBuddiesMessage(
  dungeonId: Short
) extends Message(6289)

object TeleportBuddiesMessage {
  implicit val codec: Codec[TeleportBuddiesMessage] =
    new Codec[TeleportBuddiesMessage] {
      def decode: Get[TeleportBuddiesMessage] =
        for {
          dungeonId <- varShort.decode
        } yield TeleportBuddiesMessage(dungeonId)

      def encode(value: TeleportBuddiesMessage): ByteVector =
        varShort.encode(value.dungeonId)
    }
}
