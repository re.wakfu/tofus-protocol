package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyLocateMembersRequestMessage(
  partyId: Int
) extends Message(5587)

object PartyLocateMembersRequestMessage {
  implicit val codec: Codec[PartyLocateMembersRequestMessage] =
    new Codec[PartyLocateMembersRequestMessage] {
      def decode: Get[PartyLocateMembersRequestMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyLocateMembersRequestMessage(partyId)

      def encode(value: PartyLocateMembersRequestMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
