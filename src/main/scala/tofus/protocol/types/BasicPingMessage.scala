package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicPingMessage(
  quiet: Boolean
) extends Message(182)

object BasicPingMessage {
  implicit val codec: Codec[BasicPingMessage] =
    new Codec[BasicPingMessage] {
      def decode: Get[BasicPingMessage] =
        for {
          quiet <- bool.decode
        } yield BasicPingMessage(quiet)

      def encode(value: BasicPingMessage): ByteVector =
        bool.encode(value.quiet)
    }
}
