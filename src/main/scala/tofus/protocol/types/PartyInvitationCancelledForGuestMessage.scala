package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationCancelledForGuestMessage(
  partyId: Int,
  cancelerId: Long
) extends Message(6256)

object PartyInvitationCancelledForGuestMessage {
  implicit val codec: Codec[PartyInvitationCancelledForGuestMessage] =
    new Codec[PartyInvitationCancelledForGuestMessage] {
      def decode: Get[PartyInvitationCancelledForGuestMessage] =
        for {
          partyId <- varInt.decode
          cancelerId <- varLong.decode
        } yield PartyInvitationCancelledForGuestMessage(partyId, cancelerId)

      def encode(value: PartyInvitationCancelledForGuestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.cancelerId)
    }
}
