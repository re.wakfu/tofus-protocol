package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NetworkDataContainerMessage(

) extends Message(2)

object NetworkDataContainerMessage {
  implicit val codec: Codec[NetworkDataContainerMessage] =
    Codec.const(NetworkDataContainerMessage())
}
