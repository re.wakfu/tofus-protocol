package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyLoyaltyStatusMessage(
  partyId: Int,
  loyal: Boolean
) extends Message(6270)

object PartyLoyaltyStatusMessage {
  implicit val codec: Codec[PartyLoyaltyStatusMessage] =
    new Codec[PartyLoyaltyStatusMessage] {
      def decode: Get[PartyLoyaltyStatusMessage] =
        for {
          partyId <- varInt.decode
          loyal <- bool.decode
        } yield PartyLoyaltyStatusMessage(partyId, loyal)

      def encode(value: PartyLoyaltyStatusMessage): ByteVector =
        varInt.encode(value.partyId) ++
        bool.encode(value.loyal)
    }
}
