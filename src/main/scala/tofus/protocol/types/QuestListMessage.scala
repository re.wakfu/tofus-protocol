package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestListMessage(
  finishedQuestsIds: List[Short],
  finishedQuestsCounts: List[Short],
  activeQuests: List[QuestActiveInformations],
  reinitDoneQuestsIds: List[Short]
) extends Message(5626)

object QuestListMessage {
  implicit val codec: Codec[QuestListMessage] =
    new Codec[QuestListMessage] {
      def decode: Get[QuestListMessage] =
        for {
          finishedQuestsIds <- list(ushort, varShort).decode
          finishedQuestsCounts <- list(ushort, varShort).decode
          activeQuests <- list(ushort, Codec[QuestActiveInformations]).decode
          reinitDoneQuestsIds <- list(ushort, varShort).decode
        } yield QuestListMessage(finishedQuestsIds, finishedQuestsCounts, activeQuests, reinitDoneQuestsIds)

      def encode(value: QuestListMessage): ByteVector =
        list(ushort, varShort).encode(value.finishedQuestsIds) ++
        list(ushort, varShort).encode(value.finishedQuestsCounts) ++
        list(ushort, Codec[QuestActiveInformations]).encode(value.activeQuests) ++
        list(ushort, varShort).encode(value.reinitDoneQuestsIds)
    }
}
