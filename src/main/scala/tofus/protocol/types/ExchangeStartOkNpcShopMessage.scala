package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkNpcShopMessage(
  npcSellerId: Double,
  tokenId: Short,
  objectsInfos: List[ObjectItemToSellInNpcShop]
) extends Message(5761)

object ExchangeStartOkNpcShopMessage {
  implicit val codec: Codec[ExchangeStartOkNpcShopMessage] =
    new Codec[ExchangeStartOkNpcShopMessage] {
      def decode: Get[ExchangeStartOkNpcShopMessage] =
        for {
          npcSellerId <- double.decode
          tokenId <- varShort.decode
          objectsInfos <- list(ushort, Codec[ObjectItemToSellInNpcShop]).decode
        } yield ExchangeStartOkNpcShopMessage(npcSellerId, tokenId, objectsInfos)

      def encode(value: ExchangeStartOkNpcShopMessage): ByteVector =
        double.encode(value.npcSellerId) ++
        varShort.encode(value.tokenId) ++
        list(ushort, Codec[ObjectItemToSellInNpcShop]).encode(value.objectsInfos)
    }
}
