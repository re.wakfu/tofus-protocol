package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCreationRequestMessage(
  name: String,
  breed: Byte,
  sex: Boolean,
  colors: List[Int],
  cosmeticId: Short
) extends Message(160)

object CharacterCreationRequestMessage {
  implicit val codec: Codec[CharacterCreationRequestMessage] =
    new Codec[CharacterCreationRequestMessage] {
      def decode: Get[CharacterCreationRequestMessage] =
        for {
          name <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
          colors <- listC(5, int).decode
          cosmeticId <- varShort.decode
        } yield CharacterCreationRequestMessage(name, breed, sex, colors, cosmeticId)

      def encode(value: CharacterCreationRequestMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        listC(5, int).encode(value.colors) ++
        varShort.encode(value.cosmeticId)
    }
}
