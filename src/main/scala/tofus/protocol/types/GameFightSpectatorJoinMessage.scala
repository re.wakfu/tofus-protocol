package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightSpectatorJoinMessage(
  flags0: Byte,
  timeMaxBeforeFightStart: Short,
  fightType: Byte,
  namedPartyTeams: List[NamedPartyTeam]
) extends Message(6504)

object GameFightSpectatorJoinMessage {
  implicit val codec: Codec[GameFightSpectatorJoinMessage] =
    new Codec[GameFightSpectatorJoinMessage] {
      def decode: Get[GameFightSpectatorJoinMessage] =
        for {
          flags0 <- byte.decode
          timeMaxBeforeFightStart <- short.decode
          fightType <- byte.decode
          namedPartyTeams <- list(ushort, Codec[NamedPartyTeam]).decode
        } yield GameFightSpectatorJoinMessage(flags0, timeMaxBeforeFightStart, fightType, namedPartyTeams)

      def encode(value: GameFightSpectatorJoinMessage): ByteVector =
        byte.encode(value.flags0) ++
        short.encode(value.timeMaxBeforeFightStart) ++
        byte.encode(value.fightType) ++
        list(ushort, Codec[NamedPartyTeam]).encode(value.namedPartyTeams)
    }
}
