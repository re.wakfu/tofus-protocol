package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StatisticDataString(
  value: String
) extends StatisticData {
  override val protocolId = 487
}

object StatisticDataString {
  implicit val codec: Codec[StatisticDataString] =
    new Codec[StatisticDataString] {
      def decode: Get[StatisticDataString] =
        for {
          value <- utf8(ushort).decode
        } yield StatisticDataString(value)

      def encode(value: StatisticDataString): ByteVector =
        utf8(ushort).encode(value.value)
    }
}
