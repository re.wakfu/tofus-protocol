package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorLootInformations(
  kamas: Long,
  experience: Long,
  pods: Int,
  itemsValue: Long
) extends TaxCollectorComplementaryInformations {
  override val protocolId = 372
}

object TaxCollectorLootInformations {
  implicit val codec: Codec[TaxCollectorLootInformations] =
    new Codec[TaxCollectorLootInformations] {
      def decode: Get[TaxCollectorLootInformations] =
        for {
          kamas <- varLong.decode
          experience <- varLong.decode
          pods <- varInt.decode
          itemsValue <- varLong.decode
        } yield TaxCollectorLootInformations(kamas, experience, pods, itemsValue)

      def encode(value: TaxCollectorLootInformations): ByteVector =
        varLong.encode(value.kamas) ++
        varLong.encode(value.experience) ++
        varInt.encode(value.pods) ++
        varLong.encode(value.itemsValue)
    }
}
