package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PresetUseResultMessage(
  presetId: Short,
  code: Byte
) extends Message(6747)

object PresetUseResultMessage {
  implicit val codec: Codec[PresetUseResultMessage] =
    new Codec[PresetUseResultMessage] {
      def decode: Get[PresetUseResultMessage] =
        for {
          presetId <- short.decode
          code <- byte.decode
        } yield PresetUseResultMessage(presetId, code)

      def encode(value: PresetUseResultMessage): ByteVector =
        short.encode(value.presetId) ++
        byte.encode(value.code)
    }
}
