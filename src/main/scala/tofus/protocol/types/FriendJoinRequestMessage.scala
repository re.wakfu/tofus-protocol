package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendJoinRequestMessage(
  name: String
) extends Message(5605)

object FriendJoinRequestMessage {
  implicit val codec: Codec[FriendJoinRequestMessage] =
    new Codec[FriendJoinRequestMessage] {
      def decode: Get[FriendJoinRequestMessage] =
        for {
          name <- utf8(ushort).decode
        } yield FriendJoinRequestMessage(name)

      def encode(value: FriendJoinRequestMessage): ByteVector =
        utf8(ushort).encode(value.name)
    }
}
