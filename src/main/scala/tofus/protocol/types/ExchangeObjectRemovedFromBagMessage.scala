package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectRemovedFromBagMessage(
  remote: Boolean,
  objectUID: Int
) extends Message(6010)

object ExchangeObjectRemovedFromBagMessage {
  implicit val codec: Codec[ExchangeObjectRemovedFromBagMessage] =
    new Codec[ExchangeObjectRemovedFromBagMessage] {
      def decode: Get[ExchangeObjectRemovedFromBagMessage] =
        for {
          remote <- bool.decode
          objectUID <- varInt.decode
        } yield ExchangeObjectRemovedFromBagMessage(remote, objectUID)

      def encode(value: ExchangeObjectRemovedFromBagMessage): ByteVector =
        bool.encode(value.remote) ++
        varInt.encode(value.objectUID)
    }
}
