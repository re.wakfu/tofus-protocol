package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemNotInContainer(
  objectGID: Short,
  effects: List[ObjectEffect],
  objectUID: Int,
  quantity: Int
) extends Item {
  override val protocolId = 134
}

object ObjectItemNotInContainer {
  implicit val codec: Codec[ObjectItemNotInContainer] =
    new Codec[ObjectItemNotInContainer] {
      def decode: Get[ObjectItemNotInContainer] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          objectUID <- varInt.decode
          quantity <- varInt.decode
        } yield ObjectItemNotInContainer(objectGID, effects, objectUID, quantity)

      def encode(value: ObjectItemNotInContainer): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varInt.encode(value.objectUID) ++
        varInt.encode(value.quantity)
    }
}
