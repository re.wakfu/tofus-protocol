package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccountHouseMessage(
  houses: List[AccountHouseInformations]
) extends Message(6315)

object AccountHouseMessage {
  implicit val codec: Codec[AccountHouseMessage] =
    new Codec[AccountHouseMessage] {
      def decode: Get[AccountHouseMessage] =
        for {
          houses <- list(ushort, Codec[AccountHouseInformations]).decode
        } yield AccountHouseMessage(houses)

      def encode(value: AccountHouseMessage): ByteVector =
        list(ushort, Codec[AccountHouseInformations]).encode(value.houses)
    }
}
