package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PaddockInformations extends ProtocolType

final case class ConcretePaddockInformations(
  maxOutdoorMount: Short,
  maxItems: Short
) extends PaddockInformations {
  override val protocolId = 132
}

object ConcretePaddockInformations {
  implicit val codec: Codec[ConcretePaddockInformations] =  
    new Codec[ConcretePaddockInformations] {
      def decode: Get[ConcretePaddockInformations] =
        for {
          maxOutdoorMount <- varShort.decode
          maxItems <- varShort.decode
        } yield ConcretePaddockInformations(maxOutdoorMount, maxItems)

      def encode(value: ConcretePaddockInformations): ByteVector =
        varShort.encode(value.maxOutdoorMount) ++
        varShort.encode(value.maxItems)
    }
}

object PaddockInformations {
  implicit val codec: Codec[PaddockInformations] =
    new Codec[PaddockInformations] {
      def decode: Get[PaddockInformations] =
        ushort.decode.flatMap {
          case 132 => Codec[ConcretePaddockInformations].decode
          case 183 => Codec[PaddockContentInformations].decode
          case 509 => Codec[PaddockInstancesInformations].decode
        }

      def encode(value: PaddockInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePaddockInformations => Codec[ConcretePaddockInformations].encode(i)
          case i: PaddockContentInformations => Codec[PaddockContentInformations].encode(i)
          case i: PaddockInstancesInformations => Codec[PaddockInstancesInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
