package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobBookSubscribeRequestMessage(
  jobIds: ByteVector
) extends Message(6592)

object JobBookSubscribeRequestMessage {
  implicit val codec: Codec[JobBookSubscribeRequestMessage] =
    new Codec[JobBookSubscribeRequestMessage] {
      def decode: Get[JobBookSubscribeRequestMessage] =
        for {
          jobIds <- bytes(ushort).decode
        } yield JobBookSubscribeRequestMessage(jobIds)

      def encode(value: JobBookSubscribeRequestMessage): ByteVector =
        bytes(ushort).encode(value.jobIds)
    }
}
