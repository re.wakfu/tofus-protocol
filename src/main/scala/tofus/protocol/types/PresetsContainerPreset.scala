package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PresetsContainerPreset extends Preset

final case class ConcretePresetsContainerPreset(
  id: Short,
  presets: List[Preset]
) extends PresetsContainerPreset {
  override val protocolId = 520
}

object ConcretePresetsContainerPreset {
  implicit val codec: Codec[ConcretePresetsContainerPreset] =  
    new Codec[ConcretePresetsContainerPreset] {
      def decode: Get[ConcretePresetsContainerPreset] =
        for {
          id <- short.decode
          presets <- list(ushort, Codec[Preset]).decode
        } yield ConcretePresetsContainerPreset(id, presets)

      def encode(value: ConcretePresetsContainerPreset): ByteVector =
        short.encode(value.id) ++
        list(ushort, Codec[Preset]).encode(value.presets)
    }
}

object PresetsContainerPreset {
  implicit val codec: Codec[PresetsContainerPreset] =
    new Codec[PresetsContainerPreset] {
      def decode: Get[PresetsContainerPreset] =
        ushort.decode.flatMap {
          case 520 => Codec[ConcretePresetsContainerPreset].decode
          case 534 => Codec[CharacterBuildPreset].decode
        }

      def encode(value: PresetsContainerPreset): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePresetsContainerPreset => Codec[ConcretePresetsContainerPreset].encode(i)
          case i: CharacterBuildPreset => Codec[CharacterBuildPreset].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
