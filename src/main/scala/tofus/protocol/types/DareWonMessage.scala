package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareWonMessage(
  dareId: Double
) extends Message(6681)

object DareWonMessage {
  implicit val codec: Codec[DareWonMessage] =
    new Codec[DareWonMessage] {
      def decode: Get[DareWonMessage] =
        for {
          dareId <- double.decode
        } yield DareWonMessage(dareId)

      def encode(value: DareWonMessage): ByteVector =
        double.encode(value.dareId)
    }
}
