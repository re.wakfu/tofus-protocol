package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaFightAnswerMessage(
  fightId: Short,
  accept: Boolean
) extends Message(6279)

object GameRolePlayArenaFightAnswerMessage {
  implicit val codec: Codec[GameRolePlayArenaFightAnswerMessage] =
    new Codec[GameRolePlayArenaFightAnswerMessage] {
      def decode: Get[GameRolePlayArenaFightAnswerMessage] =
        for {
          fightId <- varShort.decode
          accept <- bool.decode
        } yield GameRolePlayArenaFightAnswerMessage(fightId, accept)

      def encode(value: GameRolePlayArenaFightAnswerMessage): ByteVector =
        varShort.encode(value.fightId) ++
        bool.encode(value.accept)
    }
}
