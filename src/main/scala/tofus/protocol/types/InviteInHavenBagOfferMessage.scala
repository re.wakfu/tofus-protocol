package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InviteInHavenBagOfferMessage(
  hostInformations: ConcreteCharacterMinimalInformations,
  timeLeftBeforeCancel: Int
) extends Message(6643)

object InviteInHavenBagOfferMessage {
  implicit val codec: Codec[InviteInHavenBagOfferMessage] =
    new Codec[InviteInHavenBagOfferMessage] {
      def decode: Get[InviteInHavenBagOfferMessage] =
        for {
          hostInformations <- Codec[ConcreteCharacterMinimalInformations].decode
          timeLeftBeforeCancel <- varInt.decode
        } yield InviteInHavenBagOfferMessage(hostInformations, timeLeftBeforeCancel)

      def encode(value: InviteInHavenBagOfferMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.hostInformations) ++
        varInt.encode(value.timeLeftBeforeCancel)
    }
}
