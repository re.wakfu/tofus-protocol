package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagPermissionsUpdateRequestMessage(
  permissions: Int
) extends Message(6714)

object HavenBagPermissionsUpdateRequestMessage {
  implicit val codec: Codec[HavenBagPermissionsUpdateRequestMessage] =
    new Codec[HavenBagPermissionsUpdateRequestMessage] {
      def decode: Get[HavenBagPermissionsUpdateRequestMessage] =
        for {
          permissions <- int.decode
        } yield HavenBagPermissionsUpdateRequestMessage(permissions)

      def encode(value: HavenBagPermissionsUpdateRequestMessage): ByteVector =
        int.encode(value.permissions)
    }
}
