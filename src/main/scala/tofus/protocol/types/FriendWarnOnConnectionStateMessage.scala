package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendWarnOnConnectionStateMessage(
  enable: Boolean
) extends Message(5630)

object FriendWarnOnConnectionStateMessage {
  implicit val codec: Codec[FriendWarnOnConnectionStateMessage] =
    new Codec[FriendWarnOnConnectionStateMessage] {
      def decode: Get[FriendWarnOnConnectionStateMessage] =
        for {
          enable <- bool.decode
        } yield FriendWarnOnConnectionStateMessage(enable)

      def encode(value: FriendWarnOnConnectionStateMessage): ByteVector =
        bool.encode(value.enable)
    }
}
