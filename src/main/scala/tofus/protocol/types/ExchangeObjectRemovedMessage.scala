package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectRemovedMessage(
  remote: Boolean,
  objectUID: Int
) extends Message(5517)

object ExchangeObjectRemovedMessage {
  implicit val codec: Codec[ExchangeObjectRemovedMessage] =
    new Codec[ExchangeObjectRemovedMessage] {
      def decode: Get[ExchangeObjectRemovedMessage] =
        for {
          remote <- bool.decode
          objectUID <- varInt.decode
        } yield ExchangeObjectRemovedMessage(remote, objectUID)

      def encode(value: ExchangeObjectRemovedMessage): ByteVector =
        bool.encode(value.remote) ++
        varInt.encode(value.objectUID)
    }
}
