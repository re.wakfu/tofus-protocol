package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatSmileyMessage(
  entityId: Double,
  smileyId: Short,
  accountId: Int
) extends Message(801)

object ChatSmileyMessage {
  implicit val codec: Codec[ChatSmileyMessage] =
    new Codec[ChatSmileyMessage] {
      def decode: Get[ChatSmileyMessage] =
        for {
          entityId <- double.decode
          smileyId <- varShort.decode
          accountId <- int.decode
        } yield ChatSmileyMessage(entityId, smileyId, accountId)

      def encode(value: ChatSmileyMessage): ByteVector =
        double.encode(value.entityId) ++
        varShort.encode(value.smileyId) ++
        int.encode(value.accountId)
    }
}
