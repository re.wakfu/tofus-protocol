package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccountLoggingKickedMessage(
  days: Short,
  hours: Byte,
  minutes: Byte
) extends Message(6029)

object AccountLoggingKickedMessage {
  implicit val codec: Codec[AccountLoggingKickedMessage] =
    new Codec[AccountLoggingKickedMessage] {
      def decode: Get[AccountLoggingKickedMessage] =
        for {
          days <- varShort.decode
          hours <- byte.decode
          minutes <- byte.decode
        } yield AccountLoggingKickedMessage(days, hours, minutes)

      def encode(value: AccountLoggingKickedMessage): ByteVector =
        varShort.encode(value.days) ++
        byte.encode(value.hours) ++
        byte.encode(value.minutes)
    }
}
