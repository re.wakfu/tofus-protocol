package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AllianceInformations extends BasicNamedAllianceInformations

final case class ConcreteAllianceInformations(
  allianceId: Int,
  allianceTag: String,
  allianceName: String,
  allianceEmblem: GuildEmblem
) extends AllianceInformations {
  override val protocolId = 417
}

object ConcreteAllianceInformations {
  implicit val codec: Codec[ConcreteAllianceInformations] =  
    new Codec[ConcreteAllianceInformations] {
      def decode: Get[ConcreteAllianceInformations] =
        for {
          allianceId <- varInt.decode
          allianceTag <- utf8(ushort).decode
          allianceName <- utf8(ushort).decode
          allianceEmblem <- Codec[GuildEmblem].decode
        } yield ConcreteAllianceInformations(allianceId, allianceTag, allianceName, allianceEmblem)

      def encode(value: ConcreteAllianceInformations): ByteVector =
        varInt.encode(value.allianceId) ++
        utf8(ushort).encode(value.allianceTag) ++
        utf8(ushort).encode(value.allianceName) ++
        Codec[GuildEmblem].encode(value.allianceEmblem)
    }
}

object AllianceInformations {
  implicit val codec: Codec[AllianceInformations] =
    new Codec[AllianceInformations] {
      def decode: Get[AllianceInformations] =
        ushort.decode.flatMap {
          case 417 => Codec[ConcreteAllianceInformations].decode
          case 421 => Codec[AllianceFactSheetInformations].decode
        }

      def encode(value: AllianceInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAllianceInformations => Codec[ConcreteAllianceInformations].encode(i)
          case i: AllianceFactSheetInformations => Codec[AllianceFactSheetInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
