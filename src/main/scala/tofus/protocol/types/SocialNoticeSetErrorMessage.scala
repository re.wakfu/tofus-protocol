package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SocialNoticeSetErrorMessage(
  reason: Byte
) extends Message(6684)

object SocialNoticeSetErrorMessage {
  implicit val codec: Codec[SocialNoticeSetErrorMessage] =
    new Codec[SocialNoticeSetErrorMessage] {
      def decode: Get[SocialNoticeSetErrorMessage] =
        for {
          reason <- byte.decode
        } yield SocialNoticeSetErrorMessage(reason)

      def encode(value: SocialNoticeSetErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
