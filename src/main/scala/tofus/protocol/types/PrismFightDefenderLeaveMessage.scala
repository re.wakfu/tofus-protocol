package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightDefenderLeaveMessage(
  subAreaId: Short,
  fightId: Short,
  fighterToRemoveId: Long
) extends Message(5892)

object PrismFightDefenderLeaveMessage {
  implicit val codec: Codec[PrismFightDefenderLeaveMessage] =
    new Codec[PrismFightDefenderLeaveMessage] {
      def decode: Get[PrismFightDefenderLeaveMessage] =
        for {
          subAreaId <- varShort.decode
          fightId <- varShort.decode
          fighterToRemoveId <- varLong.decode
        } yield PrismFightDefenderLeaveMessage(subAreaId, fightId, fighterToRemoveId)

      def encode(value: PrismFightDefenderLeaveMessage): ByteVector =
        varShort.encode(value.subAreaId) ++
        varShort.encode(value.fightId) ++
        varLong.encode(value.fighterToRemoveId)
    }
}
