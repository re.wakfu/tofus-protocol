package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangePodsModifiedMessage(
  remote: Boolean,
  currentWeight: Int,
  maxWeight: Int
) extends Message(6670)

object ExchangePodsModifiedMessage {
  implicit val codec: Codec[ExchangePodsModifiedMessage] =
    new Codec[ExchangePodsModifiedMessage] {
      def decode: Get[ExchangePodsModifiedMessage] =
        for {
          remote <- bool.decode
          currentWeight <- varInt.decode
          maxWeight <- varInt.decode
        } yield ExchangePodsModifiedMessage(remote, currentWeight, maxWeight)

      def encode(value: ExchangePodsModifiedMessage): ByteVector =
        bool.encode(value.remote) ++
        varInt.encode(value.currentWeight) ++
        varInt.encode(value.maxWeight)
    }
}
