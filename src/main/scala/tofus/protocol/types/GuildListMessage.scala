package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildListMessage(
  guilds: List[ConcreteGuildInformations]
) extends Message(6413)

object GuildListMessage {
  implicit val codec: Codec[GuildListMessage] =
    new Codec[GuildListMessage] {
      def decode: Get[GuildListMessage] =
        for {
          guilds <- list(ushort, Codec[ConcreteGuildInformations]).decode
        } yield GuildListMessage(guilds)

      def encode(value: GuildListMessage): ByteVector =
        list(ushort, Codec[ConcreteGuildInformations]).encode(value.guilds)
    }
}
