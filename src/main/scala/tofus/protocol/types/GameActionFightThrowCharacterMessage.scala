package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightThrowCharacterMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  cellId: Short
) extends Message(5829)

object GameActionFightThrowCharacterMessage {
  implicit val codec: Codec[GameActionFightThrowCharacterMessage] =
    new Codec[GameActionFightThrowCharacterMessage] {
      def decode: Get[GameActionFightThrowCharacterMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          cellId <- short.decode
        } yield GameActionFightThrowCharacterMessage(actionId, sourceId, targetId, cellId)

      def encode(value: GameActionFightThrowCharacterMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.cellId)
    }
}
