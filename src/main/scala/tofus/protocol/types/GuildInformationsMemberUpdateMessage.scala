package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildInformationsMemberUpdateMessage(
  member: GuildMember
) extends Message(5597)

object GuildInformationsMemberUpdateMessage {
  implicit val codec: Codec[GuildInformationsMemberUpdateMessage] =
    new Codec[GuildInformationsMemberUpdateMessage] {
      def decode: Get[GuildInformationsMemberUpdateMessage] =
        for {
          member <- Codec[GuildMember].decode
        } yield GuildInformationsMemberUpdateMessage(member)

      def encode(value: GuildInformationsMemberUpdateMessage): ByteVector =
        Codec[GuildMember].encode(value.member)
    }
}
