package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkRecycleTradeMessage(
  percentToPrism: Short,
  percentToPlayer: Short
) extends Message(6600)

object ExchangeStartOkRecycleTradeMessage {
  implicit val codec: Codec[ExchangeStartOkRecycleTradeMessage] =
    new Codec[ExchangeStartOkRecycleTradeMessage] {
      def decode: Get[ExchangeStartOkRecycleTradeMessage] =
        for {
          percentToPrism <- short.decode
          percentToPlayer <- short.decode
        } yield ExchangeStartOkRecycleTradeMessage(percentToPrism, percentToPlayer)

      def encode(value: ExchangeStartOkRecycleTradeMessage): ByteVector =
        short.encode(value.percentToPrism) ++
        short.encode(value.percentToPlayer)
    }
}
