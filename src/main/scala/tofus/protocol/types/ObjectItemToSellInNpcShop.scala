package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectItemToSellInNpcShop(
  objectGID: Short,
  effects: List[ObjectEffect],
  objectPrice: Long,
  buyCriterion: String
) extends ObjectItemMinimalInformation {
  override val protocolId = 352
}

object ObjectItemToSellInNpcShop {
  implicit val codec: Codec[ObjectItemToSellInNpcShop] =
    new Codec[ObjectItemToSellInNpcShop] {
      def decode: Get[ObjectItemToSellInNpcShop] =
        for {
          objectGID <- varShort.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          objectPrice <- varLong.decode
          buyCriterion <- utf8(ushort).decode
        } yield ObjectItemToSellInNpcShop(objectGID, effects, objectPrice, buyCriterion)

      def encode(value: ObjectItemToSellInNpcShop): ByteVector =
        varShort.encode(value.objectGID) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        varLong.encode(value.objectPrice) ++
        utf8(ushort).encode(value.buyCriterion)
    }
}
