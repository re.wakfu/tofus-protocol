package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterMinimalPlusLookAndGradeInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  grade: Int
) extends CharacterMinimalPlusLookInformations {
  override val protocolId = 193
}

object CharacterMinimalPlusLookAndGradeInformations {
  implicit val codec: Codec[CharacterMinimalPlusLookAndGradeInformations] =
    new Codec[CharacterMinimalPlusLookAndGradeInformations] {
      def decode: Get[CharacterMinimalPlusLookAndGradeInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          grade <- varInt.decode
        } yield CharacterMinimalPlusLookAndGradeInformations(id, name, level, entityLook, breed, grade)

      def encode(value: CharacterMinimalPlusLookAndGradeInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        varInt.encode(value.grade)
    }
}
