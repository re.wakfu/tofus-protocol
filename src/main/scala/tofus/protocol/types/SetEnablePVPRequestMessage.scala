package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SetEnablePVPRequestMessage(
  enable: Boolean
) extends Message(1810)

object SetEnablePVPRequestMessage {
  implicit val codec: Codec[SetEnablePVPRequestMessage] =
    new Codec[SetEnablePVPRequestMessage] {
      def decode: Get[SetEnablePVPRequestMessage] =
        for {
          enable <- bool.decode
        } yield SetEnablePVPRequestMessage(enable)

      def encode(value: SetEnablePVPRequestMessage): ByteVector =
        bool.encode(value.enable)
    }
}
