package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachSavedMessage(
  saved: Boolean
) extends Message(6798)

object BreachSavedMessage {
  implicit val codec: Codec[BreachSavedMessage] =
    new Codec[BreachSavedMessage] {
      def decode: Get[BreachSavedMessage] =
        for {
          saved <- bool.decode
        } yield BreachSavedMessage(saved)

      def encode(value: BreachSavedMessage): ByteVector =
        bool.encode(value.saved)
    }
}
