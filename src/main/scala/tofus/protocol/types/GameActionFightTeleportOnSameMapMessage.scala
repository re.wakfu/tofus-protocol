package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightTeleportOnSameMapMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  cellId: Short
) extends Message(5528)

object GameActionFightTeleportOnSameMapMessage {
  implicit val codec: Codec[GameActionFightTeleportOnSameMapMessage] =
    new Codec[GameActionFightTeleportOnSameMapMessage] {
      def decode: Get[GameActionFightTeleportOnSameMapMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          cellId <- short.decode
        } yield GameActionFightTeleportOnSameMapMessage(actionId, sourceId, targetId, cellId)

      def encode(value: GameActionFightTeleportOnSameMapMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.cellId)
    }
}
