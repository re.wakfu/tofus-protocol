package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractGameActionFightTargetedAbilityMessage(
  actionId: Short,
  sourceId: Double,
  flags0: Byte,
  targetId: Double,
  destinationCellId: Short,
  critical: Byte
) extends Message(6118)

object AbstractGameActionFightTargetedAbilityMessage {
  implicit val codec: Codec[AbstractGameActionFightTargetedAbilityMessage] =
    new Codec[AbstractGameActionFightTargetedAbilityMessage] {
      def decode: Get[AbstractGameActionFightTargetedAbilityMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          flags0 <- byte.decode
          targetId <- double.decode
          destinationCellId <- short.decode
          critical <- byte.decode
        } yield AbstractGameActionFightTargetedAbilityMessage(actionId, sourceId, flags0, targetId, destinationCellId, critical)

      def encode(value: AbstractGameActionFightTargetedAbilityMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        byte.encode(value.flags0) ++
        double.encode(value.targetId) ++
        short.encode(value.destinationCellId) ++
        byte.encode(value.critical)
    }
}
