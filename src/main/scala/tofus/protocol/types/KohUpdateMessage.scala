package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class KohUpdateMessage(
  alliances: List[ConcreteAllianceInformations],
  allianceNbMembers: List[Short],
  allianceRoundWeigth: List[Int],
  allianceMatchScore: ByteVector,
  allianceMapWinners: List[ConcreteBasicAllianceInformations],
  allianceMapWinnerScore: Int,
  allianceMapMyAllianceScore: Int,
  nextTickTime: Double
) extends Message(6439)

object KohUpdateMessage {
  implicit val codec: Codec[KohUpdateMessage] =
    new Codec[KohUpdateMessage] {
      def decode: Get[KohUpdateMessage] =
        for {
          alliances <- list(ushort, Codec[ConcreteAllianceInformations]).decode
          allianceNbMembers <- list(ushort, varShort).decode
          allianceRoundWeigth <- list(ushort, varInt).decode
          allianceMatchScore <- bytes(ushort).decode
          allianceMapWinners <- list(ushort, Codec[ConcreteBasicAllianceInformations]).decode
          allianceMapWinnerScore <- varInt.decode
          allianceMapMyAllianceScore <- varInt.decode
          nextTickTime <- double.decode
        } yield KohUpdateMessage(alliances, allianceNbMembers, allianceRoundWeigth, allianceMatchScore, allianceMapWinners, allianceMapWinnerScore, allianceMapMyAllianceScore, nextTickTime)

      def encode(value: KohUpdateMessage): ByteVector =
        list(ushort, Codec[ConcreteAllianceInformations]).encode(value.alliances) ++
        list(ushort, varShort).encode(value.allianceNbMembers) ++
        list(ushort, varInt).encode(value.allianceRoundWeigth) ++
        bytes(ushort).encode(value.allianceMatchScore) ++
        list(ushort, Codec[ConcreteBasicAllianceInformations]).encode(value.allianceMapWinners) ++
        varInt.encode(value.allianceMapWinnerScore) ++
        varInt.encode(value.allianceMapMyAllianceScore) ++
        double.encode(value.nextTickTime)
    }
}
