package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPrismInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  prism: PrismInformation
) extends GameRolePlayActorInformations {
  override val protocolId = 161
}

object GameRolePlayPrismInformations {
  implicit val codec: Codec[GameRolePlayPrismInformations] =
    new Codec[GameRolePlayPrismInformations] {
      def decode: Get[GameRolePlayPrismInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          prism <- Codec[PrismInformation].decode
        } yield GameRolePlayPrismInformations(contextualId, disposition, look, prism)

      def encode(value: GameRolePlayPrismInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[PrismInformation].encode(value.prism)
    }
}
