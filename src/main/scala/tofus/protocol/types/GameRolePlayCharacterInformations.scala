package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayCharacterInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  name: String,
  humanoidInfo: HumanInformations,
  accountId: Int,
  alignmentInfos: ConcreteActorAlignmentInformations
) extends GameRolePlayHumanoidInformations {
  override val protocolId = 36
}

object GameRolePlayCharacterInformations {
  implicit val codec: Codec[GameRolePlayCharacterInformations] =
    new Codec[GameRolePlayCharacterInformations] {
      def decode: Get[GameRolePlayCharacterInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          name <- utf8(ushort).decode
          humanoidInfo <- Codec[HumanInformations].decode
          accountId <- int.decode
          alignmentInfos <- Codec[ConcreteActorAlignmentInformations].decode
        } yield GameRolePlayCharacterInformations(contextualId, disposition, look, name, humanoidInfo, accountId, alignmentInfos)

      def encode(value: GameRolePlayCharacterInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        utf8(ushort).encode(value.name) ++
        Codec[HumanInformations].encode(value.humanoidInfo) ++
        int.encode(value.accountId) ++
        Codec[ConcreteActorAlignmentInformations].encode(value.alignmentInfos)
    }
}
