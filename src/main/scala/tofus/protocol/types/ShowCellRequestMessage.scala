package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShowCellRequestMessage(
  cellId: Short
) extends Message(5611)

object ShowCellRequestMessage {
  implicit val codec: Codec[ShowCellRequestMessage] =
    new Codec[ShowCellRequestMessage] {
      def decode: Get[ShowCellRequestMessage] =
        for {
          cellId <- varShort.decode
        } yield ShowCellRequestMessage(cellId)

      def encode(value: ShowCellRequestMessage): ByteVector =
        varShort.encode(value.cellId)
    }
}
