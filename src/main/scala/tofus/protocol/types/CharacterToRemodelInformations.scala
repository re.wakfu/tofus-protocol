package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterToRemodelInformations(
  id: Long,
  name: String,
  breed: Byte,
  sex: Boolean,
  cosmeticId: Short,
  colors: List[Int],
  possibleChangeMask: Byte,
  mandatoryChangeMask: Byte
) extends CharacterRemodelingInformation {
  override val protocolId = 477
}

object CharacterToRemodelInformations {
  implicit val codec: Codec[CharacterToRemodelInformations] =
    new Codec[CharacterToRemodelInformations] {
      def decode: Get[CharacterToRemodelInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
          cosmeticId <- varShort.decode
          colors <- list(ushort, int).decode
          possibleChangeMask <- byte.decode
          mandatoryChangeMask <- byte.decode
        } yield CharacterToRemodelInformations(id, name, breed, sex, cosmeticId, colors, possibleChangeMask, mandatoryChangeMask)

      def encode(value: CharacterToRemodelInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        varShort.encode(value.cosmeticId) ++
        list(ushort, int).encode(value.colors) ++
        byte.encode(value.possibleChangeMask) ++
        byte.encode(value.mandatoryChangeMask)
    }
}
