package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class WrapperObjectDissociateRequestMessage(
  hostUID: Int,
  hostPos: Short
) extends Message(6524)

object WrapperObjectDissociateRequestMessage {
  implicit val codec: Codec[WrapperObjectDissociateRequestMessage] =
    new Codec[WrapperObjectDissociateRequestMessage] {
      def decode: Get[WrapperObjectDissociateRequestMessage] =
        for {
          hostUID <- varInt.decode
          hostPos <- ubyte.decode
        } yield WrapperObjectDissociateRequestMessage(hostUID, hostPos)

      def encode(value: WrapperObjectDissociateRequestMessage): ByteVector =
        varInt.encode(value.hostUID) ++
        ubyte.encode(value.hostPos)
    }
}
