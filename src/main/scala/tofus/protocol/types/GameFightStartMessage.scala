package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightStartMessage(
  idols: List[ConcreteIdol]
) extends Message(712)

object GameFightStartMessage {
  implicit val codec: Codec[GameFightStartMessage] =
    new Codec[GameFightStartMessage] {
      def decode: Get[GameFightStartMessage] =
        for {
          idols <- list(ushort, Codec[ConcreteIdol]).decode
        } yield GameFightStartMessage(idols)

      def encode(value: GameFightStartMessage): ByteVector =
        list(ushort, Codec[ConcreteIdol]).encode(value.idols)
    }
}
