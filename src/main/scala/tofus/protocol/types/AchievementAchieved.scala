package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AchievementAchieved extends ProtocolType

final case class ConcreteAchievementAchieved(
  id: Short,
  achievedBy: Long
) extends AchievementAchieved {
  override val protocolId = 514
}

object ConcreteAchievementAchieved {
  implicit val codec: Codec[ConcreteAchievementAchieved] =  
    new Codec[ConcreteAchievementAchieved] {
      def decode: Get[ConcreteAchievementAchieved] =
        for {
          id <- varShort.decode
          achievedBy <- varLong.decode
        } yield ConcreteAchievementAchieved(id, achievedBy)

      def encode(value: ConcreteAchievementAchieved): ByteVector =
        varShort.encode(value.id) ++
        varLong.encode(value.achievedBy)
    }
}

object AchievementAchieved {
  implicit val codec: Codec[AchievementAchieved] =
    new Codec[AchievementAchieved] {
      def decode: Get[AchievementAchieved] =
        ushort.decode.flatMap {
          case 514 => Codec[ConcreteAchievementAchieved].decode
          case 515 => Codec[AchievementAchievedRewardable].decode
        }

      def encode(value: AchievementAchieved): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAchievementAchieved => Codec[ConcreteAchievementAchieved].encode(i)
          case i: AchievementAchievedRewardable => Codec[AchievementAchievedRewardable].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
