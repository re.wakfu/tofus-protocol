package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EnterHavenBagRequestMessage(
  havenBagOwner: Long
) extends Message(6636)

object EnterHavenBagRequestMessage {
  implicit val codec: Codec[EnterHavenBagRequestMessage] =
    new Codec[EnterHavenBagRequestMessage] {
      def decode: Get[EnterHavenBagRequestMessage] =
        for {
          havenBagOwner <- varLong.decode
        } yield EnterHavenBagRequestMessage(havenBagOwner)

      def encode(value: EnterHavenBagRequestMessage): ByteVector =
        varLong.encode(value.havenBagOwner)
    }
}
