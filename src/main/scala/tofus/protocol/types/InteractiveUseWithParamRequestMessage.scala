package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveUseWithParamRequestMessage(
  elemId: Int,
  skillInstanceUid: Int,
  id: Int
) extends Message(6715)

object InteractiveUseWithParamRequestMessage {
  implicit val codec: Codec[InteractiveUseWithParamRequestMessage] =
    new Codec[InteractiveUseWithParamRequestMessage] {
      def decode: Get[InteractiveUseWithParamRequestMessage] =
        for {
          elemId <- varInt.decode
          skillInstanceUid <- varInt.decode
          id <- int.decode
        } yield InteractiveUseWithParamRequestMessage(elemId, skillInstanceUid, id)

      def encode(value: InteractiveUseWithParamRequestMessage): ByteVector =
        varInt.encode(value.elemId) ++
        varInt.encode(value.skillInstanceUid) ++
        int.encode(value.id)
    }
}
