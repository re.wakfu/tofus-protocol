package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MoodSmileyUpdateMessage(
  accountId: Int,
  playerId: Long,
  smileyId: Short
) extends Message(6388)

object MoodSmileyUpdateMessage {
  implicit val codec: Codec[MoodSmileyUpdateMessage] =
    new Codec[MoodSmileyUpdateMessage] {
      def decode: Get[MoodSmileyUpdateMessage] =
        for {
          accountId <- int.decode
          playerId <- varLong.decode
          smileyId <- varShort.decode
        } yield MoodSmileyUpdateMessage(accountId, playerId, smileyId)

      def encode(value: MoodSmileyUpdateMessage): ByteVector =
        int.encode(value.accountId) ++
        varLong.encode(value.playerId) ++
        varShort.encode(value.smileyId)
    }
}
