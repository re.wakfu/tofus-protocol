package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightTemporaryBoostWeaponDamagesEffect(
  uid: Int,
  targetId: Double,
  turnDuration: Short,
  dispelable: Byte,
  spellId: Short,
  effectId: Int,
  parentBoostUid: Int,
  delta: Int,
  weaponTypeId: Short
) extends FightTemporaryBoostEffect {
  override val protocolId = 211
}

object FightTemporaryBoostWeaponDamagesEffect {
  implicit val codec: Codec[FightTemporaryBoostWeaponDamagesEffect] =
    new Codec[FightTemporaryBoostWeaponDamagesEffect] {
      def decode: Get[FightTemporaryBoostWeaponDamagesEffect] =
        for {
          uid <- varInt.decode
          targetId <- double.decode
          turnDuration <- short.decode
          dispelable <- byte.decode
          spellId <- varShort.decode
          effectId <- varInt.decode
          parentBoostUid <- varInt.decode
          delta <- int.decode
          weaponTypeId <- short.decode
        } yield FightTemporaryBoostWeaponDamagesEffect(uid, targetId, turnDuration, dispelable, spellId, effectId, parentBoostUid, delta, weaponTypeId)

      def encode(value: FightTemporaryBoostWeaponDamagesEffect): ByteVector =
        varInt.encode(value.uid) ++
        double.encode(value.targetId) ++
        short.encode(value.turnDuration) ++
        byte.encode(value.dispelable) ++
        varShort.encode(value.spellId) ++
        varInt.encode(value.effectId) ++
        varInt.encode(value.parentBoostUid) ++
        int.encode(value.delta) ++
        short.encode(value.weaponTypeId)
    }
}
