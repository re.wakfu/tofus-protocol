package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorAttackedResultMessage(
  deadOrAlive: Boolean,
  basicInfos: TaxCollectorBasicInformations,
  guild: ConcreteBasicGuildInformations
) extends Message(5635)

object TaxCollectorAttackedResultMessage {
  implicit val codec: Codec[TaxCollectorAttackedResultMessage] =
    new Codec[TaxCollectorAttackedResultMessage] {
      def decode: Get[TaxCollectorAttackedResultMessage] =
        for {
          deadOrAlive <- bool.decode
          basicInfos <- Codec[TaxCollectorBasicInformations].decode
          guild <- Codec[ConcreteBasicGuildInformations].decode
        } yield TaxCollectorAttackedResultMessage(deadOrAlive, basicInfos, guild)

      def encode(value: TaxCollectorAttackedResultMessage): ByteVector =
        bool.encode(value.deadOrAlive) ++
        Codec[TaxCollectorBasicInformations].encode(value.basicInfos) ++
        Codec[ConcreteBasicGuildInformations].encode(value.guild)
    }
}
