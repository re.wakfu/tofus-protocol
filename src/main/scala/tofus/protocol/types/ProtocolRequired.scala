package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ProtocolRequired(
  requiredVersion: Int,
  currentVersion: Int
) extends Message(1)

object ProtocolRequired {
  implicit val codec: Codec[ProtocolRequired] =
    new Codec[ProtocolRequired] {
      def decode: Get[ProtocolRequired] =
        for {
          requiredVersion <- int.decode
          currentVersion <- int.decode
        } yield ProtocolRequired(requiredVersion, currentVersion)

      def encode(value: ProtocolRequired): ByteVector =
        int.encode(value.requiredVersion) ++
        int.encode(value.currentVersion)
    }
}
