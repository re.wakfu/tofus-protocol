package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceCreationResultMessage(
  result: Byte
) extends Message(6391)

object AllianceCreationResultMessage {
  implicit val codec: Codec[AllianceCreationResultMessage] =
    new Codec[AllianceCreationResultMessage] {
      def decode: Get[AllianceCreationResultMessage] =
        for {
          result <- byte.decode
        } yield AllianceCreationResultMessage(result)

      def encode(value: AllianceCreationResultMessage): ByteVector =
        byte.encode(value.result)
    }
}
