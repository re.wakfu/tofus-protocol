package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationDetailsRequestMessage(
  partyId: Int
) extends Message(6264)

object PartyInvitationDetailsRequestMessage {
  implicit val codec: Codec[PartyInvitationDetailsRequestMessage] =
    new Codec[PartyInvitationDetailsRequestMessage] {
      def decode: Get[PartyInvitationDetailsRequestMessage] =
        for {
          partyId <- varInt.decode
        } yield PartyInvitationDetailsRequestMessage(partyId)

      def encode(value: PartyInvitationDetailsRequestMessage): ByteVector =
        varInt.encode(value.partyId)
    }
}
