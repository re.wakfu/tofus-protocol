package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyMemberInBreachFightMessage(
  partyId: Int,
  reason: Byte,
  memberId: Long,
  memberAccountId: Int,
  memberName: String,
  fightId: Short,
  timeBeforeFightStart: Short,
  floor: Int,
  room: Byte
) extends Message(6824)

object PartyMemberInBreachFightMessage {
  implicit val codec: Codec[PartyMemberInBreachFightMessage] =
    new Codec[PartyMemberInBreachFightMessage] {
      def decode: Get[PartyMemberInBreachFightMessage] =
        for {
          partyId <- varInt.decode
          reason <- byte.decode
          memberId <- varLong.decode
          memberAccountId <- int.decode
          memberName <- utf8(ushort).decode
          fightId <- varShort.decode
          timeBeforeFightStart <- varShort.decode
          floor <- varInt.decode
          room <- byte.decode
        } yield PartyMemberInBreachFightMessage(partyId, reason, memberId, memberAccountId, memberName, fightId, timeBeforeFightStart, floor, room)

      def encode(value: PartyMemberInBreachFightMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.reason) ++
        varLong.encode(value.memberId) ++
        int.encode(value.memberAccountId) ++
        utf8(ushort).encode(value.memberName) ++
        varShort.encode(value.fightId) ++
        varShort.encode(value.timeBeforeFightStart) ++
        varInt.encode(value.floor) ++
        byte.encode(value.room)
    }
}
