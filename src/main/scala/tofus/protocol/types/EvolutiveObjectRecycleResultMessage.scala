package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EvolutiveObjectRecycleResultMessage(
  recycledItems: List[RecycledItem]
) extends Message(6779)

object EvolutiveObjectRecycleResultMessage {
  implicit val codec: Codec[EvolutiveObjectRecycleResultMessage] =
    new Codec[EvolutiveObjectRecycleResultMessage] {
      def decode: Get[EvolutiveObjectRecycleResultMessage] =
        for {
          recycledItems <- list(ushort, Codec[RecycledItem]).decode
        } yield EvolutiveObjectRecycleResultMessage(recycledItems)

      def encode(value: EvolutiveObjectRecycleResultMessage): ByteVector =
        list(ushort, Codec[RecycledItem]).encode(value.recycledItems)
    }
}
