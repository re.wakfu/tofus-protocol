package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StorageObjectsRemoveMessage(
  objectUIDList: List[Int]
) extends Message(6035)

object StorageObjectsRemoveMessage {
  implicit val codec: Codec[StorageObjectsRemoveMessage] =
    new Codec[StorageObjectsRemoveMessage] {
      def decode: Get[StorageObjectsRemoveMessage] =
        for {
          objectUIDList <- list(ushort, varInt).decode
        } yield StorageObjectsRemoveMessage(objectUIDList)

      def encode(value: StorageObjectsRemoveMessage): ByteVector =
        list(ushort, varInt).encode(value.objectUIDList)
    }
}
