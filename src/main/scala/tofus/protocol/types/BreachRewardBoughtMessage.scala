package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachRewardBoughtMessage(
  id: Int,
  bought: Boolean
) extends Message(6797)

object BreachRewardBoughtMessage {
  implicit val codec: Codec[BreachRewardBoughtMessage] =
    new Codec[BreachRewardBoughtMessage] {
      def decode: Get[BreachRewardBoughtMessage] =
        for {
          id <- varInt.decode
          bought <- bool.decode
        } yield BreachRewardBoughtMessage(id, bought)

      def encode(value: BreachRewardBoughtMessage): ByteVector =
        varInt.encode(value.id) ++
        bool.encode(value.bought)
    }
}
