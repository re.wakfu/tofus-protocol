package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmotePlayMessage(
  emoteId: Short,
  emoteStartTime: Double,
  actorId: Double,
  accountId: Int
) extends Message(5683)

object EmotePlayMessage {
  implicit val codec: Codec[EmotePlayMessage] =
    new Codec[EmotePlayMessage] {
      def decode: Get[EmotePlayMessage] =
        for {
          emoteId <- ubyte.decode
          emoteStartTime <- double.decode
          actorId <- double.decode
          accountId <- int.decode
        } yield EmotePlayMessage(emoteId, emoteStartTime, actorId, accountId)

      def encode(value: EmotePlayMessage): ByteVector =
        ubyte.encode(value.emoteId) ++
        double.encode(value.emoteStartTime) ++
        double.encode(value.actorId) ++
        int.encode(value.accountId)
    }
}
