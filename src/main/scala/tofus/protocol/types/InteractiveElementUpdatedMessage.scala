package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveElementUpdatedMessage(
  interactiveElement: ConcreteInteractiveElement
) extends Message(5708)

object InteractiveElementUpdatedMessage {
  implicit val codec: Codec[InteractiveElementUpdatedMessage] =
    new Codec[InteractiveElementUpdatedMessage] {
      def decode: Get[InteractiveElementUpdatedMessage] =
        for {
          interactiveElement <- Codec[ConcreteInteractiveElement].decode
        } yield InteractiveElementUpdatedMessage(interactiveElement)

      def encode(value: InteractiveElementUpdatedMessage): ByteVector =
        Codec[ConcreteInteractiveElement].encode(value.interactiveElement)
    }
}
