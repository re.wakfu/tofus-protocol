package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayMonsterNotAngryAtPlayerMessage(
  playerId: Long,
  monsterGroupId: Double
) extends Message(6742)

object GameRolePlayMonsterNotAngryAtPlayerMessage {
  implicit val codec: Codec[GameRolePlayMonsterNotAngryAtPlayerMessage] =
    new Codec[GameRolePlayMonsterNotAngryAtPlayerMessage] {
      def decode: Get[GameRolePlayMonsterNotAngryAtPlayerMessage] =
        for {
          playerId <- varLong.decode
          monsterGroupId <- double.decode
        } yield GameRolePlayMonsterNotAngryAtPlayerMessage(playerId, monsterGroupId)

      def encode(value: GameRolePlayMonsterNotAngryAtPlayerMessage): ByteVector =
        varLong.encode(value.playerId) ++
        double.encode(value.monsterGroupId)
    }
}
