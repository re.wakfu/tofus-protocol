package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendStatusShareStateMessage(
  share: Boolean
) extends Message(6823)

object FriendStatusShareStateMessage {
  implicit val codec: Codec[FriendStatusShareStateMessage] =
    new Codec[FriendStatusShareStateMessage] {
      def decode: Get[FriendStatusShareStateMessage] =
        for {
          share <- bool.decode
        } yield FriendStatusShareStateMessage(share)

      def encode(value: FriendStatusShareStateMessage): ByteVector =
        bool.encode(value.share)
    }
}
