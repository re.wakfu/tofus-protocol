package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InviteInHavenBagClosedMessage(
  hostInformations: ConcreteCharacterMinimalInformations
) extends Message(6645)

object InviteInHavenBagClosedMessage {
  implicit val codec: Codec[InviteInHavenBagClosedMessage] =
    new Codec[InviteInHavenBagClosedMessage] {
      def decode: Get[InviteInHavenBagClosedMessage] =
        for {
          hostInformations <- Codec[ConcreteCharacterMinimalInformations].decode
        } yield InviteInHavenBagClosedMessage(hostInformations)

      def encode(value: InviteInHavenBagClosedMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.hostInformations)
    }
}
