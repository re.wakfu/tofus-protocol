package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachStateMessage(
  owner: ConcreteCharacterMinimalInformations,
  bonuses: List[ObjectEffectInteger],
  bugdet: Int,
  saved: Boolean
) extends Message(6799)

object BreachStateMessage {
  implicit val codec: Codec[BreachStateMessage] =
    new Codec[BreachStateMessage] {
      def decode: Get[BreachStateMessage] =
        for {
          owner <- Codec[ConcreteCharacterMinimalInformations].decode
          bonuses <- list(ushort, Codec[ObjectEffectInteger]).decode
          bugdet <- varInt.decode
          saved <- bool.decode
        } yield BreachStateMessage(owner, bonuses, bugdet, saved)

      def encode(value: BreachStateMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.owner) ++
        list(ushort, Codec[ObjectEffectInteger]).encode(value.bonuses) ++
        varInt.encode(value.bugdet) ++
        bool.encode(value.saved)
    }
}
