package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectsDeletedMessage(
  objectUID: List[Int]
) extends Message(6034)

object ObjectsDeletedMessage {
  implicit val codec: Codec[ObjectsDeletedMessage] =
    new Codec[ObjectsDeletedMessage] {
      def decode: Get[ObjectsDeletedMessage] =
        for {
          objectUID <- list(ushort, varInt).decode
        } yield ObjectsDeletedMessage(objectUID)

      def encode(value: ObjectsDeletedMessage): ByteVector =
        list(ushort, varInt).encode(value.objectUID)
    }
}
