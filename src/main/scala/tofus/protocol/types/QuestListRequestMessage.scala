package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestListRequestMessage(

) extends Message(5623)

object QuestListRequestMessage {
  implicit val codec: Codec[QuestListRequestMessage] =
    Codec.const(QuestListRequestMessage())
}
