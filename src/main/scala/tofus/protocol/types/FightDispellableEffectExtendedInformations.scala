package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightDispellableEffectExtendedInformations(
  actionId: Short,
  sourceId: Double,
  effect: AbstractFightDispellableEffect
) extends ProtocolType {
  override val protocolId = 208
}

object FightDispellableEffectExtendedInformations {
  implicit val codec: Codec[FightDispellableEffectExtendedInformations] =
    new Codec[FightDispellableEffectExtendedInformations] {
      def decode: Get[FightDispellableEffectExtendedInformations] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          effect <- Codec[AbstractFightDispellableEffect].decode
        } yield FightDispellableEffectExtendedInformations(actionId, sourceId, effect)

      def encode(value: FightDispellableEffectExtendedInformations): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        Codec[AbstractFightDispellableEffect].encode(value.effect)
    }
}
