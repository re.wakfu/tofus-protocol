package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockRemoveItemRequestMessage(
  cellId: Short
) extends Message(5958)

object PaddockRemoveItemRequestMessage {
  implicit val codec: Codec[PaddockRemoveItemRequestMessage] =
    new Codec[PaddockRemoveItemRequestMessage] {
      def decode: Get[PaddockRemoveItemRequestMessage] =
        for {
          cellId <- varShort.decode
        } yield PaddockRemoveItemRequestMessage(cellId)

      def encode(value: PaddockRemoveItemRequestMessage): ByteVector =
        varShort.encode(value.cellId)
    }
}
