package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntityTalkMessage(
  entityId: Double,
  textId: Short,
  parameters: List[String]
) extends Message(6110)

object EntityTalkMessage {
  implicit val codec: Codec[EntityTalkMessage] =
    new Codec[EntityTalkMessage] {
      def decode: Get[EntityTalkMessage] =
        for {
          entityId <- double.decode
          textId <- varShort.decode
          parameters <- list(ushort, utf8(ushort)).decode
        } yield EntityTalkMessage(entityId, textId, parameters)

      def encode(value: EntityTalkMessage): ByteVector =
        double.encode(value.entityId) ++
        varShort.encode(value.textId) ++
        list(ushort, utf8(ushort)).encode(value.parameters)
    }
}
