package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ArenaRankInfos(

) extends ProtocolType {
  override val protocolId = 499
}

object ArenaRankInfos {
  implicit val codec: Codec[ArenaRankInfos] =
    Codec.const(ArenaRankInfos())
}
