package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceFactsRequestMessage(
  allianceId: Int
) extends Message(6409)

object AllianceFactsRequestMessage {
  implicit val codec: Codec[AllianceFactsRequestMessage] =
    new Codec[AllianceFactsRequestMessage] {
      def decode: Get[AllianceFactsRequestMessage] =
        for {
          allianceId <- varInt.decode
        } yield AllianceFactsRequestMessage(allianceId)

      def encode(value: AllianceFactsRequestMessage): ByteVector =
        varInt.encode(value.allianceId)
    }
}
