package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NewMailMessage(
  unread: Short,
  total: Short,
  sendersAccountId: List[Int]
) extends Message(6292)

object NewMailMessage {
  implicit val codec: Codec[NewMailMessage] =
    new Codec[NewMailMessage] {
      def decode: Get[NewMailMessage] =
        for {
          unread <- varShort.decode
          total <- varShort.decode
          sendersAccountId <- list(ushort, int).decode
        } yield NewMailMessage(unread, total, sendersAccountId)

      def encode(value: NewMailMessage): ByteVector =
        varShort.encode(value.unread) ++
        varShort.encode(value.total) ++
        list(ushort, int).encode(value.sendersAccountId)
    }
}
