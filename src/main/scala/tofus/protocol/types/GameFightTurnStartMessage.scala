package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnStartMessage(
  id: Double,
  waitTime: Int
) extends Message(714)

object GameFightTurnStartMessage {
  implicit val codec: Codec[GameFightTurnStartMessage] =
    new Codec[GameFightTurnStartMessage] {
      def decode: Get[GameFightTurnStartMessage] =
        for {
          id <- double.decode
          waitTime <- varInt.decode
        } yield GameFightTurnStartMessage(id, waitTime)

      def encode(value: GameFightTurnStartMessage): ByteVector =
        double.encode(value.id) ++
        varInt.encode(value.waitTime)
    }
}
