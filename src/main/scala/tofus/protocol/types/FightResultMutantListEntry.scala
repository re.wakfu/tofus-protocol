package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightResultMutantListEntry(
  outcome: Short,
  wave: Byte,
  rewards: FightLoot,
  id: Double,
  alive: Boolean,
  level: Short
) extends FightResultFighterListEntry {
  override val protocolId = 216
}

object FightResultMutantListEntry {
  implicit val codec: Codec[FightResultMutantListEntry] =
    new Codec[FightResultMutantListEntry] {
      def decode: Get[FightResultMutantListEntry] =
        for {
          outcome <- varShort.decode
          wave <- byte.decode
          rewards <- Codec[FightLoot].decode
          id <- double.decode
          alive <- bool.decode
          level <- varShort.decode
        } yield FightResultMutantListEntry(outcome, wave, rewards, id, alive, level)

      def encode(value: FightResultMutantListEntry): ByteVector =
        varShort.encode(value.outcome) ++
        byte.encode(value.wave) ++
        Codec[FightLoot].encode(value.rewards) ++
        double.encode(value.id) ++
        bool.encode(value.alive) ++
        varShort.encode(value.level)
    }
}
