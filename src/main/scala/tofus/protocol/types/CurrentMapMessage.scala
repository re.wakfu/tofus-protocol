package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CurrentMapMessage(
  mapId: Double,
  mapKey: String
) extends Message(220)

object CurrentMapMessage {
  implicit val codec: Codec[CurrentMapMessage] =
    new Codec[CurrentMapMessage] {
      def decode: Get[CurrentMapMessage] =
        for {
          mapId <- double.decode
          mapKey <- utf8(ushort).decode
        } yield CurrentMapMessage(mapId, mapKey)

      def encode(value: CurrentMapMessage): ByteVector =
        double.encode(value.mapId) ++
        utf8(ushort).encode(value.mapKey)
    }
}
