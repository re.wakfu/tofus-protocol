package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait FightResultAdditionalData extends ProtocolType

final case class ConcreteFightResultAdditionalData(

) extends FightResultAdditionalData {
  override val protocolId = 191
}

object ConcreteFightResultAdditionalData {
  implicit val codec: Codec[ConcreteFightResultAdditionalData] =  
    Codec.const(ConcreteFightResultAdditionalData())
}

object FightResultAdditionalData {
  implicit val codec: Codec[FightResultAdditionalData] =
    new Codec[FightResultAdditionalData] {
      def decode: Get[FightResultAdditionalData] =
        ushort.decode.flatMap {
          case 191 => Codec[ConcreteFightResultAdditionalData].decode
          case 192 => Codec[FightResultExperienceData].decode
          case 190 => Codec[FightResultPvpData].decode
        }

      def encode(value: FightResultAdditionalData): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteFightResultAdditionalData => Codec[ConcreteFightResultAdditionalData].encode(i)
          case i: FightResultExperienceData => Codec[FightResultExperienceData].encode(i)
          case i: FightResultPvpData => Codec[FightResultPvpData].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
