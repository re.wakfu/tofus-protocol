package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmoteRemoveMessage(
  emoteId: Short
) extends Message(5687)

object EmoteRemoveMessage {
  implicit val codec: Codec[EmoteRemoveMessage] =
    new Codec[EmoteRemoveMessage] {
      def decode: Get[EmoteRemoveMessage] =
        for {
          emoteId <- ubyte.decode
        } yield EmoteRemoveMessage(emoteId)

      def encode(value: EmoteRemoveMessage): ByteVector =
        ubyte.encode(value.emoteId)
    }
}
