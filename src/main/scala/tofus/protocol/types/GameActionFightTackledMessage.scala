package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightTackledMessage(
  actionId: Short,
  sourceId: Double,
  tacklersIds: List[Double]
) extends Message(1004)

object GameActionFightTackledMessage {
  implicit val codec: Codec[GameActionFightTackledMessage] =
    new Codec[GameActionFightTackledMessage] {
      def decode: Get[GameActionFightTackledMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          tacklersIds <- list(ushort, double).decode
        } yield GameActionFightTackledMessage(actionId, sourceId, tacklersIds)

      def encode(value: GameActionFightTackledMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        list(ushort, double).encode(value.tacklersIds)
    }
}
