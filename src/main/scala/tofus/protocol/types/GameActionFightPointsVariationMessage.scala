package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightPointsVariationMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  delta: Short
) extends Message(1030)

object GameActionFightPointsVariationMessage {
  implicit val codec: Codec[GameActionFightPointsVariationMessage] =
    new Codec[GameActionFightPointsVariationMessage] {
      def decode: Get[GameActionFightPointsVariationMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          delta <- short.decode
        } yield GameActionFightPointsVariationMessage(actionId, sourceId, targetId, delta)

      def encode(value: GameActionFightPointsVariationMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.delta)
    }
}
