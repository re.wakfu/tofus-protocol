package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftPaymentModificationRequestMessage(
  quantity: Long
) extends Message(6579)

object ExchangeCraftPaymentModificationRequestMessage {
  implicit val codec: Codec[ExchangeCraftPaymentModificationRequestMessage] =
    new Codec[ExchangeCraftPaymentModificationRequestMessage] {
      def decode: Get[ExchangeCraftPaymentModificationRequestMessage] =
        for {
          quantity <- varLong.decode
        } yield ExchangeCraftPaymentModificationRequestMessage(quantity)

      def encode(value: ExchangeCraftPaymentModificationRequestMessage): ByteVector =
        varLong.encode(value.quantity)
    }
}
