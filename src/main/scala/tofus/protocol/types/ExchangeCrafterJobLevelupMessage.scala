package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCrafterJobLevelupMessage(
  crafterJobLevel: Short
) extends Message(6598)

object ExchangeCrafterJobLevelupMessage {
  implicit val codec: Codec[ExchangeCrafterJobLevelupMessage] =
    new Codec[ExchangeCrafterJobLevelupMessage] {
      def decode: Get[ExchangeCrafterJobLevelupMessage] =
        for {
          crafterJobLevel <- ubyte.decode
        } yield ExchangeCrafterJobLevelupMessage(crafterJobLevel)

      def encode(value: ExchangeCrafterJobLevelupMessage): ByteVector =
        ubyte.encode(value.crafterJobLevel)
    }
}
