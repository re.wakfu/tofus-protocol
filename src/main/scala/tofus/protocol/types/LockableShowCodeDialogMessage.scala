package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableShowCodeDialogMessage(
  changeOrUse: Boolean,
  codeSize: Byte
) extends Message(5740)

object LockableShowCodeDialogMessage {
  implicit val codec: Codec[LockableShowCodeDialogMessage] =
    new Codec[LockableShowCodeDialogMessage] {
      def decode: Get[LockableShowCodeDialogMessage] =
        for {
          changeOrUse <- bool.decode
          codeSize <- byte.decode
        } yield LockableShowCodeDialogMessage(changeOrUse, codeSize)

      def encode(value: LockableShowCodeDialogMessage): ByteVector =
        bool.encode(value.changeOrUse) ++
        byte.encode(value.codeSize)
    }
}
