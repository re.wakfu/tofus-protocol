package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LeaveDialogRequestMessage(

) extends Message(5501)

object LeaveDialogRequestMessage {
  implicit val codec: Codec[LeaveDialogRequestMessage] =
    Codec.const(LeaveDialogRequestMessage())
}
