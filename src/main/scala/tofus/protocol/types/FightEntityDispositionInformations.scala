package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightEntityDispositionInformations(
  cellId: Short,
  direction: Byte,
  carryingCharacterId: Double
) extends EntityDispositionInformations {
  override val protocolId = 217
}

object FightEntityDispositionInformations {
  implicit val codec: Codec[FightEntityDispositionInformations] =
    new Codec[FightEntityDispositionInformations] {
      def decode: Get[FightEntityDispositionInformations] =
        for {
          cellId <- short.decode
          direction <- byte.decode
          carryingCharacterId <- double.decode
        } yield FightEntityDispositionInformations(cellId, direction, carryingCharacterId)

      def encode(value: FightEntityDispositionInformations): ByteVector =
        short.encode(value.cellId) ++
        byte.encode(value.direction) ++
        double.encode(value.carryingCharacterId)
    }
}
