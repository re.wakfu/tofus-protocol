package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InvalidPresetsMessage(
  presetIds: List[Short]
) extends Message(6839)

object InvalidPresetsMessage {
  implicit val codec: Codec[InvalidPresetsMessage] =
    new Codec[InvalidPresetsMessage] {
      def decode: Get[InvalidPresetsMessage] =
        for {
          presetIds <- list(ushort, short).decode
        } yield InvalidPresetsMessage(presetIds)

      def encode(value: InvalidPresetsMessage): ByteVector =
        list(ushort, short).encode(value.presetIds)
    }
}
