package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagFurnitureInformation(
  cellId: Short,
  funitureId: Int,
  orientation: Byte
) extends ProtocolType {
  override val protocolId = 498
}

object HavenBagFurnitureInformation {
  implicit val codec: Codec[HavenBagFurnitureInformation] =
    new Codec[HavenBagFurnitureInformation] {
      def decode: Get[HavenBagFurnitureInformation] =
        for {
          cellId <- varShort.decode
          funitureId <- int.decode
          orientation <- byte.decode
        } yield HavenBagFurnitureInformation(cellId, funitureId, orientation)

      def encode(value: HavenBagFurnitureInformation): ByteVector =
        varShort.encode(value.cellId) ++
        int.encode(value.funitureId) ++
        byte.encode(value.orientation)
    }
}
