package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorErrorMessage(
  reason: Byte
) extends Message(5634)

object TaxCollectorErrorMessage {
  implicit val codec: Codec[TaxCollectorErrorMessage] =
    new Codec[TaxCollectorErrorMessage] {
      def decode: Get[TaxCollectorErrorMessage] =
        for {
          reason <- byte.decode
        } yield TaxCollectorErrorMessage(reason)

      def encode(value: TaxCollectorErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
