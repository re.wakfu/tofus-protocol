package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMoneyMovementInformationMessage(
  limit: Long
) extends Message(6834)

object ExchangeMoneyMovementInformationMessage {
  implicit val codec: Codec[ExchangeMoneyMovementInformationMessage] =
    new Codec[ExchangeMoneyMovementInformationMessage] {
      def decode: Get[ExchangeMoneyMovementInformationMessage] =
        for {
          limit <- varLong.decode
        } yield ExchangeMoneyMovementInformationMessage(limit)

      def encode(value: ExchangeMoneyMovementInformationMessage): ByteVector =
        varLong.encode(value.limit)
    }
}
