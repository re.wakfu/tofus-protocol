package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ObjectEffect extends ProtocolType

final case class ConcreteObjectEffect(
  actionId: Short
) extends ObjectEffect {
  override val protocolId = 76
}

object ConcreteObjectEffect {
  implicit val codec: Codec[ConcreteObjectEffect] =  
    new Codec[ConcreteObjectEffect] {
      def decode: Get[ConcreteObjectEffect] =
        for {
          actionId <- varShort.decode
        } yield ConcreteObjectEffect(actionId)

      def encode(value: ConcreteObjectEffect): ByteVector =
        varShort.encode(value.actionId)
    }
}

object ObjectEffect {
  implicit val codec: Codec[ObjectEffect] =
    new Codec[ObjectEffect] {
      def decode: Get[ObjectEffect] =
        ushort.decode.flatMap {
          case 76 => Codec[ConcreteObjectEffect].decode
          case 70 => Codec[ObjectEffectInteger].decode
          case 81 => Codec[ObjectEffectLadder].decode
          case 71 => Codec[ConcreteObjectEffectCreature].decode
          case 179 => Codec[ObjectEffectMount].decode
          case 72 => Codec[ObjectEffectDate].decode
          case 73 => Codec[ObjectEffectDice].decode
          case 82 => Codec[ObjectEffectMinMax].decode
          case 74 => Codec[ObjectEffectString].decode
          case 75 => Codec[ObjectEffectDuration].decode
        }

      def encode(value: ObjectEffect): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteObjectEffect => Codec[ConcreteObjectEffect].encode(i)
          case i: ObjectEffectInteger => Codec[ObjectEffectInteger].encode(i)
          case i: ObjectEffectLadder => Codec[ObjectEffectLadder].encode(i)
          case i: ConcreteObjectEffectCreature => Codec[ConcreteObjectEffectCreature].encode(i)
          case i: ObjectEffectMount => Codec[ObjectEffectMount].encode(i)
          case i: ObjectEffectDate => Codec[ObjectEffectDate].encode(i)
          case i: ObjectEffectDice => Codec[ObjectEffectDice].encode(i)
          case i: ObjectEffectMinMax => Codec[ObjectEffectMinMax].encode(i)
          case i: ObjectEffectString => Codec[ObjectEffectString].encode(i)
          case i: ObjectEffectDuration => Codec[ObjectEffectDuration].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
