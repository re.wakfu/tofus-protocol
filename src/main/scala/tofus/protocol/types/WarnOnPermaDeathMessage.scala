package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class WarnOnPermaDeathMessage(
  enable: Boolean
) extends Message(6512)

object WarnOnPermaDeathMessage {
  implicit val codec: Codec[WarnOnPermaDeathMessage] =
    new Codec[WarnOnPermaDeathMessage] {
      def decode: Get[WarnOnPermaDeathMessage] =
        for {
          enable <- bool.decode
        } yield WarnOnPermaDeathMessage(enable)

      def encode(value: WarnOnPermaDeathMessage): ByteVector =
        bool.encode(value.enable)
    }
}
