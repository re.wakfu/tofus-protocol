package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class InteractiveElementWithAgeBonus(
  elementId: Int,
  elementTypeId: Int,
  enabledSkills: List[InteractiveElementSkill],
  disabledSkills: List[InteractiveElementSkill],
  onCurrentMap: Boolean,
  ageBonus: Short
) extends InteractiveElement {
  override val protocolId = 398
}

object InteractiveElementWithAgeBonus {
  implicit val codec: Codec[InteractiveElementWithAgeBonus] =
    new Codec[InteractiveElementWithAgeBonus] {
      def decode: Get[InteractiveElementWithAgeBonus] =
        for {
          elementId <- int.decode
          elementTypeId <- int.decode
          enabledSkills <- list(ushort, Codec[InteractiveElementSkill]).decode
          disabledSkills <- list(ushort, Codec[InteractiveElementSkill]).decode
          onCurrentMap <- bool.decode
          ageBonus <- short.decode
        } yield InteractiveElementWithAgeBonus(elementId, elementTypeId, enabledSkills, disabledSkills, onCurrentMap, ageBonus)

      def encode(value: InteractiveElementWithAgeBonus): ByteVector =
        int.encode(value.elementId) ++
        int.encode(value.elementTypeId) ++
        list(ushort, Codec[InteractiveElementSkill]).encode(value.enabledSkills) ++
        list(ushort, Codec[InteractiveElementSkill]).encode(value.disabledSkills) ++
        bool.encode(value.onCurrentMap) ++
        short.encode(value.ageBonus)
    }
}
