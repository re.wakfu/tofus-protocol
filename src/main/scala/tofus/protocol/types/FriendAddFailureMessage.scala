package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendAddFailureMessage(
  reason: Byte
) extends Message(5600)

object FriendAddFailureMessage {
  implicit val codec: Codec[FriendAddFailureMessage] =
    new Codec[FriendAddFailureMessage] {
      def decode: Get[FriendAddFailureMessage] =
        for {
          reason <- byte.decode
        } yield FriendAddFailureMessage(reason)

      def encode(value: FriendAddFailureMessage): ByteVector =
        byte.encode(value.reason)
    }
}
