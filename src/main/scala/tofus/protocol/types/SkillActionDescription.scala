package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait SkillActionDescription extends ProtocolType

final case class ConcreteSkillActionDescription(
  skillId: Short
) extends SkillActionDescription {
  override val protocolId = 102
}

object ConcreteSkillActionDescription {
  implicit val codec: Codec[ConcreteSkillActionDescription] =  
    new Codec[ConcreteSkillActionDescription] {
      def decode: Get[ConcreteSkillActionDescription] =
        for {
          skillId <- varShort.decode
        } yield ConcreteSkillActionDescription(skillId)

      def encode(value: ConcreteSkillActionDescription): ByteVector =
        varShort.encode(value.skillId)
    }
}

object SkillActionDescription {
  implicit val codec: Codec[SkillActionDescription] =
    new Codec[SkillActionDescription] {
      def decode: Get[SkillActionDescription] =
        ushort.decode.flatMap {
          case 102 => Codec[ConcreteSkillActionDescription].decode
          case 99 => Codec[SkillActionDescriptionCollect].decode
          case 103 => Codec[ConcreteSkillActionDescriptionTimed].decode
          case 100 => Codec[SkillActionDescriptionCraft].decode
        }

      def encode(value: SkillActionDescription): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteSkillActionDescription => Codec[ConcreteSkillActionDescription].encode(i)
          case i: SkillActionDescriptionCollect => Codec[SkillActionDescriptionCollect].encode(i)
          case i: ConcreteSkillActionDescriptionTimed => Codec[ConcreteSkillActionDescriptionTimed].encode(i)
          case i: SkillActionDescriptionCraft => Codec[SkillActionDescriptionCraft].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
