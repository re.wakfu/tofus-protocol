package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareRewardConsumeRequestMessage(
  dareId: Double,
  `type`: Byte
) extends Message(6676)

object DareRewardConsumeRequestMessage {
  implicit val codec: Codec[DareRewardConsumeRequestMessage] =
    new Codec[DareRewardConsumeRequestMessage] {
      def decode: Get[DareRewardConsumeRequestMessage] =
        for {
          dareId <- double.decode
          `type` <- byte.decode
        } yield DareRewardConsumeRequestMessage(dareId, `type`)

      def encode(value: DareRewardConsumeRequestMessage): ByteVector =
        double.encode(value.dareId) ++
        byte.encode(value.`type`)
    }
}
