package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EntityInformationMessage(
  entity: EntityInformation
) extends Message(6771)

object EntityInformationMessage {
  implicit val codec: Codec[EntityInformationMessage] =
    new Codec[EntityInformationMessage] {
      def decode: Get[EntityInformationMessage] =
        for {
          entity <- Codec[EntityInformation].decode
        } yield EntityInformationMessage(entity)

      def encode(value: EntityInformationMessage): ByteVector =
        Codec[EntityInformation].encode(value.entity)
    }
}
