package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntFlagRequestMessage(
  questType: Byte,
  index: Byte
) extends Message(6508)

object TreasureHuntFlagRequestMessage {
  implicit val codec: Codec[TreasureHuntFlagRequestMessage] =
    new Codec[TreasureHuntFlagRequestMessage] {
      def decode: Get[TreasureHuntFlagRequestMessage] =
        for {
          questType <- byte.decode
          index <- byte.decode
        } yield TreasureHuntFlagRequestMessage(questType, index)

      def encode(value: TreasureHuntFlagRequestMessage): ByteVector =
        byte.encode(value.questType) ++
        byte.encode(value.index)
    }
}
