package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait TaxCollectorStaticInformations extends ProtocolType

final case class ConcreteTaxCollectorStaticInformations(
  firstNameId: Short,
  lastNameId: Short,
  guildIdentity: ConcreteGuildInformations
) extends TaxCollectorStaticInformations {
  override val protocolId = 147
}

object ConcreteTaxCollectorStaticInformations {
  implicit val codec: Codec[ConcreteTaxCollectorStaticInformations] =  
    new Codec[ConcreteTaxCollectorStaticInformations] {
      def decode: Get[ConcreteTaxCollectorStaticInformations] =
        for {
          firstNameId <- varShort.decode
          lastNameId <- varShort.decode
          guildIdentity <- Codec[ConcreteGuildInformations].decode
        } yield ConcreteTaxCollectorStaticInformations(firstNameId, lastNameId, guildIdentity)

      def encode(value: ConcreteTaxCollectorStaticInformations): ByteVector =
        varShort.encode(value.firstNameId) ++
        varShort.encode(value.lastNameId) ++
        Codec[ConcreteGuildInformations].encode(value.guildIdentity)
    }
}

object TaxCollectorStaticInformations {
  implicit val codec: Codec[TaxCollectorStaticInformations] =
    new Codec[TaxCollectorStaticInformations] {
      def decode: Get[TaxCollectorStaticInformations] =
        ushort.decode.flatMap {
          case 147 => Codec[ConcreteTaxCollectorStaticInformations].decode
          case 440 => Codec[TaxCollectorStaticExtendedInformations].decode
        }

      def encode(value: TaxCollectorStaticInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteTaxCollectorStaticInformations => Codec[ConcreteTaxCollectorStaticInformations].encode(i)
          case i: TaxCollectorStaticExtendedInformations => Codec[TaxCollectorStaticExtendedInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
