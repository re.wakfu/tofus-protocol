package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightDodgePointLossMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  amount: Short
) extends Message(5828)

object GameActionFightDodgePointLossMessage {
  implicit val codec: Codec[GameActionFightDodgePointLossMessage] =
    new Codec[GameActionFightDodgePointLossMessage] {
      def decode: Get[GameActionFightDodgePointLossMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          amount <- varShort.decode
        } yield GameActionFightDodgePointLossMessage(actionId, sourceId, targetId, amount)

      def encode(value: GameActionFightDodgePointLossMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varShort.encode(value.amount)
    }
}
