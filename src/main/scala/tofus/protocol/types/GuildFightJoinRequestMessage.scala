package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightJoinRequestMessage(
  taxCollectorId: Double
) extends Message(5717)

object GuildFightJoinRequestMessage {
  implicit val codec: Codec[GuildFightJoinRequestMessage] =
    new Codec[GuildFightJoinRequestMessage] {
      def decode: Get[GuildFightJoinRequestMessage] =
        for {
          taxCollectorId <- double.decode
        } yield GuildFightJoinRequestMessage(taxCollectorId)

      def encode(value: GuildFightJoinRequestMessage): ByteVector =
        double.encode(value.taxCollectorId)
    }
}
