package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectoryEntryMessage(
  playerInfo: JobCrafterDirectoryEntryPlayerInfo,
  jobInfoList: List[JobCrafterDirectoryEntryJobInfo],
  playerLook: EntityLook
) extends Message(6044)

object JobCrafterDirectoryEntryMessage {
  implicit val codec: Codec[JobCrafterDirectoryEntryMessage] =
    new Codec[JobCrafterDirectoryEntryMessage] {
      def decode: Get[JobCrafterDirectoryEntryMessage] =
        for {
          playerInfo <- Codec[JobCrafterDirectoryEntryPlayerInfo].decode
          jobInfoList <- list(ushort, Codec[JobCrafterDirectoryEntryJobInfo]).decode
          playerLook <- Codec[EntityLook].decode
        } yield JobCrafterDirectoryEntryMessage(playerInfo, jobInfoList, playerLook)

      def encode(value: JobCrafterDirectoryEntryMessage): ByteVector =
        Codec[JobCrafterDirectoryEntryPlayerInfo].encode(value.playerInfo) ++
        list(ushort, Codec[JobCrafterDirectoryEntryJobInfo]).encode(value.jobInfoList) ++
        Codec[EntityLook].encode(value.playerLook)
    }
}
