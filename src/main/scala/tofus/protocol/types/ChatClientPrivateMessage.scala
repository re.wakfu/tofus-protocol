package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatClientPrivateMessage(
  content: String,
  receiver: String
) extends Message(851)

object ChatClientPrivateMessage {
  implicit val codec: Codec[ChatClientPrivateMessage] =
    new Codec[ChatClientPrivateMessage] {
      def decode: Get[ChatClientPrivateMessage] =
        for {
          content <- utf8(ushort).decode
          receiver <- utf8(ushort).decode
        } yield ChatClientPrivateMessage(content, receiver)

      def encode(value: ChatClientPrivateMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        utf8(ushort).encode(value.receiver)
    }
}
