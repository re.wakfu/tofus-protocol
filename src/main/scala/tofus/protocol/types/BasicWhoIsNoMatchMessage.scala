package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicWhoIsNoMatchMessage(
  search: String
) extends Message(179)

object BasicWhoIsNoMatchMessage {
  implicit val codec: Codec[BasicWhoIsNoMatchMessage] =
    new Codec[BasicWhoIsNoMatchMessage] {
      def decode: Get[BasicWhoIsNoMatchMessage] =
        for {
          search <- utf8(ushort).decode
        } yield BasicWhoIsNoMatchMessage(search)

      def encode(value: BasicWhoIsNoMatchMessage): ByteVector =
        utf8(ushort).encode(value.search)
    }
}
