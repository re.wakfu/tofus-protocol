package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountSterilizeFromPaddockMessage(
  name: String,
  worldX: Short,
  worldY: Short,
  sterilizator: String
) extends Message(6056)

object ExchangeMountSterilizeFromPaddockMessage {
  implicit val codec: Codec[ExchangeMountSterilizeFromPaddockMessage] =
    new Codec[ExchangeMountSterilizeFromPaddockMessage] {
      def decode: Get[ExchangeMountSterilizeFromPaddockMessage] =
        for {
          name <- utf8(ushort).decode
          worldX <- short.decode
          worldY <- short.decode
          sterilizator <- utf8(ushort).decode
        } yield ExchangeMountSterilizeFromPaddockMessage(name, worldX, worldY, sterilizator)

      def encode(value: ExchangeMountSterilizeFromPaddockMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        utf8(ushort).encode(value.sterilizator)
    }
}
