package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceGuildLeavingMessage(
  kicked: Boolean,
  guildId: Int
) extends Message(6399)

object AllianceGuildLeavingMessage {
  implicit val codec: Codec[AllianceGuildLeavingMessage] =
    new Codec[AllianceGuildLeavingMessage] {
      def decode: Get[AllianceGuildLeavingMessage] =
        for {
          kicked <- bool.decode
          guildId <- varInt.decode
        } yield AllianceGuildLeavingMessage(kicked, guildId)

      def encode(value: AllianceGuildLeavingMessage): ByteVector =
        bool.encode(value.kicked) ++
        varInt.encode(value.guildId)
    }
}
