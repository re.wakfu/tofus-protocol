package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionGuild(
  guildInformations: ConcreteGuildInformations
) extends HumanOption {
  override val protocolId = 409
}

object HumanOptionGuild {
  implicit val codec: Codec[HumanOptionGuild] =
    new Codec[HumanOptionGuild] {
      def decode: Get[HumanOptionGuild] =
        for {
          guildInformations <- Codec[ConcreteGuildInformations].decode
        } yield HumanOptionGuild(guildInformations)

      def encode(value: HumanOptionGuild): ByteVector =
        Codec[ConcreteGuildInformations].encode(value.guildInformations)
    }
}
