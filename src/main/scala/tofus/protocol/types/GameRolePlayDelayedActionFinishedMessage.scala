package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayDelayedActionFinishedMessage(
  delayedCharacterId: Double,
  delayTypeId: Byte
) extends Message(6150)

object GameRolePlayDelayedActionFinishedMessage {
  implicit val codec: Codec[GameRolePlayDelayedActionFinishedMessage] =
    new Codec[GameRolePlayDelayedActionFinishedMessage] {
      def decode: Get[GameRolePlayDelayedActionFinishedMessage] =
        for {
          delayedCharacterId <- double.decode
          delayTypeId <- byte.decode
        } yield GameRolePlayDelayedActionFinishedMessage(delayedCharacterId, delayTypeId)

      def encode(value: GameRolePlayDelayedActionFinishedMessage): ByteVector =
        double.encode(value.delayedCharacterId) ++
        byte.encode(value.delayTypeId)
    }
}
