package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterSelectedSuccessMessage(
  infos: ConcreteCharacterBaseInformations,
  isCollectingStats: Boolean
) extends Message(153)

object CharacterSelectedSuccessMessage {
  implicit val codec: Codec[CharacterSelectedSuccessMessage] =
    new Codec[CharacterSelectedSuccessMessage] {
      def decode: Get[CharacterSelectedSuccessMessage] =
        for {
          infos <- Codec[ConcreteCharacterBaseInformations].decode
          isCollectingStats <- bool.decode
        } yield CharacterSelectedSuccessMessage(infos, isCollectingStats)

      def encode(value: CharacterSelectedSuccessMessage): ByteVector =
        Codec[ConcreteCharacterBaseInformations].encode(value.infos) ++
        bool.encode(value.isCollectingStats)
    }
}
