package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeAcceptMessage(

) extends Message(5508)

object ExchangeAcceptMessage {
  implicit val codec: Codec[ExchangeAcceptMessage] =
    Codec.const(ExchangeAcceptMessage())
}
