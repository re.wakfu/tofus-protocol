package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountUnSetMessage(

) extends Message(5982)

object MountUnSetMessage {
  implicit val codec: Codec[MountUnSetMessage] =
    Codec.const(MountUnSetMessage())
}
