package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobMultiCraftAvailableSkillsMessage(
  enabled: Boolean,
  playerId: Long,
  skills: List[Short]
) extends Message(5747)

object JobMultiCraftAvailableSkillsMessage {
  implicit val codec: Codec[JobMultiCraftAvailableSkillsMessage] =
    new Codec[JobMultiCraftAvailableSkillsMessage] {
      def decode: Get[JobMultiCraftAvailableSkillsMessage] =
        for {
          enabled <- bool.decode
          playerId <- varLong.decode
          skills <- list(ushort, varShort).decode
        } yield JobMultiCraftAvailableSkillsMessage(enabled, playerId, skills)

      def encode(value: JobMultiCraftAvailableSkillsMessage): ByteVector =
        bool.encode(value.enabled) ++
        varLong.encode(value.playerId) ++
        list(ushort, varShort).encode(value.skills)
    }
}
