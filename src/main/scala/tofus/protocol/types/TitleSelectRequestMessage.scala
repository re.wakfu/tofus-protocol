package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitleSelectRequestMessage(
  titleId: Short
) extends Message(6365)

object TitleSelectRequestMessage {
  implicit val codec: Codec[TitleSelectRequestMessage] =
    new Codec[TitleSelectRequestMessage] {
      def decode: Get[TitleSelectRequestMessage] =
        for {
          titleId <- varShort.decode
        } yield TitleSelectRequestMessage(titleId)

      def encode(value: TitleSelectRequestMessage): ByteVector =
        varShort.encode(value.titleId)
    }
}
