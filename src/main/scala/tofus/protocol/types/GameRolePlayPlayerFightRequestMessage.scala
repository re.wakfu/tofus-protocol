package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPlayerFightRequestMessage(
  targetId: Long,
  targetCellId: Short,
  friendly: Boolean
) extends Message(5731)

object GameRolePlayPlayerFightRequestMessage {
  implicit val codec: Codec[GameRolePlayPlayerFightRequestMessage] =
    new Codec[GameRolePlayPlayerFightRequestMessage] {
      def decode: Get[GameRolePlayPlayerFightRequestMessage] =
        for {
          targetId <- varLong.decode
          targetCellId <- short.decode
          friendly <- bool.decode
        } yield GameRolePlayPlayerFightRequestMessage(targetId, targetCellId, friendly)

      def encode(value: GameRolePlayPlayerFightRequestMessage): ByteVector =
        varLong.encode(value.targetId) ++
        short.encode(value.targetCellId) ++
        bool.encode(value.friendly)
    }
}
