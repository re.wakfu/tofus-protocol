package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NpcDialogCreationMessage(
  mapId: Double,
  npcId: Int
) extends Message(5618)

object NpcDialogCreationMessage {
  implicit val codec: Codec[NpcDialogCreationMessage] =
    new Codec[NpcDialogCreationMessage] {
      def decode: Get[NpcDialogCreationMessage] =
        for {
          mapId <- double.decode
          npcId <- int.decode
        } yield NpcDialogCreationMessage(mapId, npcId)

      def encode(value: NpcDialogCreationMessage): ByteVector =
        double.encode(value.mapId) ++
        int.encode(value.npcId)
    }
}
