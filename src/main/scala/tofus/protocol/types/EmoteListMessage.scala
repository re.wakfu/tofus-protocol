package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EmoteListMessage(
  emoteIds: List[Short]
) extends Message(5689)

object EmoteListMessage {
  implicit val codec: Codec[EmoteListMessage] =
    new Codec[EmoteListMessage] {
      def decode: Get[EmoteListMessage] =
        for {
          emoteIds <- list(ushort, ubyte).decode
        } yield EmoteListMessage(emoteIds)

      def encode(value: EmoteListMessage): ByteVector =
        list(ushort, ubyte).encode(value.emoteIds)
    }
}
