package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatServerMessage(
  channel: Byte,
  content: String,
  timestamp: Int,
  fingerprint: String,
  senderId: Double,
  senderName: String,
  prefix: String,
  senderAccountId: Int
) extends Message(881)

object ChatServerMessage {
  implicit val codec: Codec[ChatServerMessage] =
    new Codec[ChatServerMessage] {
      def decode: Get[ChatServerMessage] =
        for {
          channel <- byte.decode
          content <- utf8(ushort).decode
          timestamp <- int.decode
          fingerprint <- utf8(ushort).decode
          senderId <- double.decode
          senderName <- utf8(ushort).decode
          prefix <- utf8(ushort).decode
          senderAccountId <- int.decode
        } yield ChatServerMessage(channel, content, timestamp, fingerprint, senderId, senderName, prefix, senderAccountId)

      def encode(value: ChatServerMessage): ByteVector =
        byte.encode(value.channel) ++
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        utf8(ushort).encode(value.fingerprint) ++
        double.encode(value.senderId) ++
        utf8(ushort).encode(value.senderName) ++
        utf8(ushort).encode(value.prefix) ++
        int.encode(value.senderAccountId)
    }
}
