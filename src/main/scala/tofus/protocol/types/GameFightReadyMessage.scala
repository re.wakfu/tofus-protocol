package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightReadyMessage(
  isReady: Boolean
) extends Message(708)

object GameFightReadyMessage {
  implicit val codec: Codec[GameFightReadyMessage] =
    new Codec[GameFightReadyMessage] {
      def decode: Get[GameFightReadyMessage] =
        for {
          isReady <- bool.decode
        } yield GameFightReadyMessage(isReady)

      def encode(value: GameFightReadyMessage): ByteVector =
        bool.encode(value.isReady)
    }
}
