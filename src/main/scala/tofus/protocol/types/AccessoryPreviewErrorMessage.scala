package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccessoryPreviewErrorMessage(
  error: Byte
) extends Message(6521)

object AccessoryPreviewErrorMessage {
  implicit val codec: Codec[AccessoryPreviewErrorMessage] =
    new Codec[AccessoryPreviewErrorMessage] {
      def decode: Get[AccessoryPreviewErrorMessage] =
        for {
          error <- byte.decode
        } yield AccessoryPreviewErrorMessage(error)

      def encode(value: AccessoryPreviewErrorMessage): ByteVector =
        byte.encode(value.error)
    }
}
