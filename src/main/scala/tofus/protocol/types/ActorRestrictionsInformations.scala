package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ActorRestrictionsInformations(
  flags0: Byte,
  flags1: Byte,
  flags2: Byte
) extends ProtocolType {
  override val protocolId = 204
}

object ActorRestrictionsInformations {
  implicit val codec: Codec[ActorRestrictionsInformations] =
    new Codec[ActorRestrictionsInformations] {
      def decode: Get[ActorRestrictionsInformations] =
        for {
          flags0 <- byte.decode
          flags1 <- byte.decode
          flags2 <- byte.decode
        } yield ActorRestrictionsInformations(flags0, flags1, flags2)

      def encode(value: ActorRestrictionsInformations): ByteVector =
        byte.encode(value.flags0) ++
        byte.encode(value.flags1) ++
        byte.encode(value.flags2)
    }
}
