package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCreationResultMessage(
  result: Byte
) extends Message(161)

object CharacterCreationResultMessage {
  implicit val codec: Codec[CharacterCreationResultMessage] =
    new Codec[CharacterCreationResultMessage] {
      def decode: Get[CharacterCreationResultMessage] =
        for {
          result <- byte.decode
        } yield CharacterCreationResultMessage(result)

      def encode(value: CharacterCreationResultMessage): ByteVector =
        byte.encode(value.result)
    }
}
