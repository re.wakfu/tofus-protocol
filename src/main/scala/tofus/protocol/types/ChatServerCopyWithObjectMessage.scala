package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatServerCopyWithObjectMessage(
  channel: Byte,
  content: String,
  timestamp: Int,
  fingerprint: String,
  receiverId: Long,
  receiverName: String,
  objects: List[ObjectItem]
) extends Message(884)

object ChatServerCopyWithObjectMessage {
  implicit val codec: Codec[ChatServerCopyWithObjectMessage] =
    new Codec[ChatServerCopyWithObjectMessage] {
      def decode: Get[ChatServerCopyWithObjectMessage] =
        for {
          channel <- byte.decode
          content <- utf8(ushort).decode
          timestamp <- int.decode
          fingerprint <- utf8(ushort).decode
          receiverId <- varLong.decode
          receiverName <- utf8(ushort).decode
          objects <- list(ushort, Codec[ObjectItem]).decode
        } yield ChatServerCopyWithObjectMessage(channel, content, timestamp, fingerprint, receiverId, receiverName, objects)

      def encode(value: ChatServerCopyWithObjectMessage): ByteVector =
        byte.encode(value.channel) ++
        utf8(ushort).encode(value.content) ++
        int.encode(value.timestamp) ++
        utf8(ushort).encode(value.fingerprint) ++
        varLong.encode(value.receiverId) ++
        utf8(ushort).encode(value.receiverName) ++
        list(ushort, Codec[ObjectItem]).encode(value.objects)
    }
}
