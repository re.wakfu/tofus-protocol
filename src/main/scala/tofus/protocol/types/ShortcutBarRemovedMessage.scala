package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarRemovedMessage(
  barType: Byte,
  slot: Byte
) extends Message(6224)

object ShortcutBarRemovedMessage {
  implicit val codec: Codec[ShortcutBarRemovedMessage] =
    new Codec[ShortcutBarRemovedMessage] {
      def decode: Get[ShortcutBarRemovedMessage] =
        for {
          barType <- byte.decode
          slot <- byte.decode
        } yield ShortcutBarRemovedMessage(barType, slot)

      def encode(value: ShortcutBarRemovedMessage): ByteVector =
        byte.encode(value.barType) ++
        byte.encode(value.slot)
    }
}
