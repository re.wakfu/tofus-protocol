package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChallengeResultMessage(
  challengeId: Short,
  success: Boolean
) extends Message(6019)

object ChallengeResultMessage {
  implicit val codec: Codec[ChallengeResultMessage] =
    new Codec[ChallengeResultMessage] {
      def decode: Get[ChallengeResultMessage] =
        for {
          challengeId <- varShort.decode
          success <- bool.decode
        } yield ChallengeResultMessage(challengeId, success)

      def encode(value: ChallengeResultMessage): ByteVector =
        varShort.encode(value.challengeId) ++
        bool.encode(value.success)
    }
}
