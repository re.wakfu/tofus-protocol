package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentRankUpdateMessage(
  alignmentRank: Byte,
  verbose: Boolean
) extends Message(6058)

object AlignmentRankUpdateMessage {
  implicit val codec: Codec[AlignmentRankUpdateMessage] =
    new Codec[AlignmentRankUpdateMessage] {
      def decode: Get[AlignmentRankUpdateMessage] =
        for {
          alignmentRank <- byte.decode
          verbose <- bool.decode
        } yield AlignmentRankUpdateMessage(alignmentRank, verbose)

      def encode(value: AlignmentRankUpdateMessage): ByteVector =
        byte.encode(value.alignmentRank) ++
        bool.encode(value.verbose)
    }
}
