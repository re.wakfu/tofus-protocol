package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffectMinMax(
  actionId: Short,
  min: Int,
  max: Int
) extends ObjectEffect {
  override val protocolId = 82
}

object ObjectEffectMinMax {
  implicit val codec: Codec[ObjectEffectMinMax] =
    new Codec[ObjectEffectMinMax] {
      def decode: Get[ObjectEffectMinMax] =
        for {
          actionId <- varShort.decode
          min <- varInt.decode
          max <- varInt.decode
        } yield ObjectEffectMinMax(actionId, min, max)

      def encode(value: ObjectEffectMinMax): ByteVector =
        varShort.encode(value.actionId) ++
        varInt.encode(value.min) ++
        varInt.encode(value.max)
    }
}
