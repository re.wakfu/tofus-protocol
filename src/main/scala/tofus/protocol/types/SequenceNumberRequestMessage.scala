package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SequenceNumberRequestMessage(

) extends Message(6316)

object SequenceNumberRequestMessage {
  implicit val codec: Codec[SequenceNumberRequestMessage] =
    Codec.const(SequenceNumberRequestMessage())
}
