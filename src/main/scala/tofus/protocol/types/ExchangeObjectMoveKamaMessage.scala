package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectMoveKamaMessage(
  quantity: Long
) extends Message(5520)

object ExchangeObjectMoveKamaMessage {
  implicit val codec: Codec[ExchangeObjectMoveKamaMessage] =
    new Codec[ExchangeObjectMoveKamaMessage] {
      def decode: Get[ExchangeObjectMoveKamaMessage] =
        for {
          quantity <- varLong.decode
        } yield ExchangeObjectMoveKamaMessage(quantity)

      def encode(value: ExchangeObjectMoveKamaMessage): ByteVector =
        varLong.encode(value.quantity)
    }
}
