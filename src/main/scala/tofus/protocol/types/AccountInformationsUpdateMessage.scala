package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccountInformationsUpdateMessage(
  subscriptionEndDate: Double
) extends Message(6740)

object AccountInformationsUpdateMessage {
  implicit val codec: Codec[AccountInformationsUpdateMessage] =
    new Codec[AccountInformationsUpdateMessage] {
      def decode: Get[AccountInformationsUpdateMessage] =
        for {
          subscriptionEndDate <- double.decode
        } yield AccountInformationsUpdateMessage(subscriptionEndDate)

      def encode(value: AccountInformationsUpdateMessage): ByteVector =
        double.encode(value.subscriptionEndDate)
    }
}
