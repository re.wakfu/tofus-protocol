package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachRoomUnlockRequestMessage(
  roomId: Byte
) extends Message(6863)

object BreachRoomUnlockRequestMessage {
  implicit val codec: Codec[BreachRoomUnlockRequestMessage] =
    new Codec[BreachRoomUnlockRequestMessage] {
      def decode: Get[BreachRoomUnlockRequestMessage] =
        for {
          roomId <- byte.decode
        } yield BreachRoomUnlockRequestMessage(roomId)

      def encode(value: BreachRoomUnlockRequestMessage): ByteVector =
        byte.encode(value.roomId)
    }
}
