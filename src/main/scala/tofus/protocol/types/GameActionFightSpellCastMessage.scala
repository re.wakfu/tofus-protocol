package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightSpellCastMessage(
  actionId: Short,
  sourceId: Double,
  flags0: Byte,
  targetId: Double,
  destinationCellId: Short,
  critical: Byte,
  spellId: Short,
  spellLevel: Short,
  portalsIds: List[Short]
) extends Message(1010)

object GameActionFightSpellCastMessage {
  implicit val codec: Codec[GameActionFightSpellCastMessage] =
    new Codec[GameActionFightSpellCastMessage] {
      def decode: Get[GameActionFightSpellCastMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          flags0 <- byte.decode
          targetId <- double.decode
          destinationCellId <- short.decode
          critical <- byte.decode
          spellId <- varShort.decode
          spellLevel <- short.decode
          portalsIds <- list(ushort, short).decode
        } yield GameActionFightSpellCastMessage(actionId, sourceId, flags0, targetId, destinationCellId, critical, spellId, spellLevel, portalsIds)

      def encode(value: GameActionFightSpellCastMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        byte.encode(value.flags0) ++
        double.encode(value.targetId) ++
        short.encode(value.destinationCellId) ++
        byte.encode(value.critical) ++
        varShort.encode(value.spellId) ++
        short.encode(value.spellLevel) ++
        list(ushort, short).encode(value.portalsIds)
    }
}
