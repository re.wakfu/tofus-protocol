package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LockableChangeCodeMessage(
  code: String
) extends Message(5666)

object LockableChangeCodeMessage {
  implicit val codec: Codec[LockableChangeCodeMessage] =
    new Codec[LockableChangeCodeMessage] {
      def decode: Get[LockableChangeCodeMessage] =
        for {
          code <- utf8(ushort).decode
        } yield LockableChangeCodeMessage(code)

      def encode(value: LockableChangeCodeMessage): ByteVector =
        utf8(ushort).encode(value.code)
    }
}
