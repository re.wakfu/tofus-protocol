package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameDataPaddockObjectRemoveMessage(
  cellId: Short
) extends Message(5993)

object GameDataPaddockObjectRemoveMessage {
  implicit val codec: Codec[GameDataPaddockObjectRemoveMessage] =
    new Codec[GameDataPaddockObjectRemoveMessage] {
      def decode: Get[GameDataPaddockObjectRemoveMessage] =
        for {
          cellId <- varShort.decode
        } yield GameDataPaddockObjectRemoveMessage(cellId)

      def encode(value: GameDataPaddockObjectRemoveMessage): ByteVector =
        varShort.encode(value.cellId)
    }
}
