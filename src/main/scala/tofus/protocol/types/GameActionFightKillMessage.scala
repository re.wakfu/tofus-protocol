package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightKillMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double
) extends Message(5571)

object GameActionFightKillMessage {
  implicit val codec: Codec[GameActionFightKillMessage] =
    new Codec[GameActionFightKillMessage] {
      def decode: Get[GameActionFightKillMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
        } yield GameActionFightKillMessage(actionId, sourceId, targetId)

      def encode(value: GameActionFightKillMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId)
    }
}
