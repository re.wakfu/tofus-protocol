package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class WarnOnPermaDeathStateMessage(
  enable: Boolean
) extends Message(6513)

object WarnOnPermaDeathStateMessage {
  implicit val codec: Codec[WarnOnPermaDeathStateMessage] =
    new Codec[WarnOnPermaDeathStateMessage] {
      def decode: Get[WarnOnPermaDeathStateMessage] =
        for {
          enable <- bool.decode
        } yield WarnOnPermaDeathStateMessage(enable)

      def encode(value: WarnOnPermaDeathStateMessage): ByteVector =
        bool.encode(value.enable)
    }
}
