package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyUpdateLightMessage(
  partyId: Int,
  id: Long,
  lifePoints: Int,
  maxLifePoints: Int,
  prospecting: Short,
  regenRate: Short
) extends Message(6054)

object PartyUpdateLightMessage {
  implicit val codec: Codec[PartyUpdateLightMessage] =
    new Codec[PartyUpdateLightMessage] {
      def decode: Get[PartyUpdateLightMessage] =
        for {
          partyId <- varInt.decode
          id <- varLong.decode
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          prospecting <- varShort.decode
          regenRate <- ubyte.decode
        } yield PartyUpdateLightMessage(partyId, id, lifePoints, maxLifePoints, prospecting, regenRate)

      def encode(value: PartyUpdateLightMessage): ByteVector =
        varInt.encode(value.partyId) ++
        varLong.encode(value.id) ++
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varShort.encode(value.prospecting) ++
        ubyte.encode(value.regenRate)
    }
}
