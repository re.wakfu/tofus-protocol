package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait TaxCollectorComplementaryInformations extends ProtocolType

final case class ConcreteTaxCollectorComplementaryInformations(

) extends TaxCollectorComplementaryInformations {
  override val protocolId = 448
}

object ConcreteTaxCollectorComplementaryInformations {
  implicit val codec: Codec[ConcreteTaxCollectorComplementaryInformations] =  
    Codec.const(ConcreteTaxCollectorComplementaryInformations())
}

object TaxCollectorComplementaryInformations {
  implicit val codec: Codec[TaxCollectorComplementaryInformations] =
    new Codec[TaxCollectorComplementaryInformations] {
      def decode: Get[TaxCollectorComplementaryInformations] =
        ushort.decode.flatMap {
          case 448 => Codec[ConcreteTaxCollectorComplementaryInformations].decode
          case 446 => Codec[TaxCollectorGuildInformations].decode
          case 372 => Codec[TaxCollectorLootInformations].decode
          case 447 => Codec[TaxCollectorWaitingForHelpInformations].decode
        }

      def encode(value: TaxCollectorComplementaryInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteTaxCollectorComplementaryInformations => Codec[ConcreteTaxCollectorComplementaryInformations].encode(i)
          case i: TaxCollectorGuildInformations => Codec[TaxCollectorGuildInformations].encode(i)
          case i: TaxCollectorLootInformations => Codec[TaxCollectorLootInformations].encode(i)
          case i: TaxCollectorWaitingForHelpInformations => Codec[TaxCollectorWaitingForHelpInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
