package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightTurnReadyMessage(
  isReady: Boolean
) extends Message(716)

object GameFightTurnReadyMessage {
  implicit val codec: Codec[GameFightTurnReadyMessage] =
    new Codec[GameFightTurnReadyMessage] {
      def decode: Get[GameFightTurnReadyMessage] =
        for {
          isReady <- bool.decode
        } yield GameFightTurnReadyMessage(isReady)

      def encode(value: GameFightTurnReadyMessage): ByteVector =
        bool.encode(value.isReady)
    }
}
