package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameEntityDispositionErrorMessage(

) extends Message(5695)

object GameEntityDispositionErrorMessage {
  implicit val codec: Codec[GameEntityDispositionErrorMessage] =
    Codec.const(GameEntityDispositionErrorMessage())
}
