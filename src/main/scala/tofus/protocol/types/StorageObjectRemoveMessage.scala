package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StorageObjectRemoveMessage(
  objectUID: Int
) extends Message(5648)

object StorageObjectRemoveMessage {
  implicit val codec: Codec[StorageObjectRemoveMessage] =
    new Codec[StorageObjectRemoveMessage] {
      def decode: Get[StorageObjectRemoveMessage] =
        for {
          objectUID <- varInt.decode
        } yield StorageObjectRemoveMessage(objectUID)

      def encode(value: StorageObjectRemoveMessage): ByteVector =
        varInt.encode(value.objectUID)
    }
}
