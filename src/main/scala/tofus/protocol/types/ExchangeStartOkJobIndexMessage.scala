package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkJobIndexMessage(
  jobs: List[Int]
) extends Message(5819)

object ExchangeStartOkJobIndexMessage {
  implicit val codec: Codec[ExchangeStartOkJobIndexMessage] =
    new Codec[ExchangeStartOkJobIndexMessage] {
      def decode: Get[ExchangeStartOkJobIndexMessage] =
        for {
          jobs <- list(ushort, varInt).decode
        } yield ExchangeStartOkJobIndexMessage(jobs)

      def encode(value: ExchangeStartOkJobIndexMessage): ByteVector =
        list(ushort, varInt).encode(value.jobs)
    }
}
