package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ContactAddFailureMessage(
  reason: Byte
) extends Message(6821)

object ContactAddFailureMessage {
  implicit val codec: Codec[ContactAddFailureMessage] =
    new Codec[ContactAddFailureMessage] {
      def decode: Get[ContactAddFailureMessage] =
        for {
          reason <- byte.decode
        } yield ContactAddFailureMessage(reason)

      def encode(value: ContactAddFailureMessage): ByteVector =
        byte.encode(value.reason)
    }
}
