package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicPongMessage(
  quiet: Boolean
) extends Message(183)

object BasicPongMessage {
  implicit val codec: Codec[BasicPongMessage] =
    new Codec[BasicPongMessage] {
      def decode: Get[BasicPongMessage] =
        for {
          quiet <- bool.decode
        } yield BasicPongMessage(quiet)

      def encode(value: BasicPongMessage): ByteVector =
        bool.encode(value.quiet)
    }
}
