package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceFactsMessage(
  infos: AllianceFactSheetInformations,
  guilds: List[GuildInAllianceInformations],
  controlledSubareaIds: List[Short],
  leaderCharacterId: Long,
  leaderCharacterName: String
) extends Message(6414)

object AllianceFactsMessage {
  implicit val codec: Codec[AllianceFactsMessage] =
    new Codec[AllianceFactsMessage] {
      def decode: Get[AllianceFactsMessage] =
        for {
          infos <- Codec[AllianceFactSheetInformations].decode
          guilds <- list(ushort, Codec[GuildInAllianceInformations]).decode
          controlledSubareaIds <- list(ushort, varShort).decode
          leaderCharacterId <- varLong.decode
          leaderCharacterName <- utf8(ushort).decode
        } yield AllianceFactsMessage(infos, guilds, controlledSubareaIds, leaderCharacterId, leaderCharacterName)

      def encode(value: AllianceFactsMessage): ByteVector =
        Codec[AllianceFactSheetInformations].encode(value.infos) ++
        list(ushort, Codec[GuildInAllianceInformations]).encode(value.guilds) ++
        list(ushort, varShort).encode(value.controlledSubareaIds) ++
        varLong.encode(value.leaderCharacterId) ++
        utf8(ushort).encode(value.leaderCharacterName)
    }
}
