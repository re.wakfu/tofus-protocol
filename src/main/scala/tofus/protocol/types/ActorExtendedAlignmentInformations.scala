package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ActorExtendedAlignmentInformations(
  alignmentSide: Byte,
  alignmentValue: Byte,
  alignmentGrade: Byte,
  characterPower: Double,
  honor: Short,
  honorGradeFloor: Short,
  honorNextGradeFloor: Short,
  aggressable: Byte
) extends ActorAlignmentInformations {
  override val protocolId = 202
}

object ActorExtendedAlignmentInformations {
  implicit val codec: Codec[ActorExtendedAlignmentInformations] =
    new Codec[ActorExtendedAlignmentInformations] {
      def decode: Get[ActorExtendedAlignmentInformations] =
        for {
          alignmentSide <- byte.decode
          alignmentValue <- byte.decode
          alignmentGrade <- byte.decode
          characterPower <- double.decode
          honor <- varShort.decode
          honorGradeFloor <- varShort.decode
          honorNextGradeFloor <- varShort.decode
          aggressable <- byte.decode
        } yield ActorExtendedAlignmentInformations(alignmentSide, alignmentValue, alignmentGrade, characterPower, honor, honorGradeFloor, honorNextGradeFloor, aggressable)

      def encode(value: ActorExtendedAlignmentInformations): ByteVector =
        byte.encode(value.alignmentSide) ++
        byte.encode(value.alignmentValue) ++
        byte.encode(value.alignmentGrade) ++
        double.encode(value.characterPower) ++
        varShort.encode(value.honor) ++
        varShort.encode(value.honorGradeFloor) ++
        varShort.encode(value.honorNextGradeFloor) ++
        byte.encode(value.aggressable)
    }
}
