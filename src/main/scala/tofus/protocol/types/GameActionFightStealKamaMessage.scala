package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightStealKamaMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  amount: Long
) extends Message(5535)

object GameActionFightStealKamaMessage {
  implicit val codec: Codec[GameActionFightStealKamaMessage] =
    new Codec[GameActionFightStealKamaMessage] {
      def decode: Get[GameActionFightStealKamaMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          amount <- varLong.decode
        } yield GameActionFightStealKamaMessage(actionId, sourceId, targetId, amount)

      def encode(value: GameActionFightStealKamaMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        varLong.encode(value.amount)
    }
}
