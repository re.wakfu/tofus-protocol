package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountRenameRequestMessage(
  name: String,
  mountId: Int
) extends Message(5987)

object MountRenameRequestMessage {
  implicit val codec: Codec[MountRenameRequestMessage] =
    new Codec[MountRenameRequestMessage] {
      def decode: Get[MountRenameRequestMessage] =
        for {
          name <- utf8(ushort).decode
          mountId <- varInt.decode
        } yield MountRenameRequestMessage(name, mountId)

      def encode(value: MountRenameRequestMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        varInt.encode(value.mountId)
    }
}
