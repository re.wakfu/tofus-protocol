package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayMutantInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  name: String,
  humanoidInfo: HumanInformations,
  accountId: Int,
  monsterId: Short,
  powerLevel: Byte
) extends GameRolePlayHumanoidInformations {
  override val protocolId = 3
}

object GameRolePlayMutantInformations {
  implicit val codec: Codec[GameRolePlayMutantInformations] =
    new Codec[GameRolePlayMutantInformations] {
      def decode: Get[GameRolePlayMutantInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          name <- utf8(ushort).decode
          humanoidInfo <- Codec[HumanInformations].decode
          accountId <- int.decode
          monsterId <- varShort.decode
          powerLevel <- byte.decode
        } yield GameRolePlayMutantInformations(contextualId, disposition, look, name, humanoidInfo, accountId, monsterId, powerLevel)

      def encode(value: GameRolePlayMutantInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        utf8(ushort).encode(value.name) ++
        Codec[HumanInformations].encode(value.humanoidInfo) ++
        int.encode(value.accountId) ++
        varShort.encode(value.monsterId) ++
        byte.encode(value.powerLevel)
    }
}
