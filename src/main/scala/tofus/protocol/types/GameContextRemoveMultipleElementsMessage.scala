package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextRemoveMultipleElementsMessage(
  elementsIds: List[Double]
) extends Message(252)

object GameContextRemoveMultipleElementsMessage {
  implicit val codec: Codec[GameContextRemoveMultipleElementsMessage] =
    new Codec[GameContextRemoveMultipleElementsMessage] {
      def decode: Get[GameContextRemoveMultipleElementsMessage] =
        for {
          elementsIds <- list(ushort, double).decode
        } yield GameContextRemoveMultipleElementsMessage(elementsIds)

      def encode(value: GameContextRemoveMultipleElementsMessage): ByteVector =
        list(ushort, double).encode(value.elementsIds)
    }
}
