package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AbstractPartyMemberInFightMessage(
  partyId: Int,
  reason: Byte,
  memberId: Long,
  memberAccountId: Int,
  memberName: String,
  fightId: Short,
  timeBeforeFightStart: Short
) extends Message(6825)

object AbstractPartyMemberInFightMessage {
  implicit val codec: Codec[AbstractPartyMemberInFightMessage] =
    new Codec[AbstractPartyMemberInFightMessage] {
      def decode: Get[AbstractPartyMemberInFightMessage] =
        for {
          partyId <- varInt.decode
          reason <- byte.decode
          memberId <- varLong.decode
          memberAccountId <- int.decode
          memberName <- utf8(ushort).decode
          fightId <- varShort.decode
          timeBeforeFightStart <- varShort.decode
        } yield AbstractPartyMemberInFightMessage(partyId, reason, memberId, memberAccountId, memberName, fightId, timeBeforeFightStart)

      def encode(value: AbstractPartyMemberInFightMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.reason) ++
        varLong.encode(value.memberId) ++
        int.encode(value.memberAccountId) ++
        utf8(ushort).encode(value.memberName) ++
        varShort.encode(value.fightId) ++
        varShort.encode(value.timeBeforeFightStart)
    }
}
