package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaSwitchToGameServerMessage(
  validToken: Boolean,
  ticket: ByteVector,
  homeServerId: Short
) extends Message(6574)

object GameRolePlayArenaSwitchToGameServerMessage {
  implicit val codec: Codec[GameRolePlayArenaSwitchToGameServerMessage] =
    new Codec[GameRolePlayArenaSwitchToGameServerMessage] {
      def decode: Get[GameRolePlayArenaSwitchToGameServerMessage] =
        for {
          validToken <- bool.decode
          ticket <- bytes(varInt).decode
          homeServerId <- short.decode
        } yield GameRolePlayArenaSwitchToGameServerMessage(validToken, ticket, homeServerId)

      def encode(value: GameRolePlayArenaSwitchToGameServerMessage): ByteVector =
        bool.encode(value.validToken) ++
        bytes(varInt).encode(value.ticket) ++
        short.encode(value.homeServerId)
    }
}
