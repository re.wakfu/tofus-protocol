package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameActionFightInvisibleDetectedMessage(
  actionId: Short,
  sourceId: Double,
  targetId: Double,
  cellId: Short
) extends Message(6320)

object GameActionFightInvisibleDetectedMessage {
  implicit val codec: Codec[GameActionFightInvisibleDetectedMessage] =
    new Codec[GameActionFightInvisibleDetectedMessage] {
      def decode: Get[GameActionFightInvisibleDetectedMessage] =
        for {
          actionId <- varShort.decode
          sourceId <- double.decode
          targetId <- double.decode
          cellId <- short.decode
        } yield GameActionFightInvisibleDetectedMessage(actionId, sourceId, targetId, cellId)

      def encode(value: GameActionFightInvisibleDetectedMessage): ByteVector =
        varShort.encode(value.actionId) ++
        double.encode(value.sourceId) ++
        double.encode(value.targetId) ++
        short.encode(value.cellId)
    }
}
