package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountFeedRequestMessage(
  mountUid: Int,
  mountLocation: Byte,
  mountFoodUid: Int,
  quantity: Int
) extends Message(6189)

object MountFeedRequestMessage {
  implicit val codec: Codec[MountFeedRequestMessage] =
    new Codec[MountFeedRequestMessage] {
      def decode: Get[MountFeedRequestMessage] =
        for {
          mountUid <- varInt.decode
          mountLocation <- byte.decode
          mountFoodUid <- varInt.decode
          quantity <- varInt.decode
        } yield MountFeedRequestMessage(mountUid, mountLocation, mountFoodUid, quantity)

      def encode(value: MountFeedRequestMessage): ByteVector =
        varInt.encode(value.mountUid) ++
        byte.encode(value.mountLocation) ++
        varInt.encode(value.mountFoodUid) ++
        varInt.encode(value.quantity)
    }
}
