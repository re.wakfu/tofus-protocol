package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NamedPartyTeamWithOutcome(
  team: NamedPartyTeam,
  outcome: Short
) extends ProtocolType {
  override val protocolId = 470
}

object NamedPartyTeamWithOutcome {
  implicit val codec: Codec[NamedPartyTeamWithOutcome] =
    new Codec[NamedPartyTeamWithOutcome] {
      def decode: Get[NamedPartyTeamWithOutcome] =
        for {
          team <- Codec[NamedPartyTeam].decode
          outcome <- varShort.decode
        } yield NamedPartyTeamWithOutcome(team, outcome)

      def encode(value: NamedPartyTeamWithOutcome): ByteVector =
        Codec[NamedPartyTeam].encode(value.team) ++
        varShort.encode(value.outcome)
    }
}
