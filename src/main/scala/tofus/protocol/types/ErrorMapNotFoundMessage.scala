package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ErrorMapNotFoundMessage(
  mapId: Double
) extends Message(6197)

object ErrorMapNotFoundMessage {
  implicit val codec: Codec[ErrorMapNotFoundMessage] =
    new Codec[ErrorMapNotFoundMessage] {
      def decode: Get[ErrorMapNotFoundMessage] =
        for {
          mapId <- double.decode
        } yield ErrorMapNotFoundMessage(mapId)

      def encode(value: ErrorMapNotFoundMessage): ByteVector =
        double.encode(value.mapId)
    }
}
