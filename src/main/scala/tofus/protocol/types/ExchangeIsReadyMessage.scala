package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeIsReadyMessage(
  id: Double,
  ready: Boolean
) extends Message(5509)

object ExchangeIsReadyMessage {
  implicit val codec: Codec[ExchangeIsReadyMessage] =
    new Codec[ExchangeIsReadyMessage] {
      def decode: Get[ExchangeIsReadyMessage] =
        for {
          id <- double.decode
          ready <- bool.decode
        } yield ExchangeIsReadyMessage(id, ready)

      def encode(value: ExchangeIsReadyMessage): ByteVector =
        double.encode(value.id) ++
        bool.encode(value.ready)
    }
}
