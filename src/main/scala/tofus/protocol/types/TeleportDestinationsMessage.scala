package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportDestinationsMessage(
  `type`: Byte,
  destinations: List[TeleportDestination]
) extends Message(6829)

object TeleportDestinationsMessage {
  implicit val codec: Codec[TeleportDestinationsMessage] =
    new Codec[TeleportDestinationsMessage] {
      def decode: Get[TeleportDestinationsMessage] =
        for {
          `type` <- byte.decode
          destinations <- list(ushort, Codec[TeleportDestination]).decode
        } yield TeleportDestinationsMessage(`type`, destinations)

      def encode(value: TeleportDestinationsMessage): ByteVector =
        byte.encode(value.`type`) ++
        list(ushort, Codec[TeleportDestination]).encode(value.destinations)
    }
}
