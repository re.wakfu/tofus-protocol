package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightEffectTriggerCount(
  effectId: Int,
  targetId: Double,
  count: Byte
) extends ProtocolType {
  override val protocolId = 569
}

object GameFightEffectTriggerCount {
  implicit val codec: Codec[GameFightEffectTriggerCount] =
    new Codec[GameFightEffectTriggerCount] {
      def decode: Get[GameFightEffectTriggerCount] =
        for {
          effectId <- varInt.decode
          targetId <- double.decode
          count <- byte.decode
        } yield GameFightEffectTriggerCount(effectId, targetId, count)

      def encode(value: GameFightEffectTriggerCount): ByteVector =
        varInt.encode(value.effectId) ++
        double.encode(value.targetId) ++
        byte.encode(value.count)
    }
}
