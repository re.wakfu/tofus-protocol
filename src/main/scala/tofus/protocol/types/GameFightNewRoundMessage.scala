package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightNewRoundMessage(
  roundNumber: Int
) extends Message(6239)

object GameFightNewRoundMessage {
  implicit val codec: Codec[GameFightNewRoundMessage] =
    new Codec[GameFightNewRoundMessage] {
      def decode: Get[GameFightNewRoundMessage] =
        for {
          roundNumber <- varInt.decode
        } yield GameFightNewRoundMessage(roundNumber)

      def encode(value: GameFightNewRoundMessage): ByteVector =
        varInt.encode(value.roundNumber)
    }
}
