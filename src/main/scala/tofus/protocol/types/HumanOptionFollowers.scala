package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionFollowers(
  followingCharactersLook: List[IndexedEntityLook]
) extends HumanOption {
  override val protocolId = 410
}

object HumanOptionFollowers {
  implicit val codec: Codec[HumanOptionFollowers] =
    new Codec[HumanOptionFollowers] {
      def decode: Get[HumanOptionFollowers] =
        for {
          followingCharactersLook <- list(ushort, Codec[IndexedEntityLook]).decode
        } yield HumanOptionFollowers(followingCharactersLook)

      def encode(value: HumanOptionFollowers): ByteVector =
        list(ushort, Codec[IndexedEntityLook]).encode(value.followingCharactersLook)
    }
}
