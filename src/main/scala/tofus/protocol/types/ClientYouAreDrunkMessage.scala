package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ClientYouAreDrunkMessage(
  level: Byte,
  message: String
) extends Message(6594)

object ClientYouAreDrunkMessage {
  implicit val codec: Codec[ClientYouAreDrunkMessage] =
    new Codec[ClientYouAreDrunkMessage] {
      def decode: Get[ClientYouAreDrunkMessage] =
        for {
          level <- byte.decode
          message <- utf8(ushort).decode
        } yield ClientYouAreDrunkMessage(level, message)

      def encode(value: ClientYouAreDrunkMessage): ByteVector =
        byte.encode(value.level) ++
        utf8(ushort).encode(value.message)
    }
}
