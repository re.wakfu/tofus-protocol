package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInsiderInfoRequestMessage(

) extends Message(6417)

object AllianceInsiderInfoRequestMessage {
  implicit val codec: Codec[AllianceInsiderInfoRequestMessage] =
    Codec.const(AllianceInsiderInfoRequestMessage())
}
