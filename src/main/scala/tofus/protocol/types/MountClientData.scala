package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountClientData(
  flags0: Byte,
  id: Double,
  model: Int,
  ancestor: List[Int],
  behaviors: List[Int],
  name: String,
  ownerId: Int,
  experience: Long,
  experienceForLevel: Long,
  experienceForNextLevel: Double,
  level: Byte,
  maxPods: Int,
  stamina: Int,
  staminaMax: Int,
  maturity: Int,
  maturityForAdult: Int,
  energy: Int,
  energyMax: Int,
  serenity: Int,
  aggressivityMax: Int,
  serenityMax: Int,
  love: Int,
  loveMax: Int,
  fecondationTime: Int,
  boostLimiter: Int,
  boostMax: Double,
  reproductionCount: Int,
  reproductionCountMax: Int,
  harnessGID: Short,
  effectList: List[ObjectEffectInteger]
) extends ProtocolType {
  override val protocolId = 178
}

object MountClientData {
  implicit val codec: Codec[MountClientData] =
    new Codec[MountClientData] {
      def decode: Get[MountClientData] =
        for {
          flags0 <- byte.decode
          id <- double.decode
          model <- varInt.decode
          ancestor <- list(ushort, int).decode
          behaviors <- list(ushort, int).decode
          name <- utf8(ushort).decode
          ownerId <- int.decode
          experience <- varLong.decode
          experienceForLevel <- varLong.decode
          experienceForNextLevel <- double.decode
          level <- byte.decode
          maxPods <- varInt.decode
          stamina <- varInt.decode
          staminaMax <- varInt.decode
          maturity <- varInt.decode
          maturityForAdult <- varInt.decode
          energy <- varInt.decode
          energyMax <- varInt.decode
          serenity <- int.decode
          aggressivityMax <- int.decode
          serenityMax <- varInt.decode
          love <- varInt.decode
          loveMax <- varInt.decode
          fecondationTime <- int.decode
          boostLimiter <- int.decode
          boostMax <- double.decode
          reproductionCount <- int.decode
          reproductionCountMax <- varInt.decode
          harnessGID <- varShort.decode
          effectList <- list(ushort, Codec[ObjectEffectInteger]).decode
        } yield MountClientData(flags0, id, model, ancestor, behaviors, name, ownerId, experience, experienceForLevel, experienceForNextLevel, level, maxPods, stamina, staminaMax, maturity, maturityForAdult, energy, energyMax, serenity, aggressivityMax, serenityMax, love, loveMax, fecondationTime, boostLimiter, boostMax, reproductionCount, reproductionCountMax, harnessGID, effectList)

      def encode(value: MountClientData): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.id) ++
        varInt.encode(value.model) ++
        list(ushort, int).encode(value.ancestor) ++
        list(ushort, int).encode(value.behaviors) ++
        utf8(ushort).encode(value.name) ++
        int.encode(value.ownerId) ++
        varLong.encode(value.experience) ++
        varLong.encode(value.experienceForLevel) ++
        double.encode(value.experienceForNextLevel) ++
        byte.encode(value.level) ++
        varInt.encode(value.maxPods) ++
        varInt.encode(value.stamina) ++
        varInt.encode(value.staminaMax) ++
        varInt.encode(value.maturity) ++
        varInt.encode(value.maturityForAdult) ++
        varInt.encode(value.energy) ++
        varInt.encode(value.energyMax) ++
        int.encode(value.serenity) ++
        int.encode(value.aggressivityMax) ++
        varInt.encode(value.serenityMax) ++
        varInt.encode(value.love) ++
        varInt.encode(value.loveMax) ++
        int.encode(value.fecondationTime) ++
        int.encode(value.boostLimiter) ++
        double.encode(value.boostMax) ++
        int.encode(value.reproductionCount) ++
        varInt.encode(value.reproductionCountMax) ++
        varShort.encode(value.harnessGID) ++
        list(ushort, Codec[ObjectEffectInteger]).encode(value.effectList)
    }
}
