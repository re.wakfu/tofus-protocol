package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EditHavenBagRequestMessage(

) extends Message(6626)

object EditHavenBagRequestMessage {
  implicit val codec: Codec[EditHavenBagRequestMessage] =
    Codec.const(EditHavenBagRequestMessage())
}
