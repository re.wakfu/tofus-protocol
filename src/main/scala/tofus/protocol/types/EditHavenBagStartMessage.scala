package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class EditHavenBagStartMessage(

) extends Message(6632)

object EditHavenBagStartMessage {
  implicit val codec: Codec[EditHavenBagStartMessage] =
    Codec.const(EditHavenBagStartMessage())
}
