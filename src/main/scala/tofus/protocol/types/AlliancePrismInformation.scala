package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlliancePrismInformation(
  typeId: Byte,
  state: Byte,
  nextVulnerabilityDate: Int,
  placementDate: Int,
  rewardTokenCount: Int,
  alliance: ConcreteAllianceInformations
) extends PrismInformation {
  override val protocolId = 427
}

object AlliancePrismInformation {
  implicit val codec: Codec[AlliancePrismInformation] =
    new Codec[AlliancePrismInformation] {
      def decode: Get[AlliancePrismInformation] =
        for {
          typeId <- byte.decode
          state <- byte.decode
          nextVulnerabilityDate <- int.decode
          placementDate <- int.decode
          rewardTokenCount <- varInt.decode
          alliance <- Codec[ConcreteAllianceInformations].decode
        } yield AlliancePrismInformation(typeId, state, nextVulnerabilityDate, placementDate, rewardTokenCount, alliance)

      def encode(value: AlliancePrismInformation): ByteVector =
        byte.encode(value.typeId) ++
        byte.encode(value.state) ++
        int.encode(value.nextVulnerabilityDate) ++
        int.encode(value.placementDate) ++
        varInt.encode(value.rewardTokenCount) ++
        Codec[ConcreteAllianceInformations].encode(value.alliance)
    }
}
