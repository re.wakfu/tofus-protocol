package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntStepFollowDirection(
  direction: Byte,
  mapCount: Short
) extends TreasureHuntStep {
  override val protocolId = 468
}

object TreasureHuntStepFollowDirection {
  implicit val codec: Codec[TreasureHuntStepFollowDirection] =
    new Codec[TreasureHuntStepFollowDirection] {
      def decode: Get[TreasureHuntStepFollowDirection] =
        for {
          direction <- byte.decode
          mapCount <- varShort.decode
        } yield TreasureHuntStepFollowDirection(direction, mapCount)

      def encode(value: TreasureHuntStepFollowDirection): ByteVector =
        byte.encode(value.direction) ++
        varShort.encode(value.mapCount)
    }
}
