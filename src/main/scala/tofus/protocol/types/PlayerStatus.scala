package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PlayerStatus extends ProtocolType

final case class ConcretePlayerStatus(
  statusId: Byte
) extends PlayerStatus {
  override val protocolId = 415
}

object ConcretePlayerStatus {
  implicit val codec: Codec[ConcretePlayerStatus] =  
    new Codec[ConcretePlayerStatus] {
      def decode: Get[ConcretePlayerStatus] =
        for {
          statusId <- byte.decode
        } yield ConcretePlayerStatus(statusId)

      def encode(value: ConcretePlayerStatus): ByteVector =
        byte.encode(value.statusId)
    }
}

object PlayerStatus {
  implicit val codec: Codec[PlayerStatus] =
    new Codec[PlayerStatus] {
      def decode: Get[PlayerStatus] =
        ushort.decode.flatMap {
          case 415 => Codec[ConcretePlayerStatus].decode
          case 414 => Codec[PlayerStatusExtended].decode
        }

      def encode(value: PlayerStatus): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePlayerStatus => Codec[ConcretePlayerStatus].encode(i)
          case i: PlayerStatusExtended => Codec[PlayerStatusExtended].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
