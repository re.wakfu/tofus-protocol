package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class KamasUpdateMessage(
  kamasTotal: Long
) extends Message(5537)

object KamasUpdateMessage {
  implicit val codec: Codec[KamasUpdateMessage] =
    new Codec[KamasUpdateMessage] {
      def decode: Get[KamasUpdateMessage] =
        for {
          kamasTotal <- varLong.decode
        } yield KamasUpdateMessage(kamasTotal)

      def encode(value: KamasUpdateMessage): ByteVector =
        varLong.encode(value.kamasTotal)
    }
}
