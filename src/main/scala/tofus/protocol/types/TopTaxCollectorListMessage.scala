package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TopTaxCollectorListMessage(
  informations: List[TaxCollectorInformations],
  isDungeon: Boolean
) extends Message(6565)

object TopTaxCollectorListMessage {
  implicit val codec: Codec[TopTaxCollectorListMessage] =
    new Codec[TopTaxCollectorListMessage] {
      def decode: Get[TopTaxCollectorListMessage] =
        for {
          informations <- list(ushort, Codec[TaxCollectorInformations]).decode
          isDungeon <- bool.decode
        } yield TopTaxCollectorListMessage(informations, isDungeon)

      def encode(value: TopTaxCollectorListMessage): ByteVector =
        list(ushort, Codec[TaxCollectorInformations]).encode(value.informations) ++
        bool.encode(value.isDungeon)
    }
}
