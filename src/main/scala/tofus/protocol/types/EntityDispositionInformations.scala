package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait EntityDispositionInformations extends ProtocolType

final case class ConcreteEntityDispositionInformations(
  cellId: Short,
  direction: Byte
) extends EntityDispositionInformations {
  override val protocolId = 60
}

object ConcreteEntityDispositionInformations {
  implicit val codec: Codec[ConcreteEntityDispositionInformations] =  
    new Codec[ConcreteEntityDispositionInformations] {
      def decode: Get[ConcreteEntityDispositionInformations] =
        for {
          cellId <- short.decode
          direction <- byte.decode
        } yield ConcreteEntityDispositionInformations(cellId, direction)

      def encode(value: ConcreteEntityDispositionInformations): ByteVector =
        short.encode(value.cellId) ++
        byte.encode(value.direction)
    }
}

object EntityDispositionInformations {
  implicit val codec: Codec[EntityDispositionInformations] =
    new Codec[EntityDispositionInformations] {
      def decode: Get[EntityDispositionInformations] =
        ushort.decode.flatMap {
          case 60 => Codec[ConcreteEntityDispositionInformations].decode
          case 107 => Codec[IdentifiedEntityDispositionInformations].decode
          case 217 => Codec[FightEntityDispositionInformations].decode
        }

      def encode(value: EntityDispositionInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteEntityDispositionInformations => Codec[ConcreteEntityDispositionInformations].encode(i)
          case i: IdentifiedEntityDispositionInformations => Codec[IdentifiedEntityDispositionInformations].encode(i)
          case i: FightEntityDispositionInformations => Codec[FightEntityDispositionInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
