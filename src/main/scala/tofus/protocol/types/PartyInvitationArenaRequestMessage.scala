package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyInvitationArenaRequestMessage(
  name: String
) extends Message(6283)

object PartyInvitationArenaRequestMessage {
  implicit val codec: Codec[PartyInvitationArenaRequestMessage] =
    new Codec[PartyInvitationArenaRequestMessage] {
      def decode: Get[PartyInvitationArenaRequestMessage] =
        for {
          name <- utf8(ushort).decode
        } yield PartyInvitationArenaRequestMessage(name)

      def encode(value: PartyInvitationArenaRequestMessage): ByteVector =
        utf8(ushort).encode(value.name)
    }
}
