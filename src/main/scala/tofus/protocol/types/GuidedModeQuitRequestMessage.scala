package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuidedModeQuitRequestMessage(

) extends Message(6092)

object GuidedModeQuitRequestMessage {
  implicit val codec: Codec[GuidedModeQuitRequestMessage] =
    Codec.const(GuidedModeQuitRequestMessage())
}
