package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayMonsterAngryAtPlayerMessage(
  playerId: Long,
  monsterGroupId: Double,
  angryStartTime: Double,
  attackTime: Double
) extends Message(6741)

object GameRolePlayMonsterAngryAtPlayerMessage {
  implicit val codec: Codec[GameRolePlayMonsterAngryAtPlayerMessage] =
    new Codec[GameRolePlayMonsterAngryAtPlayerMessage] {
      def decode: Get[GameRolePlayMonsterAngryAtPlayerMessage] =
        for {
          playerId <- varLong.decode
          monsterGroupId <- double.decode
          angryStartTime <- double.decode
          attackTime <- double.decode
        } yield GameRolePlayMonsterAngryAtPlayerMessage(playerId, monsterGroupId, angryStartTime, attackTime)

      def encode(value: GameRolePlayMonsterAngryAtPlayerMessage): ByteVector =
        varLong.encode(value.playerId) ++
        double.encode(value.monsterGroupId) ++
        double.encode(value.angryStartTime) ++
        double.encode(value.attackTime)
    }
}
