package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextRefreshEntityLookMessage(
  id: Double,
  look: EntityLook
) extends Message(5637)

object GameContextRefreshEntityLookMessage {
  implicit val codec: Codec[GameContextRefreshEntityLookMessage] =
    new Codec[GameContextRefreshEntityLookMessage] {
      def decode: Get[GameContextRefreshEntityLookMessage] =
        for {
          id <- double.decode
          look <- Codec[EntityLook].decode
        } yield GameContextRefreshEntityLookMessage(id, look)

      def encode(value: GameContextRefreshEntityLookMessage): ByteVector =
        double.encode(value.id) ++
        Codec[EntityLook].encode(value.look)
    }
}
