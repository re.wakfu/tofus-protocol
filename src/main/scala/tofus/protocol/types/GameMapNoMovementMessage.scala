package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapNoMovementMessage(
  cellX: Short,
  cellY: Short
) extends Message(954)

object GameMapNoMovementMessage {
  implicit val codec: Codec[GameMapNoMovementMessage] =
    new Codec[GameMapNoMovementMessage] {
      def decode: Get[GameMapNoMovementMessage] =
        for {
          cellX <- short.decode
          cellY <- short.decode
        } yield GameMapNoMovementMessage(cellX, cellY)

      def encode(value: GameMapNoMovementMessage): ByteVector =
        short.encode(value.cellX) ++
        short.encode(value.cellY)
    }
}
