package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightFighterMonsterLightInformations(
  flags0: Byte,
  id: Double,
  wave: Byte,
  level: Short,
  breed: Byte,
  creatureGenericId: Short
) extends GameFightFighterLightInformations {
  override val protocolId = 455
}

object GameFightFighterMonsterLightInformations {
  implicit val codec: Codec[GameFightFighterMonsterLightInformations] =
    new Codec[GameFightFighterMonsterLightInformations] {
      def decode: Get[GameFightFighterMonsterLightInformations] =
        for {
          flags0 <- byte.decode
          id <- double.decode
          wave <- byte.decode
          level <- varShort.decode
          breed <- byte.decode
          creatureGenericId <- varShort.decode
        } yield GameFightFighterMonsterLightInformations(flags0, id, wave, level, breed, creatureGenericId)

      def encode(value: GameFightFighterMonsterLightInformations): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.id) ++
        byte.encode(value.wave) ++
        varShort.encode(value.level) ++
        byte.encode(value.breed) ++
        varShort.encode(value.creatureGenericId)
    }
}
