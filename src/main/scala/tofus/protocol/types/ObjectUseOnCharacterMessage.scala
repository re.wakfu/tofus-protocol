package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectUseOnCharacterMessage(
  objectUID: Int,
  characterId: Long
) extends Message(3003)

object ObjectUseOnCharacterMessage {
  implicit val codec: Codec[ObjectUseOnCharacterMessage] =
    new Codec[ObjectUseOnCharacterMessage] {
      def decode: Get[ObjectUseOnCharacterMessage] =
        for {
          objectUID <- varInt.decode
          characterId <- varLong.decode
        } yield ObjectUseOnCharacterMessage(objectUID, characterId)

      def encode(value: ObjectUseOnCharacterMessage): ByteVector =
        varInt.encode(value.objectUID) ++
        varLong.encode(value.characterId)
    }
}
