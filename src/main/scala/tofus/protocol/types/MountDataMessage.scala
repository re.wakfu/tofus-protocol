package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountDataMessage(
  mountData: MountClientData
) extends Message(5973)

object MountDataMessage {
  implicit val codec: Codec[MountDataMessage] =
    new Codec[MountDataMessage] {
      def decode: Get[MountDataMessage] =
        for {
          mountData <- Codec[MountClientData].decode
        } yield MountDataMessage(mountData)

      def encode(value: MountDataMessage): ByteVector =
        Codec[MountClientData].encode(value.mountData)
    }
}
