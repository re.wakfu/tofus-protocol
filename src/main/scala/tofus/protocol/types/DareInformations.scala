package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareInformations(
  dareId: Double,
  creator: ConcreteCharacterBasicMinimalInformations,
  subscriptionFee: Long,
  jackpot: Long,
  maxCountWinners: Int,
  endDate: Double,
  isPrivate: Boolean,
  guildId: Int,
  allianceId: Int,
  criterions: List[DareCriteria],
  startDate: Double
) extends ProtocolType {
  override val protocolId = 502
}

object DareInformations {
  implicit val codec: Codec[DareInformations] =
    new Codec[DareInformations] {
      def decode: Get[DareInformations] =
        for {
          dareId <- double.decode
          creator <- Codec[ConcreteCharacterBasicMinimalInformations].decode
          subscriptionFee <- varLong.decode
          jackpot <- varLong.decode
          maxCountWinners <- ushort.decode
          endDate <- double.decode
          isPrivate <- bool.decode
          guildId <- varInt.decode
          allianceId <- varInt.decode
          criterions <- list(ushort, Codec[DareCriteria]).decode
          startDate <- double.decode
        } yield DareInformations(dareId, creator, subscriptionFee, jackpot, maxCountWinners, endDate, isPrivate, guildId, allianceId, criterions, startDate)

      def encode(value: DareInformations): ByteVector =
        double.encode(value.dareId) ++
        Codec[ConcreteCharacterBasicMinimalInformations].encode(value.creator) ++
        varLong.encode(value.subscriptionFee) ++
        varLong.encode(value.jackpot) ++
        ushort.encode(value.maxCountWinners) ++
        double.encode(value.endDate) ++
        bool.encode(value.isPrivate) ++
        varInt.encode(value.guildId) ++
        varInt.encode(value.allianceId) ++
        list(ushort, Codec[DareCriteria]).encode(value.criterions) ++
        double.encode(value.startDate)
    }
}
