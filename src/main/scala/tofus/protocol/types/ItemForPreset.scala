package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ItemForPreset(
  position: Short,
  objGid: Short,
  objUid: Int
) extends ProtocolType {
  override val protocolId = 540
}

object ItemForPreset {
  implicit val codec: Codec[ItemForPreset] =
    new Codec[ItemForPreset] {
      def decode: Get[ItemForPreset] =
        for {
          position <- short.decode
          objGid <- varShort.decode
          objUid <- varInt.decode
        } yield ItemForPreset(position, objGid, objUid)

      def encode(value: ItemForPreset): ByteVector =
        short.encode(value.position) ++
        varShort.encode(value.objGid) ++
        varInt.encode(value.objUid)
    }
}
