package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ObjectItemGenericQuantity extends Item

final case class ConcreteObjectItemGenericQuantity(
  objectGID: Short,
  quantity: Int
) extends ObjectItemGenericQuantity {
  override val protocolId = 483
}

object ConcreteObjectItemGenericQuantity {
  implicit val codec: Codec[ConcreteObjectItemGenericQuantity] =  
    new Codec[ConcreteObjectItemGenericQuantity] {
      def decode: Get[ConcreteObjectItemGenericQuantity] =
        for {
          objectGID <- varShort.decode
          quantity <- varInt.decode
        } yield ConcreteObjectItemGenericQuantity(objectGID, quantity)

      def encode(value: ConcreteObjectItemGenericQuantity): ByteVector =
        varShort.encode(value.objectGID) ++
        varInt.encode(value.quantity)
    }
}

object ObjectItemGenericQuantity {
  implicit val codec: Codec[ObjectItemGenericQuantity] =
    new Codec[ObjectItemGenericQuantity] {
      def decode: Get[ObjectItemGenericQuantity] =
        ushort.decode.flatMap {
          case 483 => Codec[ConcreteObjectItemGenericQuantity].decode
          case 577 => Codec[ObjectItemQuantityPriceDateEffects].decode
        }

      def encode(value: ObjectItemGenericQuantity): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteObjectItemGenericQuantity => Codec[ConcreteObjectItemGenericQuantity].encode(i)
          case i: ObjectItemQuantityPriceDateEffects => Codec[ObjectItemQuantityPriceDateEffects].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
