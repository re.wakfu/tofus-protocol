package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FullStatsPreset(
  id: Short,
  stats: List[CharacterCharacteristicForPreset]
) extends Preset {
  override val protocolId = 532
}

object FullStatsPreset {
  implicit val codec: Codec[FullStatsPreset] =
    new Codec[FullStatsPreset] {
      def decode: Get[FullStatsPreset] =
        for {
          id <- short.decode
          stats <- list(ushort, Codec[CharacterCharacteristicForPreset]).decode
        } yield FullStatsPreset(id, stats)

      def encode(value: FullStatsPreset): ByteVector =
        short.encode(value.id) ++
        list(ushort, Codec[CharacterCharacteristicForPreset]).encode(value.stats)
    }
}
