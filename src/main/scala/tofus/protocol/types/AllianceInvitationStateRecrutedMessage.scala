package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceInvitationStateRecrutedMessage(
  invitationState: Byte
) extends Message(6392)

object AllianceInvitationStateRecrutedMessage {
  implicit val codec: Codec[AllianceInvitationStateRecrutedMessage] =
    new Codec[AllianceInvitationStateRecrutedMessage] {
      def decode: Get[AllianceInvitationStateRecrutedMessage] =
        for {
          invitationState <- byte.decode
        } yield AllianceInvitationStateRecrutedMessage(invitationState)

      def encode(value: AllianceInvitationStateRecrutedMessage): ByteVector =
        byte.encode(value.invitationState)
    }
}
