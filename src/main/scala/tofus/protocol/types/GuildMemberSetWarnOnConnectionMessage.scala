package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMemberSetWarnOnConnectionMessage(
  enable: Boolean
) extends Message(6159)

object GuildMemberSetWarnOnConnectionMessage {
  implicit val codec: Codec[GuildMemberSetWarnOnConnectionMessage] =
    new Codec[GuildMemberSetWarnOnConnectionMessage] {
      def decode: Get[GuildMemberSetWarnOnConnectionMessage] =
        for {
          enable <- bool.decode
        } yield GuildMemberSetWarnOnConnectionMessage(enable)

      def encode(value: GuildMemberSetWarnOnConnectionMessage): ByteVector =
        bool.encode(value.enable)
    }
}
