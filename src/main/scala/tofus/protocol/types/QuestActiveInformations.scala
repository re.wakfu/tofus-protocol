package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait QuestActiveInformations extends ProtocolType

final case class ConcreteQuestActiveInformations(
  questId: Short
) extends QuestActiveInformations {
  override val protocolId = 381
}

object ConcreteQuestActiveInformations {
  implicit val codec: Codec[ConcreteQuestActiveInformations] =  
    new Codec[ConcreteQuestActiveInformations] {
      def decode: Get[ConcreteQuestActiveInformations] =
        for {
          questId <- varShort.decode
        } yield ConcreteQuestActiveInformations(questId)

      def encode(value: ConcreteQuestActiveInformations): ByteVector =
        varShort.encode(value.questId)
    }
}

object QuestActiveInformations {
  implicit val codec: Codec[QuestActiveInformations] =
    new Codec[QuestActiveInformations] {
      def decode: Get[QuestActiveInformations] =
        ushort.decode.flatMap {
          case 381 => Codec[ConcreteQuestActiveInformations].decode
          case 382 => Codec[QuestActiveDetailedInformations].decode
        }

      def encode(value: QuestActiveInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteQuestActiveInformations => Codec[ConcreteQuestActiveInformations].encode(i)
          case i: QuestActiveDetailedInformations => Codec[QuestActiveDetailedInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
