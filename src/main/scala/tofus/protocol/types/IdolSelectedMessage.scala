package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IdolSelectedMessage(
  flags0: Byte,
  idolId: Short
) extends Message(6581)

object IdolSelectedMessage {
  implicit val codec: Codec[IdolSelectedMessage] =
    new Codec[IdolSelectedMessage] {
      def decode: Get[IdolSelectedMessage] =
        for {
          flags0 <- byte.decode
          idolId <- varShort.decode
        } yield IdolSelectedMessage(flags0, idolId)

      def encode(value: IdolSelectedMessage): ByteVector =
        byte.encode(value.flags0) ++
        varShort.encode(value.idolId)
    }
}
