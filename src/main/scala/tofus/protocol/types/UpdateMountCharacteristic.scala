package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait UpdateMountCharacteristic extends ProtocolType

final case class ConcreteUpdateMountCharacteristic(
  `type`: Byte
) extends UpdateMountCharacteristic {
  override val protocolId = 536
}

object ConcreteUpdateMountCharacteristic {
  implicit val codec: Codec[ConcreteUpdateMountCharacteristic] =  
    new Codec[ConcreteUpdateMountCharacteristic] {
      def decode: Get[ConcreteUpdateMountCharacteristic] =
        for {
          `type` <- byte.decode
        } yield ConcreteUpdateMountCharacteristic(`type`)

      def encode(value: ConcreteUpdateMountCharacteristic): ByteVector =
        byte.encode(value.`type`)
    }
}

object UpdateMountCharacteristic {
  implicit val codec: Codec[UpdateMountCharacteristic] =
    new Codec[UpdateMountCharacteristic] {
      def decode: Get[UpdateMountCharacteristic] =
        ushort.decode.flatMap {
          case 536 => Codec[ConcreteUpdateMountCharacteristic].decode
          case 538 => Codec[UpdateMountBooleanCharacteristic].decode
          case 537 => Codec[UpdateMountIntegerCharacteristic].decode
        }

      def encode(value: UpdateMountCharacteristic): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteUpdateMountCharacteristic => Codec[ConcreteUpdateMountCharacteristic].encode(i)
          case i: UpdateMountBooleanCharacteristic => Codec[UpdateMountBooleanCharacteristic].encode(i)
          case i: UpdateMountIntegerCharacteristic => Codec[UpdateMountIntegerCharacteristic].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
