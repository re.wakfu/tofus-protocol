package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameFightFighterLightInformations extends ProtocolType

final case class ConcreteGameFightFighterLightInformations(
  flags0: Byte,
  id: Double,
  wave: Byte,
  level: Short,
  breed: Byte
) extends GameFightFighterLightInformations {
  override val protocolId = 413
}

object ConcreteGameFightFighterLightInformations {
  implicit val codec: Codec[ConcreteGameFightFighterLightInformations] =  
    new Codec[ConcreteGameFightFighterLightInformations] {
      def decode: Get[ConcreteGameFightFighterLightInformations] =
        for {
          flags0 <- byte.decode
          id <- double.decode
          wave <- byte.decode
          level <- varShort.decode
          breed <- byte.decode
        } yield ConcreteGameFightFighterLightInformations(flags0, id, wave, level, breed)

      def encode(value: ConcreteGameFightFighterLightInformations): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.id) ++
        byte.encode(value.wave) ++
        varShort.encode(value.level) ++
        byte.encode(value.breed)
    }
}

object GameFightFighterLightInformations {
  implicit val codec: Codec[GameFightFighterLightInformations] =
    new Codec[GameFightFighterLightInformations] {
      def decode: Get[GameFightFighterLightInformations] =
        ushort.decode.flatMap {
          case 413 => Codec[ConcreteGameFightFighterLightInformations].decode
          case 456 => Codec[GameFightFighterNamedLightInformations].decode
          case 457 => Codec[GameFightFighterTaxCollectorLightInformations].decode
          case 455 => Codec[GameFightFighterMonsterLightInformations].decode
          case 548 => Codec[GameFightFighterEntityLightInformation].decode
        }

      def encode(value: GameFightFighterLightInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameFightFighterLightInformations => Codec[ConcreteGameFightFighterLightInformations].encode(i)
          case i: GameFightFighterNamedLightInformations => Codec[GameFightFighterNamedLightInformations].encode(i)
          case i: GameFightFighterTaxCollectorLightInformations => Codec[GameFightFighterTaxCollectorLightInformations].encode(i)
          case i: GameFightFighterMonsterLightInformations => Codec[GameFightFighterMonsterLightInformations].encode(i)
          case i: GameFightFighterEntityLightInformation => Codec[GameFightFighterEntityLightInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
