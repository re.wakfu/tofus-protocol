package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayArenaFighterStatusMessage(
  fightId: Short,
  playerId: Double,
  accepted: Boolean
) extends Message(6281)

object GameRolePlayArenaFighterStatusMessage {
  implicit val codec: Codec[GameRolePlayArenaFighterStatusMessage] =
    new Codec[GameRolePlayArenaFighterStatusMessage] {
      def decode: Get[GameRolePlayArenaFighterStatusMessage] =
        for {
          fightId <- varShort.decode
          playerId <- double.decode
          accepted <- bool.decode
        } yield GameRolePlayArenaFighterStatusMessage(fightId, playerId, accepted)

      def encode(value: GameRolePlayArenaFighterStatusMessage): ByteVector =
        varShort.encode(value.fightId) ++
        double.encode(value.playerId) ++
        bool.encode(value.accepted)
    }
}
