package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightMonsterWithAlignmentInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  spawnInfo: GameContextBasicSpawnInformation,
  wave: Byte,
  stats: GameFightMinimalStats,
  previousPositions: List[Short],
  creatureGenericId: Short,
  creatureGrade: Byte,
  creatureLevel: Short,
  alignmentInfos: ConcreteActorAlignmentInformations
) extends GameFightMonsterInformations {
  override val protocolId = 203
}

object GameFightMonsterWithAlignmentInformations {
  implicit val codec: Codec[GameFightMonsterWithAlignmentInformations] =
    new Codec[GameFightMonsterWithAlignmentInformations] {
      def decode: Get[GameFightMonsterWithAlignmentInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          spawnInfo <- Codec[GameContextBasicSpawnInformation].decode
          wave <- byte.decode
          stats <- Codec[GameFightMinimalStats].decode
          previousPositions <- list(ushort, varShort).decode
          creatureGenericId <- varShort.decode
          creatureGrade <- byte.decode
          creatureLevel <- short.decode
          alignmentInfos <- Codec[ConcreteActorAlignmentInformations].decode
        } yield GameFightMonsterWithAlignmentInformations(contextualId, disposition, look, spawnInfo, wave, stats, previousPositions, creatureGenericId, creatureGrade, creatureLevel, alignmentInfos)

      def encode(value: GameFightMonsterWithAlignmentInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[GameContextBasicSpawnInformation].encode(value.spawnInfo) ++
        byte.encode(value.wave) ++
        Codec[GameFightMinimalStats].encode(value.stats) ++
        list(ushort, varShort).encode(value.previousPositions) ++
        varShort.encode(value.creatureGenericId) ++
        byte.encode(value.creatureGrade) ++
        short.encode(value.creatureLevel) ++
        Codec[ConcreteActorAlignmentInformations].encode(value.alignmentInfos)
    }
}
