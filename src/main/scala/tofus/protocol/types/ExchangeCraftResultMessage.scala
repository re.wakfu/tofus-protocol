package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeCraftResultMessage(
  craftResult: Byte
) extends Message(5790)

object ExchangeCraftResultMessage {
  implicit val codec: Codec[ExchangeCraftResultMessage] =
    new Codec[ExchangeCraftResultMessage] {
      def decode: Get[ExchangeCraftResultMessage] =
        for {
          craftResult <- byte.decode
        } yield ExchangeCraftResultMessage(craftResult)

      def encode(value: ExchangeCraftResultMessage): ByteVector =
        byte.encode(value.craftResult)
    }
}
