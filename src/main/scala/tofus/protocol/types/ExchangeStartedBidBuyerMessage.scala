package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedBidBuyerMessage(
  buyerDescriptor: SellerBuyerDescriptor
) extends Message(5904)

object ExchangeStartedBidBuyerMessage {
  implicit val codec: Codec[ExchangeStartedBidBuyerMessage] =
    new Codec[ExchangeStartedBidBuyerMessage] {
      def decode: Get[ExchangeStartedBidBuyerMessage] =
        for {
          buyerDescriptor <- Codec[SellerBuyerDescriptor].decode
        } yield ExchangeStartedBidBuyerMessage(buyerDescriptor)

      def encode(value: ExchangeStartedBidBuyerMessage): ByteVector =
        Codec[SellerBuyerDescriptor].encode(value.buyerDescriptor)
    }
}
