package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMembershipMessage(
  guildInfo: ConcreteGuildInformations,
  memberRights: Int
) extends Message(5835)

object GuildMembershipMessage {
  implicit val codec: Codec[GuildMembershipMessage] =
    new Codec[GuildMembershipMessage] {
      def decode: Get[GuildMembershipMessage] =
        for {
          guildInfo <- Codec[ConcreteGuildInformations].decode
          memberRights <- varInt.decode
        } yield GuildMembershipMessage(guildInfo, memberRights)

      def encode(value: GuildMembershipMessage): ByteVector =
        Codec[ConcreteGuildInformations].encode(value.guildInfo) ++
        varInt.encode(value.memberRights)
    }
}
