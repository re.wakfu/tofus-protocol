package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionFinishedMessage(
  flags0: Byte,
  actionId: Int
) extends Message(1304)

object StartupActionFinishedMessage {
  implicit val codec: Codec[StartupActionFinishedMessage] =
    new Codec[StartupActionFinishedMessage] {
      def decode: Get[StartupActionFinishedMessage] =
        for {
          flags0 <- byte.decode
          actionId <- int.decode
        } yield StartupActionFinishedMessage(flags0, actionId)

      def encode(value: StartupActionFinishedMessage): ByteVector =
        byte.encode(value.flags0) ++
        int.encode(value.actionId)
    }
}
