package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayAttackMonsterRequestMessage(
  monsterGroupId: Double
) extends Message(6191)

object GameRolePlayAttackMonsterRequestMessage {
  implicit val codec: Codec[GameRolePlayAttackMonsterRequestMessage] =
    new Codec[GameRolePlayAttackMonsterRequestMessage] {
      def decode: Get[GameRolePlayAttackMonsterRequestMessage] =
        for {
          monsterGroupId <- double.decode
        } yield GameRolePlayAttackMonsterRequestMessage(monsterGroupId)

      def encode(value: GameRolePlayAttackMonsterRequestMessage): ByteVector =
        double.encode(value.monsterGroupId)
    }
}
