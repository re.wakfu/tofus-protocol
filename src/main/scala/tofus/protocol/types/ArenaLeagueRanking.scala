package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ArenaLeagueRanking(
  rank: Short,
  leagueId: Short,
  leaguePoints: Short,
  totalLeaguePoints: Short,
  ladderPosition: Int
) extends ProtocolType {
  override val protocolId = 553
}

object ArenaLeagueRanking {
  implicit val codec: Codec[ArenaLeagueRanking] =
    new Codec[ArenaLeagueRanking] {
      def decode: Get[ArenaLeagueRanking] =
        for {
          rank <- varShort.decode
          leagueId <- varShort.decode
          leaguePoints <- varShort.decode
          totalLeaguePoints <- varShort.decode
          ladderPosition <- int.decode
        } yield ArenaLeagueRanking(rank, leagueId, leaguePoints, totalLeaguePoints, ladderPosition)

      def encode(value: ArenaLeagueRanking): ByteVector =
        varShort.encode(value.rank) ++
        varShort.encode(value.leagueId) ++
        varShort.encode(value.leaguePoints) ++
        varShort.encode(value.totalLeaguePoints) ++
        int.encode(value.ladderPosition)
    }
}
