package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AcquaintanceOnlineInformation(
  accountId: Int,
  accountName: String,
  playerState: Byte,
  playerId: Long,
  playerName: String,
  moodSmileyId: Short,
  status: PlayerStatus
) extends AcquaintanceInformation {
  override val protocolId = 562
}

object AcquaintanceOnlineInformation {
  implicit val codec: Codec[AcquaintanceOnlineInformation] =
    new Codec[AcquaintanceOnlineInformation] {
      def decode: Get[AcquaintanceOnlineInformation] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
          playerState <- byte.decode
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
          moodSmileyId <- varShort.decode
          status <- Codec[PlayerStatus].decode
        } yield AcquaintanceOnlineInformation(accountId, accountName, playerState, playerId, playerName, moodSmileyId, status)

      def encode(value: AcquaintanceOnlineInformation): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName) ++
        byte.encode(value.playerState) ++
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName) ++
        varShort.encode(value.moodSmileyId) ++
        Codec[PlayerStatus].encode(value.status)
    }
}
