package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterFirstSelectionMessage(
  id: Long,
  doTutorial: Boolean
) extends Message(6084)

object CharacterFirstSelectionMessage {
  implicit val codec: Codec[CharacterFirstSelectionMessage] =
    new Codec[CharacterFirstSelectionMessage] {
      def decode: Get[CharacterFirstSelectionMessage] =
        for {
          id <- varLong.decode
          doTutorial <- bool.decode
        } yield CharacterFirstSelectionMessage(id, doTutorial)

      def encode(value: CharacterFirstSelectionMessage): ByteVector =
        varLong.encode(value.id) ++
        bool.encode(value.doTutorial)
    }
}
