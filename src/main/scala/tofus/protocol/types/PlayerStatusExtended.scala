package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PlayerStatusExtended(
  statusId: Byte,
  message: String
) extends PlayerStatus {
  override val protocolId = 414
}

object PlayerStatusExtended {
  implicit val codec: Codec[PlayerStatusExtended] =
    new Codec[PlayerStatusExtended] {
      def decode: Get[PlayerStatusExtended] =
        for {
          statusId <- byte.decode
          message <- utf8(ushort).decode
        } yield PlayerStatusExtended(statusId, message)

      def encode(value: PlayerStatusExtended): ByteVector =
        byte.encode(value.statusId) ++
        utf8(ushort).encode(value.message)
    }
}
