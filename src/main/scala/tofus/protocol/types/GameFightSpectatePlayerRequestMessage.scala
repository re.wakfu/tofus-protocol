package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightSpectatePlayerRequestMessage(
  playerId: Long
) extends Message(6474)

object GameFightSpectatePlayerRequestMessage {
  implicit val codec: Codec[GameFightSpectatePlayerRequestMessage] =
    new Codec[GameFightSpectatePlayerRequestMessage] {
      def decode: Get[GameFightSpectatePlayerRequestMessage] =
        for {
          playerId <- varLong.decode
        } yield GameFightSpectatePlayerRequestMessage(playerId)

      def encode(value: GameFightSpectatePlayerRequestMessage): ByteVector =
        varLong.encode(value.playerId)
    }
}
