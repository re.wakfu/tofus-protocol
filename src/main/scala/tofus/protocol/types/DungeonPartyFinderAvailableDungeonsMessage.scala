package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderAvailableDungeonsMessage(
  dungeonIds: List[Short]
) extends Message(6242)

object DungeonPartyFinderAvailableDungeonsMessage {
  implicit val codec: Codec[DungeonPartyFinderAvailableDungeonsMessage] =
    new Codec[DungeonPartyFinderAvailableDungeonsMessage] {
      def decode: Get[DungeonPartyFinderAvailableDungeonsMessage] =
        for {
          dungeonIds <- list(ushort, varShort).decode
        } yield DungeonPartyFinderAvailableDungeonsMessage(dungeonIds)

      def encode(value: DungeonPartyFinderAvailableDungeonsMessage): ByteVector =
        list(ushort, varShort).encode(value.dungeonIds)
    }
}
