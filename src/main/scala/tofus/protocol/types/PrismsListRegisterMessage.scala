package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismsListRegisterMessage(
  listen: Byte
) extends Message(6441)

object PrismsListRegisterMessage {
  implicit val codec: Codec[PrismsListRegisterMessage] =
    new Codec[PrismsListRegisterMessage] {
      def decode: Get[PrismsListRegisterMessage] =
        for {
          listen <- byte.decode
        } yield PrismsListRegisterMessage(listen)

      def encode(value: PrismsListRegisterMessage): ByteVector =
        byte.encode(value.listen)
    }
}
