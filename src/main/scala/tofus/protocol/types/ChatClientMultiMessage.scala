package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatClientMultiMessage(
  content: String,
  channel: Byte
) extends Message(861)

object ChatClientMultiMessage {
  implicit val codec: Codec[ChatClientMultiMessage] =
    new Codec[ChatClientMultiMessage] {
      def decode: Get[ChatClientMultiMessage] =
        for {
          content <- utf8(ushort).decode
          channel <- byte.decode
        } yield ChatClientMultiMessage(content, channel)

      def encode(value: ChatClientMultiMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        byte.encode(value.channel)
    }
}
