package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorMovementsOfflineMessage(
  movements: List[TaxCollectorMovement]
) extends Message(6611)

object TaxCollectorMovementsOfflineMessage {
  implicit val codec: Codec[TaxCollectorMovementsOfflineMessage] =
    new Codec[TaxCollectorMovementsOfflineMessage] {
      def decode: Get[TaxCollectorMovementsOfflineMessage] =
        for {
          movements <- list(ushort, Codec[TaxCollectorMovement]).decode
        } yield TaxCollectorMovementsOfflineMessage(movements)

      def encode(value: TaxCollectorMovementsOfflineMessage): ByteVector =
        list(ushort, Codec[TaxCollectorMovement]).encode(value.movements)
    }
}
