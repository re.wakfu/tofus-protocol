package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TitlesAndOrnamentsListMessage(
  titles: List[Short],
  ornaments: List[Short],
  activeTitle: Short,
  activeOrnament: Short
) extends Message(6367)

object TitlesAndOrnamentsListMessage {
  implicit val codec: Codec[TitlesAndOrnamentsListMessage] =
    new Codec[TitlesAndOrnamentsListMessage] {
      def decode: Get[TitlesAndOrnamentsListMessage] =
        for {
          titles <- list(ushort, varShort).decode
          ornaments <- list(ushort, varShort).decode
          activeTitle <- varShort.decode
          activeOrnament <- varShort.decode
        } yield TitlesAndOrnamentsListMessage(titles, ornaments, activeTitle, activeOrnament)

      def encode(value: TitlesAndOrnamentsListMessage): ByteVector =
        list(ushort, varShort).encode(value.titles) ++
        list(ushort, varShort).encode(value.ornaments) ++
        varShort.encode(value.activeTitle) ++
        varShort.encode(value.activeOrnament)
    }
}
