package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeMountsStableRemoveMessage(
  mountsId: List[Int]
) extends Message(6556)

object ExchangeMountsStableRemoveMessage {
  implicit val codec: Codec[ExchangeMountsStableRemoveMessage] =
    new Codec[ExchangeMountsStableRemoveMessage] {
      def decode: Get[ExchangeMountsStableRemoveMessage] =
        for {
          mountsId <- list(ushort, varInt).decode
        } yield ExchangeMountsStableRemoveMessage(mountsId)

      def encode(value: ExchangeMountsStableRemoveMessage): ByteVector =
        list(ushort, varInt).encode(value.mountsId)
    }
}
