package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait AbstractContactInformations extends ProtocolType

final case class ConcreteAbstractContactInformations(
  accountId: Int,
  accountName: String
) extends AbstractContactInformations {
  override val protocolId = 380
}

object ConcreteAbstractContactInformations {
  implicit val codec: Codec[ConcreteAbstractContactInformations] =  
    new Codec[ConcreteAbstractContactInformations] {
      def decode: Get[ConcreteAbstractContactInformations] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
        } yield ConcreteAbstractContactInformations(accountId, accountName)

      def encode(value: ConcreteAbstractContactInformations): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName)
    }
}

object AbstractContactInformations {
  implicit val codec: Codec[AbstractContactInformations] =
    new Codec[AbstractContactInformations] {
      def decode: Get[AbstractContactInformations] =
        ushort.decode.flatMap {
          case 380 => Codec[ConcreteAbstractContactInformations].decode
          case 105 => Codec[IgnoredOnlineInformations].decode
          case 106 => Codec[ConcreteIgnoredInformations].decode
          case 92 => Codec[FriendOnlineInformations].decode
          case 78 => Codec[ConcreteFriendInformations].decode
          case 562 => Codec[AcquaintanceOnlineInformation].decode
          case 561 => Codec[ConcreteAcquaintanceInformation].decode
          case 555 => Codec[LeagueFriendInformations].decode
        }

      def encode(value: AbstractContactInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteAbstractContactInformations => Codec[ConcreteAbstractContactInformations].encode(i)
          case i: IgnoredOnlineInformations => Codec[IgnoredOnlineInformations].encode(i)
          case i: ConcreteIgnoredInformations => Codec[ConcreteIgnoredInformations].encode(i)
          case i: FriendOnlineInformations => Codec[FriendOnlineInformations].encode(i)
          case i: ConcreteFriendInformations => Codec[ConcreteFriendInformations].encode(i)
          case i: AcquaintanceOnlineInformation => Codec[AcquaintanceOnlineInformation].encode(i)
          case i: ConcreteAcquaintanceInformation => Codec[ConcreteAcquaintanceInformation].encode(i)
          case i: LeagueFriendInformations => Codec[LeagueFriendInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
