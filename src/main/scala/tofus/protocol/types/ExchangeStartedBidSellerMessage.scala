package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedBidSellerMessage(
  sellerDescriptor: SellerBuyerDescriptor,
  objectsInfos: List[ObjectItemToSellInBid]
) extends Message(5905)

object ExchangeStartedBidSellerMessage {
  implicit val codec: Codec[ExchangeStartedBidSellerMessage] =
    new Codec[ExchangeStartedBidSellerMessage] {
      def decode: Get[ExchangeStartedBidSellerMessage] =
        for {
          sellerDescriptor <- Codec[SellerBuyerDescriptor].decode
          objectsInfos <- list(ushort, Codec[ObjectItemToSellInBid]).decode
        } yield ExchangeStartedBidSellerMessage(sellerDescriptor, objectsInfos)

      def encode(value: ExchangeStartedBidSellerMessage): ByteVector =
        Codec[SellerBuyerDescriptor].encode(value.sellerDescriptor) ++
        list(ushort, Codec[ObjectItemToSellInBid]).encode(value.objectsInfos)
    }
}
