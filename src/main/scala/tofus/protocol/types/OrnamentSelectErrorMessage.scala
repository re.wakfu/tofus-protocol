package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class OrnamentSelectErrorMessage(
  reason: Byte
) extends Message(6370)

object OrnamentSelectErrorMessage {
  implicit val codec: Codec[OrnamentSelectErrorMessage] =
    new Codec[OrnamentSelectErrorMessage] {
      def decode: Get[OrnamentSelectErrorMessage] =
        for {
          reason <- byte.decode
        } yield OrnamentSelectErrorMessage(reason)

      def encode(value: OrnamentSelectErrorMessage): ByteVector =
        byte.encode(value.reason)
    }
}
