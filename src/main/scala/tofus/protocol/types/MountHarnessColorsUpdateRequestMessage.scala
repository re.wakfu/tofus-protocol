package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountHarnessColorsUpdateRequestMessage(
  useHarnessColors: Boolean
) extends Message(6697)

object MountHarnessColorsUpdateRequestMessage {
  implicit val codec: Codec[MountHarnessColorsUpdateRequestMessage] =
    new Codec[MountHarnessColorsUpdateRequestMessage] {
      def decode: Get[MountHarnessColorsUpdateRequestMessage] =
        for {
          useHarnessColors <- bool.decode
        } yield MountHarnessColorsUpdateRequestMessage(useHarnessColors)

      def encode(value: MountHarnessColorsUpdateRequestMessage): ByteVector =
        bool.encode(value.useHarnessColors)
    }
}
