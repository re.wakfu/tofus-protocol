package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachInvitationResponseMessage(
  guest: ConcreteCharacterMinimalInformations,
  accept: Boolean
) extends Message(6792)

object BreachInvitationResponseMessage {
  implicit val codec: Codec[BreachInvitationResponseMessage] =
    new Codec[BreachInvitationResponseMessage] {
      def decode: Get[BreachInvitationResponseMessage] =
        for {
          guest <- Codec[ConcreteCharacterMinimalInformations].decode
          accept <- bool.decode
        } yield BreachInvitationResponseMessage(guest, accept)

      def encode(value: BreachInvitationResponseMessage): ByteVector =
        Codec[ConcreteCharacterMinimalInformations].encode(value.guest) ++
        bool.encode(value.accept)
    }
}
