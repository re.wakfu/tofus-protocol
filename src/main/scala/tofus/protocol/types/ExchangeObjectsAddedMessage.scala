package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectsAddedMessage(
  remote: Boolean,
  `object`: List[ObjectItem]
) extends Message(6535)

object ExchangeObjectsAddedMessage {
  implicit val codec: Codec[ExchangeObjectsAddedMessage] =
    new Codec[ExchangeObjectsAddedMessage] {
      def decode: Get[ExchangeObjectsAddedMessage] =
        for {
          remote <- bool.decode
          `object` <- list(ushort, Codec[ObjectItem]).decode
        } yield ExchangeObjectsAddedMessage(remote, `object`)

      def encode(value: ExchangeObjectsAddedMessage): ByteVector =
        bool.encode(value.remote) ++
        list(ushort, Codec[ObjectItem]).encode(value.`object`)
    }
}
