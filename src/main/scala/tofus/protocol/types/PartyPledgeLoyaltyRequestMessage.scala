package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyPledgeLoyaltyRequestMessage(
  partyId: Int,
  loyal: Boolean
) extends Message(6269)

object PartyPledgeLoyaltyRequestMessage {
  implicit val codec: Codec[PartyPledgeLoyaltyRequestMessage] =
    new Codec[PartyPledgeLoyaltyRequestMessage] {
      def decode: Get[PartyPledgeLoyaltyRequestMessage] =
        for {
          partyId <- varInt.decode
          loyal <- bool.decode
        } yield PartyPledgeLoyaltyRequestMessage(partyId, loyal)

      def encode(value: PartyPledgeLoyaltyRequestMessage): ByteVector =
        varInt.encode(value.partyId) ++
        bool.encode(value.loyal)
    }
}
