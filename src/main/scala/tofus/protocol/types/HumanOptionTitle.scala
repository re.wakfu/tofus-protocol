package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionTitle(
  titleId: Short,
  titleParam: String
) extends HumanOption {
  override val protocolId = 408
}

object HumanOptionTitle {
  implicit val codec: Codec[HumanOptionTitle] =
    new Codec[HumanOptionTitle] {
      def decode: Get[HumanOptionTitle] =
        for {
          titleId <- varShort.decode
          titleParam <- utf8(ushort).decode
        } yield HumanOptionTitle(titleId, titleParam)

      def encode(value: HumanOptionTitle): ByteVector =
        varShort.encode(value.titleId) ++
        utf8(ushort).encode(value.titleParam)
    }
}
