package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceMembershipMessage(
  allianceInfo: ConcreteAllianceInformations,
  enabled: Boolean,
  leadingGuildId: Int
) extends Message(6390)

object AllianceMembershipMessage {
  implicit val codec: Codec[AllianceMembershipMessage] =
    new Codec[AllianceMembershipMessage] {
      def decode: Get[AllianceMembershipMessage] =
        for {
          allianceInfo <- Codec[ConcreteAllianceInformations].decode
          enabled <- bool.decode
          leadingGuildId <- varInt.decode
        } yield AllianceMembershipMessage(allianceInfo, enabled, leadingGuildId)

      def encode(value: AllianceMembershipMessage): ByteVector =
        Codec[ConcreteAllianceInformations].encode(value.allianceInfo) ++
        bool.encode(value.enabled) ++
        varInt.encode(value.leadingGuildId)
    }
}
