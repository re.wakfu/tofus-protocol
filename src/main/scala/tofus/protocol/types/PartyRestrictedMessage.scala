package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyRestrictedMessage(
  partyId: Int,
  restricted: Boolean
) extends Message(6175)

object PartyRestrictedMessage {
  implicit val codec: Codec[PartyRestrictedMessage] =
    new Codec[PartyRestrictedMessage] {
      def decode: Get[PartyRestrictedMessage] =
        for {
          partyId <- varInt.decode
          restricted <- bool.decode
        } yield PartyRestrictedMessage(partyId, restricted)

      def encode(value: PartyRestrictedMessage): ByteVector =
        varInt.encode(value.partyId) ++
        bool.encode(value.restricted)
    }
}
