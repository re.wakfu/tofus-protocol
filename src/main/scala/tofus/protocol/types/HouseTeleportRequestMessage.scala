package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseTeleportRequestMessage(
  houseId: Int,
  houseInstanceId: Int
) extends Message(6726)

object HouseTeleportRequestMessage {
  implicit val codec: Codec[HouseTeleportRequestMessage] =
    new Codec[HouseTeleportRequestMessage] {
      def decode: Get[HouseTeleportRequestMessage] =
        for {
          houseId <- varInt.decode
          houseInstanceId <- int.decode
        } yield HouseTeleportRequestMessage(houseId, houseInstanceId)

      def encode(value: HouseTeleportRequestMessage): ByteVector =
        varInt.encode(value.houseId) ++
        int.encode(value.houseInstanceId)
    }
}
