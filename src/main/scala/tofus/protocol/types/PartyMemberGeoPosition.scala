package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyMemberGeoPosition(
  memberId: Int,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short
) extends ProtocolType {
  override val protocolId = 378
}

object PartyMemberGeoPosition {
  implicit val codec: Codec[PartyMemberGeoPosition] =
    new Codec[PartyMemberGeoPosition] {
      def decode: Get[PartyMemberGeoPosition] =
        for {
          memberId <- int.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
        } yield PartyMemberGeoPosition(memberId, worldX, worldY, mapId, subAreaId)

      def encode(value: PartyMemberGeoPosition): ByteVector =
        int.encode(value.memberId) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId)
    }
}
