package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlignmentWarEffortInformation(
  alignmentSide: Byte,
  alignmentWarEffort: Long
) extends ProtocolType {
  override val protocolId = 571
}

object AlignmentWarEffortInformation {
  implicit val codec: Codec[AlignmentWarEffortInformation] =
    new Codec[AlignmentWarEffortInformation] {
      def decode: Get[AlignmentWarEffortInformation] =
        for {
          alignmentSide <- byte.decode
          alignmentWarEffort <- varLong.decode
        } yield AlignmentWarEffortInformation(alignmentSide, alignmentWarEffort)

      def encode(value: AlignmentWarEffortInformation): ByteVector =
        byte.encode(value.alignmentSide) ++
        varLong.encode(value.alignmentWarEffort)
    }
}
