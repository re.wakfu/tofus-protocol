package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismsListMessage(
  prisms: List[PrismSubareaEmptyInfo]
) extends Message(6440)

object PrismsListMessage {
  implicit val codec: Codec[PrismsListMessage] =
    new Codec[PrismsListMessage] {
      def decode: Get[PrismsListMessage] =
        for {
          prisms <- list(ushort, Codec[PrismSubareaEmptyInfo]).decode
        } yield PrismsListMessage(prisms)

      def encode(value: PrismsListMessage): ByteVector =
        list(ushort, Codec[PrismSubareaEmptyInfo]).encode(value.prisms)
    }
}
