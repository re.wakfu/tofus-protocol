package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceListMessage(
  alliances: List[AllianceFactSheetInformations]
) extends Message(6408)

object AllianceListMessage {
  implicit val codec: Codec[AllianceListMessage] =
    new Codec[AllianceListMessage] {
      def decode: Get[AllianceListMessage] =
        for {
          alliances <- list(ushort, Codec[AllianceFactSheetInformations]).decode
        } yield AllianceListMessage(alliances)

      def encode(value: AllianceListMessage): ByteVector =
        list(ushort, Codec[AllianceFactSheetInformations]).encode(value.alliances)
    }
}
