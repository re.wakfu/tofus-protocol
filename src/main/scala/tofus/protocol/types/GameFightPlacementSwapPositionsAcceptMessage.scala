package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementSwapPositionsAcceptMessage(
  requestId: Int
) extends Message(6547)

object GameFightPlacementSwapPositionsAcceptMessage {
  implicit val codec: Codec[GameFightPlacementSwapPositionsAcceptMessage] =
    new Codec[GameFightPlacementSwapPositionsAcceptMessage] {
      def decode: Get[GameFightPlacementSwapPositionsAcceptMessage] =
        for {
          requestId <- int.decode
        } yield GameFightPlacementSwapPositionsAcceptMessage(requestId)

      def encode(value: GameFightPlacementSwapPositionsAcceptMessage): ByteVector =
        int.encode(value.requestId)
    }
}
