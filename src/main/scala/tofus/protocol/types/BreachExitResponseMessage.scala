package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachExitResponseMessage(
  exited: Boolean
) extends Message(6814)

object BreachExitResponseMessage {
  implicit val codec: Codec[BreachExitResponseMessage] =
    new Codec[BreachExitResponseMessage] {
      def decode: Get[BreachExitResponseMessage] =
        for {
          exited <- bool.decode
        } yield BreachExitResponseMessage(exited)

      def encode(value: BreachExitResponseMessage): ByteVector =
        bool.encode(value.exited)
    }
}
