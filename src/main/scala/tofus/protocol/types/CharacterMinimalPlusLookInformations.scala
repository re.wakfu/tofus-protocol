package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait CharacterMinimalPlusLookInformations extends CharacterMinimalInformations

final case class ConcreteCharacterMinimalPlusLookInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte
) extends CharacterMinimalPlusLookInformations {
  override val protocolId = 163
}

object ConcreteCharacterMinimalPlusLookInformations {
  implicit val codec: Codec[ConcreteCharacterMinimalPlusLookInformations] =  
    new Codec[ConcreteCharacterMinimalPlusLookInformations] {
      def decode: Get[ConcreteCharacterMinimalPlusLookInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
        } yield ConcreteCharacterMinimalPlusLookInformations(id, name, level, entityLook, breed)

      def encode(value: ConcreteCharacterMinimalPlusLookInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed)
    }
}

object CharacterMinimalPlusLookInformations {
  implicit val codec: Codec[CharacterMinimalPlusLookInformations] =
    new Codec[CharacterMinimalPlusLookInformations] {
      def decode: Get[CharacterMinimalPlusLookInformations] =
        ushort.decode.flatMap {
          case 163 => Codec[ConcreteCharacterMinimalPlusLookInformations].decode
          case 391 => Codec[PartyMemberArenaInformations].decode
          case 90 => Codec[ConcretePartyMemberInformations].decode
          case 376 => Codec[PartyInvitationMemberInformations].decode
          case 474 => Codec[CharacterHardcoreOrEpicInformations].decode
          case 45 => Codec[ConcreteCharacterBaseInformations].decode
          case 193 => Codec[CharacterMinimalPlusLookAndGradeInformations].decode
          case 444 => Codec[CharacterMinimalAllianceInformations].decode
          case 445 => Codec[ConcreteCharacterMinimalGuildInformations].decode
        }

      def encode(value: CharacterMinimalPlusLookInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteCharacterMinimalPlusLookInformations => Codec[ConcreteCharacterMinimalPlusLookInformations].encode(i)
          case i: PartyMemberArenaInformations => Codec[PartyMemberArenaInformations].encode(i)
          case i: ConcretePartyMemberInformations => Codec[ConcretePartyMemberInformations].encode(i)
          case i: PartyInvitationMemberInformations => Codec[PartyInvitationMemberInformations].encode(i)
          case i: CharacterHardcoreOrEpicInformations => Codec[CharacterHardcoreOrEpicInformations].encode(i)
          case i: ConcreteCharacterBaseInformations => Codec[ConcreteCharacterBaseInformations].encode(i)
          case i: CharacterMinimalPlusLookAndGradeInformations => Codec[CharacterMinimalPlusLookAndGradeInformations].encode(i)
          case i: CharacterMinimalAllianceInformations => Codec[CharacterMinimalAllianceInformations].encode(i)
          case i: ConcreteCharacterMinimalGuildInformations => Codec[ConcreteCharacterMinimalGuildInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
