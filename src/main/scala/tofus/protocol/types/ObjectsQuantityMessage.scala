package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectsQuantityMessage(
  objectsUIDAndQty: List[ObjectItemQuantity]
) extends Message(6206)

object ObjectsQuantityMessage {
  implicit val codec: Codec[ObjectsQuantityMessage] =
    new Codec[ObjectsQuantityMessage] {
      def decode: Get[ObjectsQuantityMessage] =
        for {
          objectsUIDAndQty <- list(ushort, Codec[ObjectItemQuantity]).decode
        } yield ObjectsQuantityMessage(objectsUIDAndQty)

      def encode(value: ObjectsQuantityMessage): ByteVector =
        list(ushort, Codec[ObjectItemQuantity]).encode(value.objectsUIDAndQty)
    }
}
