package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NicknameRefusedMessage(
  reason: Byte
) extends Message(5638)

object NicknameRefusedMessage {
  implicit val codec: Codec[NicknameRefusedMessage] =
    new Codec[NicknameRefusedMessage] {
      def decode: Get[NicknameRefusedMessage] =
        for {
          reason <- byte.decode
        } yield NicknameRefusedMessage(reason)

      def encode(value: NicknameRefusedMessage): ByteVector =
        byte.encode(value.reason)
    }
}
