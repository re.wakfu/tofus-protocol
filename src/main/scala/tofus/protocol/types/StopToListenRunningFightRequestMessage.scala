package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StopToListenRunningFightRequestMessage(

) extends Message(6124)

object StopToListenRunningFightRequestMessage {
  implicit val codec: Codec[StopToListenRunningFightRequestMessage] =
    Codec.const(StopToListenRunningFightRequestMessage())
}
