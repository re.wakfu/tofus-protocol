package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HaapiShopApiKeyRequestMessage(

) extends Message(6859)

object HaapiShopApiKeyRequestMessage {
  implicit val codec: Codec[HaapiShopApiKeyRequestMessage] =
    Codec.const(HaapiShopApiKeyRequestMessage())
}
