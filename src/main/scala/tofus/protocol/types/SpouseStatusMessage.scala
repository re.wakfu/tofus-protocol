package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SpouseStatusMessage(
  hasSpouse: Boolean
) extends Message(6265)

object SpouseStatusMessage {
  implicit val codec: Codec[SpouseStatusMessage] =
    new Codec[SpouseStatusMessage] {
      def decode: Get[SpouseStatusMessage] =
        for {
          hasSpouse <- bool.decode
        } yield SpouseStatusMessage(hasSpouse)

      def encode(value: SpouseStatusMessage): ByteVector =
        bool.encode(value.hasSpouse)
    }
}
