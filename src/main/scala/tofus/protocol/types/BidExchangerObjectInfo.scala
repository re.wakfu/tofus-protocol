package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BidExchangerObjectInfo(
  objectUID: Int,
  objectGID: Short,
  objectType: Int,
  effects: List[ObjectEffect],
  prices: List[Long]
) extends ProtocolType {
  override val protocolId = 122
}

object BidExchangerObjectInfo {
  implicit val codec: Codec[BidExchangerObjectInfo] =
    new Codec[BidExchangerObjectInfo] {
      def decode: Get[BidExchangerObjectInfo] =
        for {
          objectUID <- varInt.decode
          objectGID <- varShort.decode
          objectType <- int.decode
          effects <- list(ushort, Codec[ObjectEffect]).decode
          prices <- list(ushort, varLong).decode
        } yield BidExchangerObjectInfo(objectUID, objectGID, objectType, effects, prices)

      def encode(value: BidExchangerObjectInfo): ByteVector =
        varInt.encode(value.objectUID) ++
        varShort.encode(value.objectGID) ++
        int.encode(value.objectType) ++
        list(ushort, Codec[ObjectEffect]).encode(value.effects) ++
        list(ushort, varLong).encode(value.prices)
    }
}
