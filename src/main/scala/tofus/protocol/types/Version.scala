package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class Version(
  major: Byte,
  minor: Byte,
  code: Byte,
  build: Int,
  buildType: Byte
) extends ProtocolType {
  override val protocolId = 11
}

object Version {
  implicit val codec: Codec[Version] =
    new Codec[Version] {
      def decode: Get[Version] =
        for {
          major <- byte.decode
          minor <- byte.decode
          code <- byte.decode
          build <- int.decode
          buildType <- byte.decode
        } yield Version(major, minor, code, build, buildType)

      def encode(value: Version): ByteVector =
        byte.encode(value.major) ++
        byte.encode(value.minor) ++
        byte.encode(value.code) ++
        int.encode(value.build) ++
        byte.encode(value.buildType)
    }
}
