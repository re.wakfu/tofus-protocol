package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NicknameRegistrationMessage(

) extends Message(5640)

object NicknameRegistrationMessage {
  implicit val codec: Codec[NicknameRegistrationMessage] =
    Codec.const(NicknameRegistrationMessage())
}
