package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PrismInformation extends ProtocolType

final case class ConcretePrismInformation(
  typeId: Byte,
  state: Byte,
  nextVulnerabilityDate: Int,
  placementDate: Int,
  rewardTokenCount: Int
) extends PrismInformation {
  override val protocolId = 428
}

object ConcretePrismInformation {
  implicit val codec: Codec[ConcretePrismInformation] =  
    new Codec[ConcretePrismInformation] {
      def decode: Get[ConcretePrismInformation] =
        for {
          typeId <- byte.decode
          state <- byte.decode
          nextVulnerabilityDate <- int.decode
          placementDate <- int.decode
          rewardTokenCount <- varInt.decode
        } yield ConcretePrismInformation(typeId, state, nextVulnerabilityDate, placementDate, rewardTokenCount)

      def encode(value: ConcretePrismInformation): ByteVector =
        byte.encode(value.typeId) ++
        byte.encode(value.state) ++
        int.encode(value.nextVulnerabilityDate) ++
        int.encode(value.placementDate) ++
        varInt.encode(value.rewardTokenCount)
    }
}

object PrismInformation {
  implicit val codec: Codec[PrismInformation] =
    new Codec[PrismInformation] {
      def decode: Get[PrismInformation] =
        ushort.decode.flatMap {
          case 428 => Codec[ConcretePrismInformation].decode
          case 431 => Codec[AllianceInsiderPrismInformation].decode
          case 427 => Codec[AlliancePrismInformation].decode
        }

      def encode(value: PrismInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePrismInformation => Codec[ConcretePrismInformation].encode(i)
          case i: AllianceInsiderPrismInformation => Codec[AllianceInsiderPrismInformation].encode(i)
          case i: AlliancePrismInformation => Codec[AlliancePrismInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
