package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayMerchantInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  name: String,
  sellType: Byte,
  options: List[HumanOption]
) extends GameRolePlayNamedActorInformations {
  override val protocolId = 129
}

object GameRolePlayMerchantInformations {
  implicit val codec: Codec[GameRolePlayMerchantInformations] =
    new Codec[GameRolePlayMerchantInformations] {
      def decode: Get[GameRolePlayMerchantInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          name <- utf8(ushort).decode
          sellType <- byte.decode
          options <- list(ushort, Codec[HumanOption]).decode
        } yield GameRolePlayMerchantInformations(contextualId, disposition, look, name, sellType, options)

      def encode(value: GameRolePlayMerchantInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        utf8(ushort).encode(value.name) ++
        byte.encode(value.sellType) ++
        list(ushort, Codec[HumanOption]).encode(value.options)
    }
}
