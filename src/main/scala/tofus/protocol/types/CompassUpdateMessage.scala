package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CompassUpdateMessage(
  `type`: Byte,
  coords: MapCoordinates
) extends Message(5591)

object CompassUpdateMessage {
  implicit val codec: Codec[CompassUpdateMessage] =
    new Codec[CompassUpdateMessage] {
      def decode: Get[CompassUpdateMessage] =
        for {
          `type` <- byte.decode
          coords <- Codec[MapCoordinates].decode
        } yield CompassUpdateMessage(`type`, coords)

      def encode(value: CompassUpdateMessage): ByteVector =
        byte.encode(value.`type`) ++
        Codec[MapCoordinates].encode(value.coords)
    }
}
