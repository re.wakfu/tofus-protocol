package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildHousesInformationMessage(
  housesInformations: List[HouseInformationsForGuild]
) extends Message(5919)

object GuildHousesInformationMessage {
  implicit val codec: Codec[GuildHousesInformationMessage] =
    new Codec[GuildHousesInformationMessage] {
      def decode: Get[GuildHousesInformationMessage] =
        for {
          housesInformations <- list(ushort, Codec[HouseInformationsForGuild]).decode
        } yield GuildHousesInformationMessage(housesInformations)

      def encode(value: GuildHousesInformationMessage): ByteVector =
        list(ushort, Codec[HouseInformationsForGuild]).encode(value.housesInformations)
    }
}
