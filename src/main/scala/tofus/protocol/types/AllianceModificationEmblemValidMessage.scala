package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AllianceModificationEmblemValidMessage(
  Alliancemblem: GuildEmblem
) extends Message(6447)

object AllianceModificationEmblemValidMessage {
  implicit val codec: Codec[AllianceModificationEmblemValidMessage] =
    new Codec[AllianceModificationEmblemValidMessage] {
      def decode: Get[AllianceModificationEmblemValidMessage] =
        for {
          Alliancemblem <- Codec[GuildEmblem].decode
        } yield AllianceModificationEmblemValidMessage(Alliancemblem)

      def encode(value: AllianceModificationEmblemValidMessage): ByteVector =
        Codec[GuildEmblem].encode(value.Alliancemblem)
    }
}
