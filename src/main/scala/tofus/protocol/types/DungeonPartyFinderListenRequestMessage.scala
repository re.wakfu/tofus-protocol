package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DungeonPartyFinderListenRequestMessage(
  dungeonId: Short
) extends Message(6246)

object DungeonPartyFinderListenRequestMessage {
  implicit val codec: Codec[DungeonPartyFinderListenRequestMessage] =
    new Codec[DungeonPartyFinderListenRequestMessage] {
      def decode: Get[DungeonPartyFinderListenRequestMessage] =
        for {
          dungeonId <- varShort.decode
        } yield DungeonPartyFinderListenRequestMessage(dungeonId)

      def encode(value: DungeonPartyFinderListenRequestMessage): ByteVector =
        varShort.encode(value.dungeonId)
    }
}
