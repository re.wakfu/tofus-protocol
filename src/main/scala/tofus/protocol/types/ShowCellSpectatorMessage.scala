package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShowCellSpectatorMessage(
  sourceId: Double,
  cellId: Short,
  playerName: String
) extends Message(6158)

object ShowCellSpectatorMessage {
  implicit val codec: Codec[ShowCellSpectatorMessage] =
    new Codec[ShowCellSpectatorMessage] {
      def decode: Get[ShowCellSpectatorMessage] =
        for {
          sourceId <- double.decode
          cellId <- varShort.decode
          playerName <- utf8(ushort).decode
        } yield ShowCellSpectatorMessage(sourceId, cellId, playerName)

      def encode(value: ShowCellSpectatorMessage): ByteVector =
        double.encode(value.sourceId) ++
        varShort.encode(value.cellId) ++
        utf8(ushort).encode(value.playerName)
    }
}
