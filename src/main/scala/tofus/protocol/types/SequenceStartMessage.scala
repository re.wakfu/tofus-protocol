package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SequenceStartMessage(
  sequenceType: Byte,
  authorId: Double
) extends Message(955)

object SequenceStartMessage {
  implicit val codec: Codec[SequenceStartMessage] =
    new Codec[SequenceStartMessage] {
      def decode: Get[SequenceStartMessage] =
        for {
          sequenceType <- byte.decode
          authorId <- double.decode
        } yield SequenceStartMessage(sequenceType, authorId)

      def encode(value: SequenceStartMessage): ByteVector =
        byte.encode(value.sequenceType) ++
        double.encode(value.authorId)
    }
}
