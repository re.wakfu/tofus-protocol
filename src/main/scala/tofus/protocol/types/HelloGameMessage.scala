package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HelloGameMessage(

) extends Message(101)

object HelloGameMessage {
  implicit val codec: Codec[HelloGameMessage] =
    Codec.const(HelloGameMessage())
}
