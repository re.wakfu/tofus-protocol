package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismsListUpdateMessage(
  prisms: List[PrismSubareaEmptyInfo]
) extends Message(6438)

object PrismsListUpdateMessage {
  implicit val codec: Codec[PrismsListUpdateMessage] =
    new Codec[PrismsListUpdateMessage] {
      def decode: Get[PrismsListUpdateMessage] =
        for {
          prisms <- list(ushort, Codec[PrismSubareaEmptyInfo]).decode
        } yield PrismsListUpdateMessage(prisms)

      def encode(value: PrismsListUpdateMessage): ByteVector =
        list(ushort, Codec[PrismSubareaEmptyInfo]).encode(value.prisms)
    }
}
