package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MonsterBoosts(
  id: Int,
  xpBoost: Short,
  dropBoost: Short
) extends ProtocolType {
  override val protocolId = 497
}

object MonsterBoosts {
  implicit val codec: Codec[MonsterBoosts] =
    new Codec[MonsterBoosts] {
      def decode: Get[MonsterBoosts] =
        for {
          id <- varInt.decode
          xpBoost <- varShort.decode
          dropBoost <- varShort.decode
        } yield MonsterBoosts(id, xpBoost, dropBoost)

      def encode(value: MonsterBoosts): ByteVector =
        varInt.encode(value.id) ++
        varShort.encode(value.xpBoost) ++
        varShort.encode(value.dropBoost)
    }
}
