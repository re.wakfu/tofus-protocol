package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NicknameChoiceRequestMessage(
  nickname: String
) extends Message(5639)

object NicknameChoiceRequestMessage {
  implicit val codec: Codec[NicknameChoiceRequestMessage] =
    new Codec[NicknameChoiceRequestMessage] {
      def decode: Get[NicknameChoiceRequestMessage] =
        for {
          nickname <- utf8(ushort).decode
        } yield NicknameChoiceRequestMessage(nickname)

      def encode(value: NicknameChoiceRequestMessage): ByteVector =
        utf8(ushort).encode(value.nickname)
    }
}
