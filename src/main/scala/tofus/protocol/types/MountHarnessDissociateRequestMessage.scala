package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MountHarnessDissociateRequestMessage(

) extends Message(6696)

object MountHarnessDissociateRequestMessage {
  implicit val codec: Codec[MountHarnessDissociateRequestMessage] =
    Codec.const(MountHarnessDissociateRequestMessage())
}
