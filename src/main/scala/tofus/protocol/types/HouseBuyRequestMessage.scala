package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseBuyRequestMessage(
  proposedPrice: Long
) extends Message(5738)

object HouseBuyRequestMessage {
  implicit val codec: Codec[HouseBuyRequestMessage] =
    new Codec[HouseBuyRequestMessage] {
      def decode: Get[HouseBuyRequestMessage] =
        for {
          proposedPrice <- varLong.decode
        } yield HouseBuyRequestMessage(proposedPrice)

      def encode(value: HouseBuyRequestMessage): ByteVector =
        varLong.encode(value.proposedPrice)
    }
}
