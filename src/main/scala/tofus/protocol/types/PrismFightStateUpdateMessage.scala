package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PrismFightStateUpdateMessage(
  state: Byte
) extends Message(6040)

object PrismFightStateUpdateMessage {
  implicit val codec: Codec[PrismFightStateUpdateMessage] =
    new Codec[PrismFightStateUpdateMessage] {
      def decode: Get[PrismFightStateUpdateMessage] =
        for {
          state <- byte.decode
        } yield PrismFightStateUpdateMessage(state)

      def encode(value: PrismFightStateUpdateMessage): ByteVector =
        byte.encode(value.state)
    }
}
