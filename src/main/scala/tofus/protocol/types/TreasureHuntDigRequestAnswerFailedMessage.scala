package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntDigRequestAnswerFailedMessage(
  questType: Byte,
  result: Byte,
  wrongFlagCount: Byte
) extends Message(6509)

object TreasureHuntDigRequestAnswerFailedMessage {
  implicit val codec: Codec[TreasureHuntDigRequestAnswerFailedMessage] =
    new Codec[TreasureHuntDigRequestAnswerFailedMessage] {
      def decode: Get[TreasureHuntDigRequestAnswerFailedMessage] =
        for {
          questType <- byte.decode
          result <- byte.decode
          wrongFlagCount <- byte.decode
        } yield TreasureHuntDigRequestAnswerFailedMessage(questType, result, wrongFlagCount)

      def encode(value: TreasureHuntDigRequestAnswerFailedMessage): ByteVector =
        byte.encode(value.questType) ++
        byte.encode(value.result) ++
        byte.encode(value.wrongFlagCount)
    }
}
