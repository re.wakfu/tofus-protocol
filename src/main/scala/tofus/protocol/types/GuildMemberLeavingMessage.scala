package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildMemberLeavingMessage(
  kicked: Boolean,
  memberId: Long
) extends Message(5923)

object GuildMemberLeavingMessage {
  implicit val codec: Codec[GuildMemberLeavingMessage] =
    new Codec[GuildMemberLeavingMessage] {
      def decode: Get[GuildMemberLeavingMessage] =
        for {
          kicked <- bool.decode
          memberId <- varLong.decode
        } yield GuildMemberLeavingMessage(kicked, memberId)

      def encode(value: GuildMemberLeavingMessage): ByteVector =
        bool.encode(value.kicked) ++
        varLong.encode(value.memberId)
    }
}
