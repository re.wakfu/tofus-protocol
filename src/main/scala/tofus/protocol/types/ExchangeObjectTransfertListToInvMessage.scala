package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectTransfertListToInvMessage(
  ids: List[Int]
) extends Message(6039)

object ExchangeObjectTransfertListToInvMessage {
  implicit val codec: Codec[ExchangeObjectTransfertListToInvMessage] =
    new Codec[ExchangeObjectTransfertListToInvMessage] {
      def decode: Get[ExchangeObjectTransfertListToInvMessage] =
        for {
          ids <- list(ushort, varInt).decode
        } yield ExchangeObjectTransfertListToInvMessage(ids)

      def encode(value: ExchangeObjectTransfertListToInvMessage): ByteVector =
        list(ushort, varInt).encode(value.ids)
    }
}
