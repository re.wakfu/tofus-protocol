package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CheckIntegrityMessage(
  data: ByteVector
) extends Message(6372)

object CheckIntegrityMessage {
  implicit val codec: Codec[CheckIntegrityMessage] =
    new Codec[CheckIntegrityMessage] {
      def decode: Get[CheckIntegrityMessage] =
        for {
          data <- bytes(varInt).decode
        } yield CheckIntegrityMessage(data)

      def encode(value: CheckIntegrityMessage): ByteVector =
        bytes(varInt).encode(value.data)
    }
}
