package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareSubscribedMessage(
  flags0: Byte,
  dareId: Double,
  dareVersatilesInfos: DareVersatileInformations
) extends Message(6660)

object DareSubscribedMessage {
  implicit val codec: Codec[DareSubscribedMessage] =
    new Codec[DareSubscribedMessage] {
      def decode: Get[DareSubscribedMessage] =
        for {
          flags0 <- byte.decode
          dareId <- double.decode
          dareVersatilesInfos <- Codec[DareVersatileInformations].decode
        } yield DareSubscribedMessage(flags0, dareId, dareVersatilesInfos)

      def encode(value: DareSubscribedMessage): ByteVector =
        byte.encode(value.flags0) ++
        double.encode(value.dareId) ++
        Codec[DareVersatileInformations].encode(value.dareVersatilesInfos)
    }
}
