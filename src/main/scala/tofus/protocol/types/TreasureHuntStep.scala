package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait TreasureHuntStep extends ProtocolType

final case class ConcreteTreasureHuntStep(

) extends TreasureHuntStep {
  override val protocolId = 463
}

object ConcreteTreasureHuntStep {
  implicit val codec: Codec[ConcreteTreasureHuntStep] =  
    Codec.const(ConcreteTreasureHuntStep())
}

object TreasureHuntStep {
  implicit val codec: Codec[TreasureHuntStep] =
    new Codec[TreasureHuntStep] {
      def decode: Get[TreasureHuntStep] =
        ushort.decode.flatMap {
          case 463 => Codec[ConcreteTreasureHuntStep].decode
          case 468 => Codec[TreasureHuntStepFollowDirection].decode
          case 472 => Codec[TreasureHuntStepFollowDirectionToHint].decode
          case 461 => Codec[TreasureHuntStepFollowDirectionToPOI].decode
        }

      def encode(value: TreasureHuntStep): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteTreasureHuntStep => Codec[ConcreteTreasureHuntStep].encode(i)
          case i: TreasureHuntStepFollowDirection => Codec[TreasureHuntStepFollowDirection].encode(i)
          case i: TreasureHuntStepFollowDirectionToHint => Codec[TreasureHuntStepFollowDirectionToHint].encode(i)
          case i: TreasureHuntStepFollowDirectionToPOI => Codec[TreasureHuntStepFollowDirectionToPOI].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
