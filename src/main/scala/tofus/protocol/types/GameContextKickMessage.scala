package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextKickMessage(
  targetId: Double
) extends Message(6081)

object GameContextKickMessage {
  implicit val codec: Codec[GameContextKickMessage] =
    new Codec[GameContextKickMessage] {
      def decode: Get[GameContextKickMessage] =
        for {
          targetId <- double.decode
        } yield GameContextKickMessage(targetId)

      def encode(value: GameContextKickMessage): ByteVector =
        double.encode(value.targetId)
    }
}
