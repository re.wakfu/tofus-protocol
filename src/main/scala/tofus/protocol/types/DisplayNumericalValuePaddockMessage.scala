package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DisplayNumericalValuePaddockMessage(
  rideId: Int,
  value: Int,
  `type`: Byte
) extends Message(6563)

object DisplayNumericalValuePaddockMessage {
  implicit val codec: Codec[DisplayNumericalValuePaddockMessage] =
    new Codec[DisplayNumericalValuePaddockMessage] {
      def decode: Get[DisplayNumericalValuePaddockMessage] =
        for {
          rideId <- int.decode
          value <- int.decode
          `type` <- byte.decode
        } yield DisplayNumericalValuePaddockMessage(rideId, value, `type`)

      def encode(value: DisplayNumericalValuePaddockMessage): ByteVector =
        int.encode(value.rideId) ++
        int.encode(value.value) ++
        byte.encode(value.`type`)
    }
}
