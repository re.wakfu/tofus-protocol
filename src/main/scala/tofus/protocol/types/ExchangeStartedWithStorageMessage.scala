package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartedWithStorageMessage(
  exchangeType: Byte,
  storageMaxSlot: Int
) extends Message(6236)

object ExchangeStartedWithStorageMessage {
  implicit val codec: Codec[ExchangeStartedWithStorageMessage] =
    new Codec[ExchangeStartedWithStorageMessage] {
      def decode: Get[ExchangeStartedWithStorageMessage] =
        for {
          exchangeType <- byte.decode
          storageMaxSlot <- varInt.decode
        } yield ExchangeStartedWithStorageMessage(exchangeType, storageMaxSlot)

      def encode(value: ExchangeStartedWithStorageMessage): ByteVector =
        byte.encode(value.exchangeType) ++
        varInt.encode(value.storageMaxSlot)
    }
}
