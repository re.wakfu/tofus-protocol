package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareCanceledMessage(
  dareId: Double
) extends Message(6679)

object DareCanceledMessage {
  implicit val codec: Codec[DareCanceledMessage] =
    new Codec[DareCanceledMessage] {
      def decode: Get[DareCanceledMessage] =
        for {
          dareId <- double.decode
        } yield DareCanceledMessage(dareId)

      def encode(value: DareCanceledMessage): ByteVector =
        double.encode(value.dareId)
    }
}
