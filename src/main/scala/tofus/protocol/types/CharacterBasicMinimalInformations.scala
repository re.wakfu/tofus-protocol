package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait CharacterBasicMinimalInformations extends AbstractCharacterInformation

final case class ConcreteCharacterBasicMinimalInformations(
  id: Long,
  name: String
) extends CharacterBasicMinimalInformations {
  override val protocolId = 503
}

object ConcreteCharacterBasicMinimalInformations {
  implicit val codec: Codec[ConcreteCharacterBasicMinimalInformations] =  
    new Codec[ConcreteCharacterBasicMinimalInformations] {
      def decode: Get[ConcreteCharacterBasicMinimalInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
        } yield ConcreteCharacterBasicMinimalInformations(id, name)

      def encode(value: ConcreteCharacterBasicMinimalInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name)
    }
}

object CharacterBasicMinimalInformations {
  implicit val codec: Codec[CharacterBasicMinimalInformations] =
    new Codec[CharacterBasicMinimalInformations] {
      def decode: Get[CharacterBasicMinimalInformations] =
        ushort.decode.flatMap {
          case 503 => Codec[ConcreteCharacterBasicMinimalInformations].decode
          case 391 => Codec[PartyMemberArenaInformations].decode
          case 90 => Codec[ConcretePartyMemberInformations].decode
          case 376 => Codec[PartyInvitationMemberInformations].decode
          case 474 => Codec[CharacterHardcoreOrEpicInformations].decode
          case 45 => Codec[ConcreteCharacterBaseInformations].decode
          case 193 => Codec[CharacterMinimalPlusLookAndGradeInformations].decode
          case 444 => Codec[CharacterMinimalAllianceInformations].decode
          case 445 => Codec[ConcreteCharacterMinimalGuildInformations].decode
          case 163 => Codec[ConcreteCharacterMinimalPlusLookInformations].decode
          case 88 => Codec[GuildMember].decode
          case 556 => Codec[CharacterMinimalGuildPublicInformations].decode
          case 110 => Codec[ConcreteCharacterMinimalInformations].decode
        }

      def encode(value: CharacterBasicMinimalInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteCharacterBasicMinimalInformations => Codec[ConcreteCharacterBasicMinimalInformations].encode(i)
          case i: PartyMemberArenaInformations => Codec[PartyMemberArenaInformations].encode(i)
          case i: ConcretePartyMemberInformations => Codec[ConcretePartyMemberInformations].encode(i)
          case i: PartyInvitationMemberInformations => Codec[PartyInvitationMemberInformations].encode(i)
          case i: CharacterHardcoreOrEpicInformations => Codec[CharacterHardcoreOrEpicInformations].encode(i)
          case i: ConcreteCharacterBaseInformations => Codec[ConcreteCharacterBaseInformations].encode(i)
          case i: CharacterMinimalPlusLookAndGradeInformations => Codec[CharacterMinimalPlusLookAndGradeInformations].encode(i)
          case i: CharacterMinimalAllianceInformations => Codec[CharacterMinimalAllianceInformations].encode(i)
          case i: ConcreteCharacterMinimalGuildInformations => Codec[ConcreteCharacterMinimalGuildInformations].encode(i)
          case i: ConcreteCharacterMinimalPlusLookInformations => Codec[ConcreteCharacterMinimalPlusLookInformations].encode(i)
          case i: GuildMember => Codec[GuildMember].encode(i)
          case i: CharacterMinimalGuildPublicInformations => Codec[CharacterMinimalGuildPublicInformations].encode(i)
          case i: ConcreteCharacterMinimalInformations => Codec[ConcreteCharacterMinimalInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
