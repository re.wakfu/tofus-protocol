package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightPlacementPositionRequestMessage(
  cellId: Short
) extends Message(704)

object GameFightPlacementPositionRequestMessage {
  implicit val codec: Codec[GameFightPlacementPositionRequestMessage] =
    new Codec[GameFightPlacementPositionRequestMessage] {
      def decode: Get[GameFightPlacementPositionRequestMessage] =
        for {
          cellId <- varShort.decode
        } yield GameFightPlacementPositionRequestMessage(cellId)

      def encode(value: GameFightPlacementPositionRequestMessage): ByteVector =
        varShort.encode(value.cellId)
    }
}
