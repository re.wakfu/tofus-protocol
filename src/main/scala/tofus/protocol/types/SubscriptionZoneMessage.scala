package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SubscriptionZoneMessage(
  active: Boolean
) extends Message(5573)

object SubscriptionZoneMessage {
  implicit val codec: Codec[SubscriptionZoneMessage] =
    new Codec[SubscriptionZoneMessage] {
      def decode: Get[SubscriptionZoneMessage] =
        for {
          active <- bool.decode
        } yield SubscriptionZoneMessage(active)

      def encode(value: SubscriptionZoneMessage): ByteVector =
        bool.encode(value.active)
    }
}
