package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightOptionStateUpdateMessage(
  fightId: Short,
  teamId: Byte,
  option: Byte,
  state: Boolean
) extends Message(5927)

object GameFightOptionStateUpdateMessage {
  implicit val codec: Codec[GameFightOptionStateUpdateMessage] =
    new Codec[GameFightOptionStateUpdateMessage] {
      def decode: Get[GameFightOptionStateUpdateMessage] =
        for {
          fightId <- varShort.decode
          teamId <- byte.decode
          option <- byte.decode
          state <- bool.decode
        } yield GameFightOptionStateUpdateMessage(fightId, teamId, option, state)

      def encode(value: GameFightOptionStateUpdateMessage): ByteVector =
        varShort.encode(value.fightId) ++
        byte.encode(value.teamId) ++
        byte.encode(value.option) ++
        bool.encode(value.state)
    }
}
