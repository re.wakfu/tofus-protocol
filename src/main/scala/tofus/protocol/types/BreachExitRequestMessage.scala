package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachExitRequestMessage(

) extends Message(6815)

object BreachExitRequestMessage {
  implicit val codec: Codec[BreachExitRequestMessage] =
    Codec.const(BreachExitRequestMessage())
}
