package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkEvolutiveObjectRecycleTradeMessage(

) extends Message(6778)

object ExchangeStartOkEvolutiveObjectRecycleTradeMessage {
  implicit val codec: Codec[ExchangeStartOkEvolutiveObjectRecycleTradeMessage] =
    Codec.const(ExchangeStartOkEvolutiveObjectRecycleTradeMessage())
}
