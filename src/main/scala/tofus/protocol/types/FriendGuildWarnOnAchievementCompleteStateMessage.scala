package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FriendGuildWarnOnAchievementCompleteStateMessage(
  enable: Boolean
) extends Message(6383)

object FriendGuildWarnOnAchievementCompleteStateMessage {
  implicit val codec: Codec[FriendGuildWarnOnAchievementCompleteStateMessage] =
    new Codec[FriendGuildWarnOnAchievementCompleteStateMessage] {
      def decode: Get[FriendGuildWarnOnAchievementCompleteStateMessage] =
        for {
          enable <- bool.decode
        } yield FriendGuildWarnOnAchievementCompleteStateMessage(enable)

      def encode(value: FriendGuildWarnOnAchievementCompleteStateMessage): ByteVector =
        bool.encode(value.enable)
    }
}
