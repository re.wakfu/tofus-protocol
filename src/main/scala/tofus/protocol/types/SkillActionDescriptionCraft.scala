package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SkillActionDescriptionCraft(
  skillId: Short,
  probability: Byte
) extends SkillActionDescription {
  override val protocolId = 100
}

object SkillActionDescriptionCraft {
  implicit val codec: Codec[SkillActionDescriptionCraft] =
    new Codec[SkillActionDescriptionCraft] {
      def decode: Get[SkillActionDescriptionCraft] =
        for {
          skillId <- varShort.decode
          probability <- byte.decode
        } yield SkillActionDescriptionCraft(skillId, probability)

      def encode(value: SkillActionDescriptionCraft): ByteVector =
        varShort.encode(value.skillId) ++
        byte.encode(value.probability)
    }
}
