package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BasicTimeMessage(
  timestamp: Double,
  timezoneOffset: Short
) extends Message(175)

object BasicTimeMessage {
  implicit val codec: Codec[BasicTimeMessage] =
    new Codec[BasicTimeMessage] {
      def decode: Get[BasicTimeMessage] =
        for {
          timestamp <- double.decode
          timezoneOffset <- short.decode
        } yield BasicTimeMessage(timestamp, timezoneOffset)

      def encode(value: BasicTimeMessage): ByteVector =
        double.encode(value.timestamp) ++
        short.encode(value.timezoneOffset)
    }
}
