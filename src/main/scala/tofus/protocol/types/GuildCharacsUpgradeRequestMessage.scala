package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildCharacsUpgradeRequestMessage(
  charaTypeTarget: Byte
) extends Message(5706)

object GuildCharacsUpgradeRequestMessage {
  implicit val codec: Codec[GuildCharacsUpgradeRequestMessage] =
    new Codec[GuildCharacsUpgradeRequestMessage] {
      def decode: Get[GuildCharacsUpgradeRequestMessage] =
        for {
          charaTypeTarget <- byte.decode
        } yield GuildCharacsUpgradeRequestMessage(charaTypeTarget)

      def encode(value: GuildCharacsUpgradeRequestMessage): ByteVector =
        byte.encode(value.charaTypeTarget)
    }
}
