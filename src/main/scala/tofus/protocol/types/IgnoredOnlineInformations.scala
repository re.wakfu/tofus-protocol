package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredOnlineInformations(
  accountId: Int,
  accountName: String,
  playerId: Long,
  playerName: String,
  breed: Byte,
  sex: Boolean
) extends IgnoredInformations {
  override val protocolId = 105
}

object IgnoredOnlineInformations {
  implicit val codec: Codec[IgnoredOnlineInformations] =
    new Codec[IgnoredOnlineInformations] {
      def decode: Get[IgnoredOnlineInformations] =
        for {
          accountId <- int.decode
          accountName <- utf8(ushort).decode
          playerId <- varLong.decode
          playerName <- utf8(ushort).decode
          breed <- byte.decode
          sex <- bool.decode
        } yield IgnoredOnlineInformations(accountId, accountName, playerId, playerName, breed, sex)

      def encode(value: IgnoredOnlineInformations): ByteVector =
        int.encode(value.accountId) ++
        utf8(ushort).encode(value.accountName) ++
        varLong.encode(value.playerId) ++
        utf8(ushort).encode(value.playerName) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex)
    }
}
