package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapRunningFightDetailsMessage(
  fightId: Short,
  attackers: List[GameFightFighterLightInformations],
  defenders: List[GameFightFighterLightInformations]
) extends Message(5751)

object MapRunningFightDetailsMessage {
  implicit val codec: Codec[MapRunningFightDetailsMessage] =
    new Codec[MapRunningFightDetailsMessage] {
      def decode: Get[MapRunningFightDetailsMessage] =
        for {
          fightId <- varShort.decode
          attackers <- list(ushort, Codec[GameFightFighterLightInformations]).decode
          defenders <- list(ushort, Codec[GameFightFighterLightInformations]).decode
        } yield MapRunningFightDetailsMessage(fightId, attackers, defenders)

      def encode(value: MapRunningFightDetailsMessage): ByteVector =
        varShort.encode(value.fightId) ++
        list(ushort, Codec[GameFightFighterLightInformations]).encode(value.attackers) ++
        list(ushort, Codec[GameFightFighterLightInformations]).encode(value.defenders)
    }
}
