package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HavenBagDailyLoteryMessage(
  returnType: Byte,
  gameActionId: String
) extends Message(6644)

object HavenBagDailyLoteryMessage {
  implicit val codec: Codec[HavenBagDailyLoteryMessage] =
    new Codec[HavenBagDailyLoteryMessage] {
      def decode: Get[HavenBagDailyLoteryMessage] =
        for {
          returnType <- byte.decode
          gameActionId <- utf8(ushort).decode
        } yield HavenBagDailyLoteryMessage(returnType, gameActionId)

      def encode(value: HavenBagDailyLoteryMessage): ByteVector =
        byte.encode(value.returnType) ++
        utf8(ushort).encode(value.gameActionId)
    }
}
