package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class StartupActionAddObject(
  uid: Int,
  title: String,
  text: String,
  descUrl: String,
  pictureUrl: String,
  items: List[ObjectItemInformationWithQuantity]
) extends ProtocolType {
  override val protocolId = 52
}

object StartupActionAddObject {
  implicit val codec: Codec[StartupActionAddObject] =
    new Codec[StartupActionAddObject] {
      def decode: Get[StartupActionAddObject] =
        for {
          uid <- int.decode
          title <- utf8(ushort).decode
          text <- utf8(ushort).decode
          descUrl <- utf8(ushort).decode
          pictureUrl <- utf8(ushort).decode
          items <- list(ushort, Codec[ObjectItemInformationWithQuantity]).decode
        } yield StartupActionAddObject(uid, title, text, descUrl, pictureUrl, items)

      def encode(value: StartupActionAddObject): ByteVector =
        int.encode(value.uid) ++
        utf8(ushort).encode(value.title) ++
        utf8(ushort).encode(value.text) ++
        utf8(ushort).encode(value.descUrl) ++
        utf8(ushort).encode(value.pictureUrl) ++
        list(ushort, Codec[ObjectItemInformationWithQuantity]).encode(value.items)
    }
}
