package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntRequestAnswerMessage(
  questType: Byte,
  result: Byte
) extends Message(6489)

object TreasureHuntRequestAnswerMessage {
  implicit val codec: Codec[TreasureHuntRequestAnswerMessage] =
    new Codec[TreasureHuntRequestAnswerMessage] {
      def decode: Get[TreasureHuntRequestAnswerMessage] =
        for {
          questType <- byte.decode
          result <- byte.decode
        } yield TreasureHuntRequestAnswerMessage(questType, result)

      def encode(value: TreasureHuntRequestAnswerMessage): ByteVector =
        byte.encode(value.questType) ++
        byte.encode(value.result)
    }
}
