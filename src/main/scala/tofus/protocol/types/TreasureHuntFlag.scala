package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntFlag(
  mapId: Double,
  state: Byte
) extends ProtocolType {
  override val protocolId = 473
}

object TreasureHuntFlag {
  implicit val codec: Codec[TreasureHuntFlag] =
    new Codec[TreasureHuntFlag] {
      def decode: Get[TreasureHuntFlag] =
        for {
          mapId <- double.decode
          state <- byte.decode
        } yield TreasureHuntFlag(mapId, state)

      def encode(value: TreasureHuntFlag): ByteVector =
        double.encode(value.mapId) ++
        byte.encode(value.state)
    }
}
