package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class IgnoredAddRequestMessage(
  name: String,
  session: Boolean
) extends Message(5673)

object IgnoredAddRequestMessage {
  implicit val codec: Codec[IgnoredAddRequestMessage] =
    new Codec[IgnoredAddRequestMessage] {
      def decode: Get[IgnoredAddRequestMessage] =
        for {
          name <- utf8(ushort).decode
          session <- bool.decode
        } yield IgnoredAddRequestMessage(name, session)

      def encode(value: IgnoredAddRequestMessage): ByteVector =
        utf8(ushort).encode(value.name) ++
        bool.encode(value.session)
    }
}
