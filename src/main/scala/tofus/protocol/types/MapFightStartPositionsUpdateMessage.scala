package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MapFightStartPositionsUpdateMessage(
  mapId: Double,
  fightStartPositions: FightStartingPositions
) extends Message(6716)

object MapFightStartPositionsUpdateMessage {
  implicit val codec: Codec[MapFightStartPositionsUpdateMessage] =
    new Codec[MapFightStartPositionsUpdateMessage] {
      def decode: Get[MapFightStartPositionsUpdateMessage] =
        for {
          mapId <- double.decode
          fightStartPositions <- Codec[FightStartingPositions].decode
        } yield MapFightStartPositionsUpdateMessage(mapId, fightStartPositions)

      def encode(value: MapFightStartPositionsUpdateMessage): ByteVector =
        double.encode(value.mapId) ++
        Codec[FightStartingPositions].encode(value.fightStartPositions)
    }
}
