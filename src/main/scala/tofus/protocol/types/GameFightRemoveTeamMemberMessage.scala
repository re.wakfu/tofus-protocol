package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightRemoveTeamMemberMessage(
  fightId: Short,
  teamId: Byte,
  charId: Double
) extends Message(711)

object GameFightRemoveTeamMemberMessage {
  implicit val codec: Codec[GameFightRemoveTeamMemberMessage] =
    new Codec[GameFightRemoveTeamMemberMessage] {
      def decode: Get[GameFightRemoveTeamMemberMessage] =
        for {
          fightId <- varShort.decode
          teamId <- byte.decode
          charId <- double.decode
        } yield GameFightRemoveTeamMemberMessage(fightId, teamId, charId)

      def encode(value: GameFightRemoveTeamMemberMessage): ByteVector =
        varShort.encode(value.fightId) ++
        byte.encode(value.teamId) ++
        double.encode(value.charId)
    }
}
