package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class NotificationUpdateFlagMessage(
  index: Short
) extends Message(6090)

object NotificationUpdateFlagMessage {
  implicit val codec: Codec[NotificationUpdateFlagMessage] =
    new Codec[NotificationUpdateFlagMessage] {
      def decode: Get[NotificationUpdateFlagMessage] =
        for {
          index <- varShort.decode
        } yield NotificationUpdateFlagMessage(index)

      def encode(value: NotificationUpdateFlagMessage): ByteVector =
        varShort.encode(value.index)
    }
}
