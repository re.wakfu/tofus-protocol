package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LifePointsRegenBeginMessage(
  regenRate: Short
) extends Message(5684)

object LifePointsRegenBeginMessage {
  implicit val codec: Codec[LifePointsRegenBeginMessage] =
    new Codec[LifePointsRegenBeginMessage] {
      def decode: Get[LifePointsRegenBeginMessage] =
        for {
          regenRate <- ubyte.decode
        } yield LifePointsRegenBeginMessage(regenRate)

      def encode(value: LifePointsRegenBeginMessage): ByteVector =
        ubyte.encode(value.regenRate)
    }
}
