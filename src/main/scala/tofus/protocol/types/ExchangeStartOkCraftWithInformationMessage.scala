package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkCraftWithInformationMessage(
  skillId: Int
) extends Message(5941)

object ExchangeStartOkCraftWithInformationMessage {
  implicit val codec: Codec[ExchangeStartOkCraftWithInformationMessage] =
    new Codec[ExchangeStartOkCraftWithInformationMessage] {
      def decode: Get[ExchangeStartOkCraftWithInformationMessage] =
        for {
          skillId <- varInt.decode
        } yield ExchangeStartOkCraftWithInformationMessage(skillId)

      def encode(value: ExchangeStartOkCraftWithInformationMessage): ByteVector =
        varInt.encode(value.skillId)
    }
}
