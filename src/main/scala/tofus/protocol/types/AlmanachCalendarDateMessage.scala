package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlmanachCalendarDateMessage(
  date: Int
) extends Message(6341)

object AlmanachCalendarDateMessage {
  implicit val codec: Codec[AlmanachCalendarDateMessage] =
    new Codec[AlmanachCalendarDateMessage] {
      def decode: Get[AlmanachCalendarDateMessage] =
        for {
          date <- int.decode
        } yield AlmanachCalendarDateMessage(date)

      def encode(value: AlmanachCalendarDateMessage): ByteVector =
        int.encode(value.date)
    }
}
