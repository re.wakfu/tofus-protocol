package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PartyEntityBaseInformation extends ProtocolType

final case class ConcretePartyEntityBaseInformation(
  indexId: Byte,
  entityModelId: Byte,
  entityLook: EntityLook
) extends PartyEntityBaseInformation {
  override val protocolId = 552
}

object ConcretePartyEntityBaseInformation {
  implicit val codec: Codec[ConcretePartyEntityBaseInformation] =  
    new Codec[ConcretePartyEntityBaseInformation] {
      def decode: Get[ConcretePartyEntityBaseInformation] =
        for {
          indexId <- byte.decode
          entityModelId <- byte.decode
          entityLook <- Codec[EntityLook].decode
        } yield ConcretePartyEntityBaseInformation(indexId, entityModelId, entityLook)

      def encode(value: ConcretePartyEntityBaseInformation): ByteVector =
        byte.encode(value.indexId) ++
        byte.encode(value.entityModelId) ++
        Codec[EntityLook].encode(value.entityLook)
    }
}

object PartyEntityBaseInformation {
  implicit val codec: Codec[PartyEntityBaseInformation] =
    new Codec[PartyEntityBaseInformation] {
      def decode: Get[PartyEntityBaseInformation] =
        ushort.decode.flatMap {
          case 552 => Codec[ConcretePartyEntityBaseInformation].decode
          case 550 => Codec[PartyEntityMemberInformation].decode
        }

      def encode(value: PartyEntityBaseInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePartyEntityBaseInformation => Codec[ConcretePartyEntityBaseInformation].encode(i)
          case i: PartyEntityMemberInformation => Codec[PartyEntityMemberInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
