package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextReadyMessage(
  mapId: Double
) extends Message(6071)

object GameContextReadyMessage {
  implicit val codec: Codec[GameContextReadyMessage] =
    new Codec[GameContextReadyMessage] {
      def decode: Get[GameContextReadyMessage] =
        for {
          mapId <- double.decode
        } yield GameContextReadyMessage(mapId)

      def encode(value: GameContextReadyMessage): ByteVector =
        double.encode(value.mapId)
    }
}
