package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ChatClientMultiWithObjectMessage(
  content: String,
  channel: Byte,
  objects: List[ObjectItem]
) extends Message(862)

object ChatClientMultiWithObjectMessage {
  implicit val codec: Codec[ChatClientMultiWithObjectMessage] =
    new Codec[ChatClientMultiWithObjectMessage] {
      def decode: Get[ChatClientMultiWithObjectMessage] =
        for {
          content <- utf8(ushort).decode
          channel <- byte.decode
          objects <- list(ushort, Codec[ObjectItem]).decode
        } yield ChatClientMultiWithObjectMessage(content, channel, objects)

      def encode(value: ChatClientMultiWithObjectMessage): ByteVector =
        utf8(ushort).encode(value.content) ++
        byte.encode(value.channel) ++
        list(ushort, Codec[ObjectItem]).encode(value.objects)
    }
}
