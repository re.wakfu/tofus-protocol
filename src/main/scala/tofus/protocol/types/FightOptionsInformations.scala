package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FightOptionsInformations(
  flags0: Byte
) extends ProtocolType {
  override val protocolId = 20
}

object FightOptionsInformations {
  implicit val codec: Codec[FightOptionsInformations] =
    new Codec[FightOptionsInformations] {
      def decode: Get[FightOptionsInformations] =
        for {
          flags0 <- byte.decode
        } yield FightOptionsInformations(flags0)

      def encode(value: FightOptionsInformations): ByteVector =
        byte.encode(value.flags0)
    }
}
