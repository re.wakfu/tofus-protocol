package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SymbioticObjectErrorMessage(
  reason: Byte,
  errorCode: Byte
) extends Message(6526)

object SymbioticObjectErrorMessage {
  implicit val codec: Codec[SymbioticObjectErrorMessage] =
    new Codec[SymbioticObjectErrorMessage] {
      def decode: Get[SymbioticObjectErrorMessage] =
        for {
          reason <- byte.decode
          errorCode <- byte.decode
        } yield SymbioticObjectErrorMessage(reason, errorCode)

      def encode(value: SymbioticObjectErrorMessage): ByteVector =
        byte.encode(value.reason) ++
        byte.encode(value.errorCode)
    }
}
