package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeTypesExchangerDescriptionForUserMessage(
  objectType: Int,
  typeDescription: List[Int]
) extends Message(5765)

object ExchangeTypesExchangerDescriptionForUserMessage {
  implicit val codec: Codec[ExchangeTypesExchangerDescriptionForUserMessage] =
    new Codec[ExchangeTypesExchangerDescriptionForUserMessage] {
      def decode: Get[ExchangeTypesExchangerDescriptionForUserMessage] =
        for {
          objectType <- int.decode
          typeDescription <- list(ushort, varInt).decode
        } yield ExchangeTypesExchangerDescriptionForUserMessage(objectType, typeDescription)

      def encode(value: ExchangeTypesExchangerDescriptionForUserMessage): ByteVector =
        int.encode(value.objectType) ++
        list(ushort, varInt).encode(value.typeDescription)
    }
}
