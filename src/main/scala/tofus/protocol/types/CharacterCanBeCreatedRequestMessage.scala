package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharacterCanBeCreatedRequestMessage(

) extends Message(6732)

object CharacterCanBeCreatedRequestMessage {
  implicit val codec: Codec[CharacterCanBeCreatedRequestMessage] =
    Codec.const(CharacterCanBeCreatedRequestMessage())
}
