package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class LocalizedChatSmileyMessage(
  entityId: Double,
  smileyId: Short,
  accountId: Int,
  cellId: Short
) extends Message(6185)

object LocalizedChatSmileyMessage {
  implicit val codec: Codec[LocalizedChatSmileyMessage] =
    new Codec[LocalizedChatSmileyMessage] {
      def decode: Get[LocalizedChatSmileyMessage] =
        for {
          entityId <- double.decode
          smileyId <- varShort.decode
          accountId <- int.decode
          cellId <- varShort.decode
        } yield LocalizedChatSmileyMessage(entityId, smileyId, accountId, cellId)

      def encode(value: LocalizedChatSmileyMessage): ByteVector =
        double.encode(value.entityId) ++
        varShort.encode(value.smileyId) ++
        int.encode(value.accountId) ++
        varShort.encode(value.cellId)
    }
}
