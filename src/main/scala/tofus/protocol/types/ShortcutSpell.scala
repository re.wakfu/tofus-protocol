package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutSpell(
  slot: Byte,
  spellId: Short
) extends Shortcut {
  override val protocolId = 368
}

object ShortcutSpell {
  implicit val codec: Codec[ShortcutSpell] =
    new Codec[ShortcutSpell] {
      def decode: Get[ShortcutSpell] =
        for {
          slot <- byte.decode
          spellId <- varShort.decode
        } yield ShortcutSpell(slot, spellId)

      def encode(value: ShortcutSpell): ByteVector =
        byte.encode(value.slot) ++
        varShort.encode(value.spellId)
    }
}
