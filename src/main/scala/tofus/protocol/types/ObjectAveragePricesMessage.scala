package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectAveragePricesMessage(
  ids: List[Short],
  avgPrices: List[Long]
) extends Message(6335)

object ObjectAveragePricesMessage {
  implicit val codec: Codec[ObjectAveragePricesMessage] =
    new Codec[ObjectAveragePricesMessage] {
      def decode: Get[ObjectAveragePricesMessage] =
        for {
          ids <- list(ushort, varShort).decode
          avgPrices <- list(ushort, varLong).decode
        } yield ObjectAveragePricesMessage(ids, avgPrices)

      def encode(value: ObjectAveragePricesMessage): ByteVector =
        list(ushort, varShort).encode(value.ids) ++
        list(ushort, varLong).encode(value.avgPrices)
    }
}
