package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HumanOptionOrnament(
  ornamentId: Short,
  level: Short,
  leagueId: Short,
  ladderPosition: Int
) extends HumanOption {
  override val protocolId = 411
}

object HumanOptionOrnament {
  implicit val codec: Codec[HumanOptionOrnament] =
    new Codec[HumanOptionOrnament] {
      def decode: Get[HumanOptionOrnament] =
        for {
          ornamentId <- varShort.decode
          level <- varShort.decode
          leagueId <- varShort.decode
          ladderPosition <- int.decode
        } yield HumanOptionOrnament(ornamentId, level, leagueId, ladderPosition)

      def encode(value: HumanOptionOrnament): ByteVector =
        varShort.encode(value.ornamentId) ++
        varShort.encode(value.level) ++
        varShort.encode(value.leagueId) ++
        int.encode(value.ladderPosition)
    }
}
