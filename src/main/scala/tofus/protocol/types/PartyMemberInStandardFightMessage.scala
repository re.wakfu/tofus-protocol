package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PartyMemberInStandardFightMessage(
  partyId: Int,
  reason: Byte,
  memberId: Long,
  memberAccountId: Int,
  memberName: String,
  fightId: Short,
  timeBeforeFightStart: Short,
  fightMap: MapCoordinatesExtended
) extends Message(6826)

object PartyMemberInStandardFightMessage {
  implicit val codec: Codec[PartyMemberInStandardFightMessage] =
    new Codec[PartyMemberInStandardFightMessage] {
      def decode: Get[PartyMemberInStandardFightMessage] =
        for {
          partyId <- varInt.decode
          reason <- byte.decode
          memberId <- varLong.decode
          memberAccountId <- int.decode
          memberName <- utf8(ushort).decode
          fightId <- varShort.decode
          timeBeforeFightStart <- varShort.decode
          fightMap <- Codec[MapCoordinatesExtended].decode
        } yield PartyMemberInStandardFightMessage(partyId, reason, memberId, memberAccountId, memberName, fightId, timeBeforeFightStart, fightMap)

      def encode(value: PartyMemberInStandardFightMessage): ByteVector =
        varInt.encode(value.partyId) ++
        byte.encode(value.reason) ++
        varLong.encode(value.memberId) ++
        int.encode(value.memberAccountId) ++
        utf8(ushort).encode(value.memberName) ++
        varShort.encode(value.fightId) ++
        varShort.encode(value.timeBeforeFightStart) ++
        Codec[MapCoordinatesExtended].encode(value.fightMap)
    }
}
