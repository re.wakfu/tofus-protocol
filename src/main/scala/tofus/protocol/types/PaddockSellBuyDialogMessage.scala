package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class PaddockSellBuyDialogMessage(
  bsell: Boolean,
  ownerId: Int,
  price: Long
) extends Message(6018)

object PaddockSellBuyDialogMessage {
  implicit val codec: Codec[PaddockSellBuyDialogMessage] =
    new Codec[PaddockSellBuyDialogMessage] {
      def decode: Get[PaddockSellBuyDialogMessage] =
        for {
          bsell <- bool.decode
          ownerId <- varInt.decode
          price <- varLong.decode
        } yield PaddockSellBuyDialogMessage(bsell, ownerId, price)

      def encode(value: PaddockSellBuyDialogMessage): ByteVector =
        bool.encode(value.bsell) ++
        varInt.encode(value.ownerId) ++
        varLong.encode(value.price)
    }
}
