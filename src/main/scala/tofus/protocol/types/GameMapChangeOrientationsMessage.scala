package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameMapChangeOrientationsMessage(
  orientations: List[ActorOrientation]
) extends Message(6155)

object GameMapChangeOrientationsMessage {
  implicit val codec: Codec[GameMapChangeOrientationsMessage] =
    new Codec[GameMapChangeOrientationsMessage] {
      def decode: Get[GameMapChangeOrientationsMessage] =
        for {
          orientations <- list(ushort, Codec[ActorOrientation]).decode
        } yield GameMapChangeOrientationsMessage(orientations)

      def encode(value: GameMapChangeOrientationsMessage): ByteVector =
        list(ushort, Codec[ActorOrientation]).encode(value.orientations)
    }
}
