package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeStartOkNpcTradeMessage(
  npcId: Double
) extends Message(5785)

object ExchangeStartOkNpcTradeMessage {
  implicit val codec: Codec[ExchangeStartOkNpcTradeMessage] =
    new Codec[ExchangeStartOkNpcTradeMessage] {
      def decode: Get[ExchangeStartOkNpcTradeMessage] =
        for {
          npcId <- double.decode
        } yield ExchangeStartOkNpcTradeMessage(npcId)

      def encode(value: ExchangeStartOkNpcTradeMessage): ByteVector =
        double.encode(value.npcId)
    }
}
