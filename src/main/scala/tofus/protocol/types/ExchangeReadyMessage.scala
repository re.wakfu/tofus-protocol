package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeReadyMessage(
  ready: Boolean,
  step: Short
) extends Message(5511)

object ExchangeReadyMessage {
  implicit val codec: Codec[ExchangeReadyMessage] =
    new Codec[ExchangeReadyMessage] {
      def decode: Get[ExchangeReadyMessage] =
        for {
          ready <- bool.decode
          step <- varShort.decode
        } yield ExchangeReadyMessage(ready, step)

      def encode(value: ExchangeReadyMessage): ByteVector =
        bool.encode(value.ready) ++
        varShort.encode(value.step)
    }
}
