package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FollowQuestObjectiveRequestMessage(
  questId: Short,
  objectiveId: Short
) extends Message(6724)

object FollowQuestObjectiveRequestMessage {
  implicit val codec: Codec[FollowQuestObjectiveRequestMessage] =
    new Codec[FollowQuestObjectiveRequestMessage] {
      def decode: Get[FollowQuestObjectiveRequestMessage] =
        for {
          questId <- varShort.decode
          objectiveId <- short.decode
        } yield FollowQuestObjectiveRequestMessage(questId, objectiveId)

      def encode(value: FollowQuestObjectiveRequestMessage): ByteVector =
        varShort.encode(value.questId) ++
        short.encode(value.objectiveId)
    }
}
