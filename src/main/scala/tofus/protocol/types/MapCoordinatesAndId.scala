package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait MapCoordinatesAndId extends MapCoordinates

final case class ConcreteMapCoordinatesAndId(
  worldX: Short,
  worldY: Short,
  mapId: Double
) extends MapCoordinatesAndId {
  override val protocolId = 392
}

object ConcreteMapCoordinatesAndId {
  implicit val codec: Codec[ConcreteMapCoordinatesAndId] =  
    new Codec[ConcreteMapCoordinatesAndId] {
      def decode: Get[ConcreteMapCoordinatesAndId] =
        for {
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
        } yield ConcreteMapCoordinatesAndId(worldX, worldY, mapId)

      def encode(value: ConcreteMapCoordinatesAndId): ByteVector =
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId)
    }
}

object MapCoordinatesAndId {
  implicit val codec: Codec[MapCoordinatesAndId] =
    new Codec[MapCoordinatesAndId] {
      def decode: Get[MapCoordinatesAndId] =
        ushort.decode.flatMap {
          case 392 => Codec[ConcreteMapCoordinatesAndId].decode
          case 176 => Codec[MapCoordinatesExtended].decode
        }

      def encode(value: MapCoordinatesAndId): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteMapCoordinatesAndId => Codec[ConcreteMapCoordinatesAndId].encode(i)
          case i: MapCoordinatesExtended => Codec[MapCoordinatesExtended].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
