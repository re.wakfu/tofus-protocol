package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRefreshMonsterBoostsMessage(
  monsterBoosts: List[MonsterBoosts],
  familyBoosts: List[MonsterBoosts]
) extends Message(6618)

object GameRefreshMonsterBoostsMessage {
  implicit val codec: Codec[GameRefreshMonsterBoostsMessage] =
    new Codec[GameRefreshMonsterBoostsMessage] {
      def decode: Get[GameRefreshMonsterBoostsMessage] =
        for {
          monsterBoosts <- list(ushort, Codec[MonsterBoosts]).decode
          familyBoosts <- list(ushort, Codec[MonsterBoosts]).decode
        } yield GameRefreshMonsterBoostsMessage(monsterBoosts, familyBoosts)

      def encode(value: GameRefreshMonsterBoostsMessage): ByteVector =
        list(ushort, Codec[MonsterBoosts]).encode(value.monsterBoosts) ++
        list(ushort, Codec[MonsterBoosts]).encode(value.familyBoosts)
    }
}
