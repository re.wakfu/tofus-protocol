package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TaxCollectorDialogQuestionExtendedMessage(
  guildInfo: ConcreteBasicGuildInformations,
  maxPods: Short,
  prospecting: Short,
  wisdom: Short,
  taxCollectorsCount: Byte,
  taxCollectorAttack: Int,
  kamas: Long,
  experience: Long,
  pods: Int,
  itemsValue: Long
) extends Message(5615)

object TaxCollectorDialogQuestionExtendedMessage {
  implicit val codec: Codec[TaxCollectorDialogQuestionExtendedMessage] =
    new Codec[TaxCollectorDialogQuestionExtendedMessage] {
      def decode: Get[TaxCollectorDialogQuestionExtendedMessage] =
        for {
          guildInfo <- Codec[ConcreteBasicGuildInformations].decode
          maxPods <- varShort.decode
          prospecting <- varShort.decode
          wisdom <- varShort.decode
          taxCollectorsCount <- byte.decode
          taxCollectorAttack <- int.decode
          kamas <- varLong.decode
          experience <- varLong.decode
          pods <- varInt.decode
          itemsValue <- varLong.decode
        } yield TaxCollectorDialogQuestionExtendedMessage(guildInfo, maxPods, prospecting, wisdom, taxCollectorsCount, taxCollectorAttack, kamas, experience, pods, itemsValue)

      def encode(value: TaxCollectorDialogQuestionExtendedMessage): ByteVector =
        Codec[ConcreteBasicGuildInformations].encode(value.guildInfo) ++
        varShort.encode(value.maxPods) ++
        varShort.encode(value.prospecting) ++
        varShort.encode(value.wisdom) ++
        byte.encode(value.taxCollectorsCount) ++
        int.encode(value.taxCollectorAttack) ++
        varLong.encode(value.kamas) ++
        varLong.encode(value.experience) ++
        varInt.encode(value.pods) ++
        varLong.encode(value.itemsValue)
    }
}
