package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class JobCrafterDirectorySettings(
  jobId: Byte,
  minLevel: Short,
  free: Boolean
) extends ProtocolType {
  override val protocolId = 97
}

object JobCrafterDirectorySettings {
  implicit val codec: Codec[JobCrafterDirectorySettings] =
    new Codec[JobCrafterDirectorySettings] {
      def decode: Get[JobCrafterDirectorySettings] =
        for {
          jobId <- byte.decode
          minLevel <- ubyte.decode
          free <- bool.decode
        } yield JobCrafterDirectorySettings(jobId, minLevel, free)

      def encode(value: JobCrafterDirectorySettings): ByteVector =
        byte.encode(value.jobId) ++
        ubyte.encode(value.minLevel) ++
        bool.encode(value.free)
    }
}
