package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameContextRemoveElementWithEventMessage(
  id: Double,
  elementEventId: Byte
) extends Message(6412)

object GameContextRemoveElementWithEventMessage {
  implicit val codec: Codec[GameContextRemoveElementWithEventMessage] =
    new Codec[GameContextRemoveElementWithEventMessage] {
      def decode: Get[GameContextRemoveElementWithEventMessage] =
        for {
          id <- double.decode
          elementEventId <- byte.decode
        } yield GameContextRemoveElementWithEventMessage(id, elementEventId)

      def encode(value: GameContextRemoveElementWithEventMessage): ByteVector =
        double.encode(value.id) ++
        byte.encode(value.elementEventId)
    }
}
