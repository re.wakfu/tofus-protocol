package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TeleportToBuddyCloseMessage(
  dungeonId: Short,
  buddyId: Long
) extends Message(6303)

object TeleportToBuddyCloseMessage {
  implicit val codec: Codec[TeleportToBuddyCloseMessage] =
    new Codec[TeleportToBuddyCloseMessage] {
      def decode: Get[TeleportToBuddyCloseMessage] =
        for {
          dungeonId <- varShort.decode
          buddyId <- varLong.decode
        } yield TeleportToBuddyCloseMessage(dungeonId, buddyId)

      def encode(value: TeleportToBuddyCloseMessage): ByteVector =
        varShort.encode(value.dungeonId) ++
        varLong.encode(value.buddyId)
    }
}
