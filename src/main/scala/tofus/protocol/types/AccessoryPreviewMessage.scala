package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AccessoryPreviewMessage(
  look: EntityLook
) extends Message(6517)

object AccessoryPreviewMessage {
  implicit val codec: Codec[AccessoryPreviewMessage] =
    new Codec[AccessoryPreviewMessage] {
      def decode: Get[AccessoryPreviewMessage] =
        for {
          look <- Codec[EntityLook].decode
        } yield AccessoryPreviewMessage(look)

      def encode(value: AccessoryPreviewMessage): ByteVector =
        Codec[EntityLook].encode(value.look)
    }
}
