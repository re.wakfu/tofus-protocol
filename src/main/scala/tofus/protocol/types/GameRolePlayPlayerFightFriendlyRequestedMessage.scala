package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayPlayerFightFriendlyRequestedMessage(
  fightId: Short,
  sourceId: Long,
  targetId: Long
) extends Message(5937)

object GameRolePlayPlayerFightFriendlyRequestedMessage {
  implicit val codec: Codec[GameRolePlayPlayerFightFriendlyRequestedMessage] =
    new Codec[GameRolePlayPlayerFightFriendlyRequestedMessage] {
      def decode: Get[GameRolePlayPlayerFightFriendlyRequestedMessage] =
        for {
          fightId <- varShort.decode
          sourceId <- varLong.decode
          targetId <- varLong.decode
        } yield GameRolePlayPlayerFightFriendlyRequestedMessage(fightId, sourceId, targetId)

      def encode(value: GameRolePlayPlayerFightFriendlyRequestedMessage): ByteVector =
        varShort.encode(value.fightId) ++
        varLong.encode(value.sourceId) ++
        varLong.encode(value.targetId)
    }
}
