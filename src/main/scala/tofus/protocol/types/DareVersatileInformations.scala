package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class DareVersatileInformations(
  dareId: Double,
  countEntrants: Int,
  countWinners: Int
) extends ProtocolType {
  override val protocolId = 504
}

object DareVersatileInformations {
  implicit val codec: Codec[DareVersatileInformations] =
    new Codec[DareVersatileInformations] {
      def decode: Get[DareVersatileInformations] =
        for {
          dareId <- double.decode
          countEntrants <- int.decode
          countWinners <- int.decode
        } yield DareVersatileInformations(dareId, countEntrants, countWinners)

      def encode(value: DareVersatileInformations): ByteVector =
        double.encode(value.dareId) ++
        int.encode(value.countEntrants) ++
        int.encode(value.countWinners)
    }
}
