package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildKickRequestMessage(
  kickedId: Long
) extends Message(5887)

object GuildKickRequestMessage {
  implicit val codec: Codec[GuildKickRequestMessage] =
    new Codec[GuildKickRequestMessage] {
      def decode: Get[GuildKickRequestMessage] =
        for {
          kickedId <- varLong.decode
        } yield GuildKickRequestMessage(kickedId)

      def encode(value: GuildKickRequestMessage): ByteVector =
        varLong.encode(value.kickedId)
    }
}
