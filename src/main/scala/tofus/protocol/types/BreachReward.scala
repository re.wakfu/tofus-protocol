package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class BreachReward(
  id: Int,
  buyLocks: ByteVector,
  buyCriterion: String,
  remainingQty: Int,
  price: Int
) extends ProtocolType {
  override val protocolId = 559
}

object BreachReward {
  implicit val codec: Codec[BreachReward] =
    new Codec[BreachReward] {
      def decode: Get[BreachReward] =
        for {
          id <- varInt.decode
          buyLocks <- bytes(ushort).decode
          buyCriterion <- utf8(ushort).decode
          remainingQty <- varInt.decode
          price <- varInt.decode
        } yield BreachReward(id, buyLocks, buyCriterion, remainingQty, price)

      def encode(value: BreachReward): ByteVector =
        varInt.encode(value.id) ++
        bytes(ushort).encode(value.buyLocks) ++
        utf8(ushort).encode(value.buyCriterion) ++
        varInt.encode(value.remainingQty) ++
        varInt.encode(value.price)
    }
}
