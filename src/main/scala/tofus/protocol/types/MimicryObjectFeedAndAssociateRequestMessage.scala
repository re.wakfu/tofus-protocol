package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class MimicryObjectFeedAndAssociateRequestMessage(
  symbioteUID: Int,
  symbiotePos: Short,
  hostUID: Int,
  hostPos: Short,
  foodUID: Int,
  foodPos: Short,
  preview: Boolean
) extends Message(6460)

object MimicryObjectFeedAndAssociateRequestMessage {
  implicit val codec: Codec[MimicryObjectFeedAndAssociateRequestMessage] =
    new Codec[MimicryObjectFeedAndAssociateRequestMessage] {
      def decode: Get[MimicryObjectFeedAndAssociateRequestMessage] =
        for {
          symbioteUID <- varInt.decode
          symbiotePos <- ubyte.decode
          hostUID <- varInt.decode
          hostPos <- ubyte.decode
          foodUID <- varInt.decode
          foodPos <- ubyte.decode
          preview <- bool.decode
        } yield MimicryObjectFeedAndAssociateRequestMessage(symbioteUID, symbiotePos, hostUID, hostPos, foodUID, foodPos, preview)

      def encode(value: MimicryObjectFeedAndAssociateRequestMessage): ByteVector =
        varInt.encode(value.symbioteUID) ++
        ubyte.encode(value.symbiotePos) ++
        varInt.encode(value.hostUID) ++
        ubyte.encode(value.hostPos) ++
        varInt.encode(value.foodUID) ++
        ubyte.encode(value.foodPos) ++
        bool.encode(value.preview)
    }
}
