package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameFightShowFighterMessage(
  informations: GameFightFighterInformations
) extends Message(5864)

object GameFightShowFighterMessage {
  implicit val codec: Codec[GameFightShowFighterMessage] =
    new Codec[GameFightShowFighterMessage] {
      def decode: Get[GameFightShowFighterMessage] =
        for {
          informations <- Codec[GameFightFighterInformations].decode
        } yield GameFightShowFighterMessage(informations)

      def encode(value: GameFightShowFighterMessage): ByteVector =
        Codec[GameFightFighterInformations].encode(value.informations)
    }
}
