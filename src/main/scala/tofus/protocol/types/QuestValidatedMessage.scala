package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class QuestValidatedMessage(
  questId: Short
) extends Message(6097)

object QuestValidatedMessage {
  implicit val codec: Codec[QuestValidatedMessage] =
    new Codec[QuestValidatedMessage] {
      def decode: Get[QuestValidatedMessage] =
        for {
          questId <- varShort.decode
        } yield QuestValidatedMessage(questId)

      def encode(value: QuestValidatedMessage): ByteVector =
        varShort.encode(value.questId)
    }
}
