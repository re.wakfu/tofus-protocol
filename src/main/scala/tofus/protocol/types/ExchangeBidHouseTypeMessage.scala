package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeBidHouseTypeMessage(
  `type`: Int,
  follow: Boolean
) extends Message(5803)

object ExchangeBidHouseTypeMessage {
  implicit val codec: Codec[ExchangeBidHouseTypeMessage] =
    new Codec[ExchangeBidHouseTypeMessage] {
      def decode: Get[ExchangeBidHouseTypeMessage] =
        for {
          `type` <- varInt.decode
          follow <- bool.decode
        } yield ExchangeBidHouseTypeMessage(`type`, follow)

      def encode(value: ExchangeBidHouseTypeMessage): ByteVector =
        varInt.encode(value.`type`) ++
        bool.encode(value.follow)
    }
}
