package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ProtectedEntityWaitingForHelpInfo(
  timeLeftBeforeFight: Int,
  waitTimeForPlacement: Int,
  nbPositionForDefensors: Byte
) extends ProtocolType {
  override val protocolId = 186
}

object ProtectedEntityWaitingForHelpInfo {
  implicit val codec: Codec[ProtectedEntityWaitingForHelpInfo] =
    new Codec[ProtectedEntityWaitingForHelpInfo] {
      def decode: Get[ProtectedEntityWaitingForHelpInfo] =
        for {
          timeLeftBeforeFight <- int.decode
          waitTimeForPlacement <- int.decode
          nbPositionForDefensors <- byte.decode
        } yield ProtectedEntityWaitingForHelpInfo(timeLeftBeforeFight, waitTimeForPlacement, nbPositionForDefensors)

      def encode(value: ProtectedEntityWaitingForHelpInfo): ByteVector =
        int.encode(value.timeLeftBeforeFight) ++
        int.encode(value.waitTimeForPlacement) ++
        byte.encode(value.nbPositionForDefensors)
    }
}
