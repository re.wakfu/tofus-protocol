package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ShortcutBarAddErrorMessage(
  error: Byte
) extends Message(6227)

object ShortcutBarAddErrorMessage {
  implicit val codec: Codec[ShortcutBarAddErrorMessage] =
    new Codec[ShortcutBarAddErrorMessage] {
      def decode: Get[ShortcutBarAddErrorMessage] =
        for {
          error <- byte.decode
        } yield ShortcutBarAddErrorMessage(error)

      def encode(value: ShortcutBarAddErrorMessage): ByteVector =
        byte.encode(value.error)
    }
}
