package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GameRolePlayTaxCollectorInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook,
  identification: TaxCollectorStaticInformations,
  guildLevel: Short,
  taxCollectorAttack: Int
) extends GameRolePlayActorInformations {
  override val protocolId = 148
}

object GameRolePlayTaxCollectorInformations {
  implicit val codec: Codec[GameRolePlayTaxCollectorInformations] =
    new Codec[GameRolePlayTaxCollectorInformations] {
      def decode: Get[GameRolePlayTaxCollectorInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
          identification <- Codec[TaxCollectorStaticInformations].decode
          guildLevel <- ubyte.decode
          taxCollectorAttack <- int.decode
        } yield GameRolePlayTaxCollectorInformations(contextualId, disposition, look, identification, guildLevel, taxCollectorAttack)

      def encode(value: GameRolePlayTaxCollectorInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look) ++
        Codec[TaxCollectorStaticInformations].encode(value.identification) ++
        ubyte.encode(value.guildLevel) ++
        int.encode(value.taxCollectorAttack)
    }
}
