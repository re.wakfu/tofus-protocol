package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ObjectEffects(
  effects: List[ObjectEffect]
) extends ProtocolType {
  override val protocolId = 358
}

object ObjectEffects {
  implicit val codec: Codec[ObjectEffects] =
    new Codec[ObjectEffects] {
      def decode: Get[ObjectEffects] =
        for {
          effects <- list(ushort, Codec[ObjectEffect]).decode
        } yield ObjectEffects(effects)

      def encode(value: ObjectEffects): ByteVector =
        list(ushort, Codec[ObjectEffect]).encode(value.effects)
    }
}
