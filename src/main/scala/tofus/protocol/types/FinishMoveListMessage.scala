package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class FinishMoveListMessage(
  finishMoves: List[FinishMoveInformations]
) extends Message(6704)

object FinishMoveListMessage {
  implicit val codec: Codec[FinishMoveListMessage] =
    new Codec[FinishMoveListMessage] {
      def decode: Get[FinishMoveListMessage] =
        for {
          finishMoves <- list(ushort, Codec[FinishMoveInformations]).decode
        } yield FinishMoveListMessage(finishMoves)

      def encode(value: FinishMoveListMessage): ByteVector =
        list(ushort, Codec[FinishMoveInformations]).encode(value.finishMoves)
    }
}
