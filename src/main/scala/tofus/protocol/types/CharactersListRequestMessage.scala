package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class CharactersListRequestMessage(

) extends Message(150)

object CharactersListRequestMessage {
  implicit val codec: Codec[CharactersListRequestMessage] =
    Codec.const(CharactersListRequestMessage())
}
