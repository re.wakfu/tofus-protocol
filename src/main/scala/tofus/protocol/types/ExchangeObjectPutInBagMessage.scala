package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class ExchangeObjectPutInBagMessage(
  remote: Boolean,
  `object`: ObjectItem
) extends Message(6009)

object ExchangeObjectPutInBagMessage {
  implicit val codec: Codec[ExchangeObjectPutInBagMessage] =
    new Codec[ExchangeObjectPutInBagMessage] {
      def decode: Get[ExchangeObjectPutInBagMessage] =
        for {
          remote <- bool.decode
          `object` <- Codec[ObjectItem].decode
        } yield ExchangeObjectPutInBagMessage(remote, `object`)

      def encode(value: ExchangeObjectPutInBagMessage): ByteVector =
        bool.encode(value.remote) ++
        Codec[ObjectItem].encode(value.`object`)
    }
}
