package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class GuildFightPlayersEnemyRemoveMessage(
  fightId: Double,
  playerId: Long
) extends Message(5929)

object GuildFightPlayersEnemyRemoveMessage {
  implicit val codec: Codec[GuildFightPlayersEnemyRemoveMessage] =
    new Codec[GuildFightPlayersEnemyRemoveMessage] {
      def decode: Get[GuildFightPlayersEnemyRemoveMessage] =
        for {
          fightId <- double.decode
          playerId <- varLong.decode
        } yield GuildFightPlayersEnemyRemoveMessage(fightId, playerId)

      def encode(value: GuildFightPlayersEnemyRemoveMessage): ByteVector =
        double.encode(value.fightId) ++
        varLong.encode(value.playerId)
    }
}
