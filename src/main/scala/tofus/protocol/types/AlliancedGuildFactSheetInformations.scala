package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class AlliancedGuildFactSheetInformations(
  guildId: Int,
  guildName: String,
  guildLevel: Short,
  guildEmblem: GuildEmblem,
  allianceInfos: ConcreteBasicNamedAllianceInformations
) extends GuildInformations {
  override val protocolId = 422
}

object AlliancedGuildFactSheetInformations {
  implicit val codec: Codec[AlliancedGuildFactSheetInformations] =
    new Codec[AlliancedGuildFactSheetInformations] {
      def decode: Get[AlliancedGuildFactSheetInformations] =
        for {
          guildId <- varInt.decode
          guildName <- utf8(ushort).decode
          guildLevel <- ubyte.decode
          guildEmblem <- Codec[GuildEmblem].decode
          allianceInfos <- Codec[ConcreteBasicNamedAllianceInformations].decode
        } yield AlliancedGuildFactSheetInformations(guildId, guildName, guildLevel, guildEmblem, allianceInfos)

      def encode(value: AlliancedGuildFactSheetInformations): ByteVector =
        varInt.encode(value.guildId) ++
        utf8(ushort).encode(value.guildName) ++
        ubyte.encode(value.guildLevel) ++
        Codec[GuildEmblem].encode(value.guildEmblem) ++
        Codec[ConcreteBasicNamedAllianceInformations].encode(value.allianceInfos)
    }
}
