package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class HouseSellFromInsideRequestMessage(
  instanceId: Int,
  amount: Long,
  forSale: Boolean
) extends Message(5884)

object HouseSellFromInsideRequestMessage {
  implicit val codec: Codec[HouseSellFromInsideRequestMessage] =
    new Codec[HouseSellFromInsideRequestMessage] {
      def decode: Get[HouseSellFromInsideRequestMessage] =
        for {
          instanceId <- int.decode
          amount <- varLong.decode
          forSale <- bool.decode
        } yield HouseSellFromInsideRequestMessage(instanceId, amount, forSale)

      def encode(value: HouseSellFromInsideRequestMessage): ByteVector =
        int.encode(value.instanceId) ++
        varLong.encode(value.amount) ++
        bool.encode(value.forSale)
    }
}
