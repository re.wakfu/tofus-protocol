package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SelectedServerDataMessage(
  serverId: Short,
  address: String,
  ports: List[Int],
  canCreateNewCharacter: Boolean,
  ticket: ByteVector
) extends Message(42)

object SelectedServerDataMessage {
  implicit val codec: Codec[SelectedServerDataMessage] =
    new Codec[SelectedServerDataMessage] {
      def decode: Get[SelectedServerDataMessage] =
        for {
          serverId <- varShort.decode
          address <- utf8(ushort).decode
          ports <- list(ushort, int).decode
          canCreateNewCharacter <- bool.decode
          ticket <- bytes(varInt).decode
        } yield SelectedServerDataMessage(serverId, address, ports, canCreateNewCharacter, ticket)

      def encode(value: SelectedServerDataMessage): ByteVector =
        varShort.encode(value.serverId) ++
        utf8(ushort).encode(value.address) ++
        list(ushort, int).encode(value.ports) ++
        bool.encode(value.canCreateNewCharacter) ++
        bytes(varInt).encode(value.ticket)
    }
}
