package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait PartyMemberInformations extends CharacterBaseInformations

final case class ConcretePartyMemberInformations(
  id: Long,
  name: String,
  level: Short,
  entityLook: EntityLook,
  breed: Byte,
  sex: Boolean,
  lifePoints: Int,
  maxLifePoints: Int,
  prospecting: Short,
  regenRate: Short,
  initiative: Short,
  alignmentSide: Byte,
  worldX: Short,
  worldY: Short,
  mapId: Double,
  subAreaId: Short,
  status: PlayerStatus,
  entities: List[PartyEntityBaseInformation]
) extends PartyMemberInformations {
  override val protocolId = 90
}

object ConcretePartyMemberInformations {
  implicit val codec: Codec[ConcretePartyMemberInformations] =  
    new Codec[ConcretePartyMemberInformations] {
      def decode: Get[ConcretePartyMemberInformations] =
        for {
          id <- varLong.decode
          name <- utf8(ushort).decode
          level <- varShort.decode
          entityLook <- Codec[EntityLook].decode
          breed <- byte.decode
          sex <- bool.decode
          lifePoints <- varInt.decode
          maxLifePoints <- varInt.decode
          prospecting <- varShort.decode
          regenRate <- ubyte.decode
          initiative <- varShort.decode
          alignmentSide <- byte.decode
          worldX <- short.decode
          worldY <- short.decode
          mapId <- double.decode
          subAreaId <- varShort.decode
          status <- Codec[PlayerStatus].decode
          entities <- list(ushort, Codec[PartyEntityBaseInformation]).decode
        } yield ConcretePartyMemberInformations(id, name, level, entityLook, breed, sex, lifePoints, maxLifePoints, prospecting, regenRate, initiative, alignmentSide, worldX, worldY, mapId, subAreaId, status, entities)

      def encode(value: ConcretePartyMemberInformations): ByteVector =
        varLong.encode(value.id) ++
        utf8(ushort).encode(value.name) ++
        varShort.encode(value.level) ++
        Codec[EntityLook].encode(value.entityLook) ++
        byte.encode(value.breed) ++
        bool.encode(value.sex) ++
        varInt.encode(value.lifePoints) ++
        varInt.encode(value.maxLifePoints) ++
        varShort.encode(value.prospecting) ++
        ubyte.encode(value.regenRate) ++
        varShort.encode(value.initiative) ++
        byte.encode(value.alignmentSide) ++
        short.encode(value.worldX) ++
        short.encode(value.worldY) ++
        double.encode(value.mapId) ++
        varShort.encode(value.subAreaId) ++
        Codec[PlayerStatus].encode(value.status) ++
        list(ushort, Codec[PartyEntityBaseInformation]).encode(value.entities)
    }
}

object PartyMemberInformations {
  implicit val codec: Codec[PartyMemberInformations] =
    new Codec[PartyMemberInformations] {
      def decode: Get[PartyMemberInformations] =
        ushort.decode.flatMap {
          case 90 => Codec[ConcretePartyMemberInformations].decode
          case 391 => Codec[PartyMemberArenaInformations].decode
        }

      def encode(value: PartyMemberInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcretePartyMemberInformations => Codec[ConcretePartyMemberInformations].encode(i)
          case i: PartyMemberArenaInformations => Codec[PartyMemberArenaInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
