package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait GameContextActorInformations extends GameContextActorPositionInformations

final case class ConcreteGameContextActorInformations(
  contextualId: Double,
  disposition: EntityDispositionInformations,
  look: EntityLook
) extends GameContextActorInformations {
  override val protocolId = 150
}

object ConcreteGameContextActorInformations {
  implicit val codec: Codec[ConcreteGameContextActorInformations] =  
    new Codec[ConcreteGameContextActorInformations] {
      def decode: Get[ConcreteGameContextActorInformations] =
        for {
          contextualId <- double.decode
          disposition <- Codec[EntityDispositionInformations].decode
          look <- Codec[EntityLook].decode
        } yield ConcreteGameContextActorInformations(contextualId, disposition, look)

      def encode(value: ConcreteGameContextActorInformations): ByteVector =
        double.encode(value.contextualId) ++
        Codec[EntityDispositionInformations].encode(value.disposition) ++
        Codec[EntityLook].encode(value.look)
    }
}

object GameContextActorInformations {
  implicit val codec: Codec[GameContextActorInformations] =
    new Codec[GameContextActorInformations] {
      def decode: Get[GameContextActorInformations] =
        ushort.decode.flatMap {
          case 150 => Codec[ConcreteGameContextActorInformations].decode
          case 46 => Codec[GameFightCharacterInformations].decode
          case 50 => Codec[GameFightMutantInformations].decode
          case 158 => Codec[ConcreteGameFightFighterNamedInformations].decode
          case 203 => Codec[GameFightMonsterWithAlignmentInformations].decode
          case 29 => Codec[ConcreteGameFightMonsterInformations].decode
          case 48 => Codec[GameFightTaxCollectorInformations].decode
          case 151 => Codec[ConcreteGameFightAIInformations].decode
          case 551 => Codec[GameFightEntityInformation].decode
          case 143 => Codec[ConcreteGameFightFighterInformations].decode
          case 464 => Codec[GameRolePlayGroupMonsterWaveInformations].decode
          case 160 => Codec[ConcreteGameRolePlayGroupMonsterInformations].decode
          case 467 => Codec[GameRolePlayPortalInformations].decode
          case 383 => Codec[GameRolePlayNpcWithQuestInformations].decode
          case 156 => Codec[ConcreteGameRolePlayNpcInformations].decode
          case 3 => Codec[GameRolePlayMutantInformations].decode
          case 36 => Codec[GameRolePlayCharacterInformations].decode
          case 159 => Codec[ConcreteGameRolePlayHumanoidInformations].decode
          case 180 => Codec[GameRolePlayMountInformations].decode
          case 129 => Codec[GameRolePlayMerchantInformations].decode
          case 154 => Codec[ConcreteGameRolePlayNamedActorInformations].decode
          case 161 => Codec[GameRolePlayPrismInformations].decode
          case 471 => Codec[GameRolePlayTreasureHintInformations].decode
          case 148 => Codec[GameRolePlayTaxCollectorInformations].decode
          case 141 => Codec[ConcreteGameRolePlayActorInformations].decode
        }

      def encode(value: GameContextActorInformations): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteGameContextActorInformations => Codec[ConcreteGameContextActorInformations].encode(i)
          case i: GameFightCharacterInformations => Codec[GameFightCharacterInformations].encode(i)
          case i: GameFightMutantInformations => Codec[GameFightMutantInformations].encode(i)
          case i: ConcreteGameFightFighterNamedInformations => Codec[ConcreteGameFightFighterNamedInformations].encode(i)
          case i: GameFightMonsterWithAlignmentInformations => Codec[GameFightMonsterWithAlignmentInformations].encode(i)
          case i: ConcreteGameFightMonsterInformations => Codec[ConcreteGameFightMonsterInformations].encode(i)
          case i: GameFightTaxCollectorInformations => Codec[GameFightTaxCollectorInformations].encode(i)
          case i: ConcreteGameFightAIInformations => Codec[ConcreteGameFightAIInformations].encode(i)
          case i: GameFightEntityInformation => Codec[GameFightEntityInformation].encode(i)
          case i: ConcreteGameFightFighterInformations => Codec[ConcreteGameFightFighterInformations].encode(i)
          case i: GameRolePlayGroupMonsterWaveInformations => Codec[GameRolePlayGroupMonsterWaveInformations].encode(i)
          case i: ConcreteGameRolePlayGroupMonsterInformations => Codec[ConcreteGameRolePlayGroupMonsterInformations].encode(i)
          case i: GameRolePlayPortalInformations => Codec[GameRolePlayPortalInformations].encode(i)
          case i: GameRolePlayNpcWithQuestInformations => Codec[GameRolePlayNpcWithQuestInformations].encode(i)
          case i: ConcreteGameRolePlayNpcInformations => Codec[ConcreteGameRolePlayNpcInformations].encode(i)
          case i: GameRolePlayMutantInformations => Codec[GameRolePlayMutantInformations].encode(i)
          case i: GameRolePlayCharacterInformations => Codec[GameRolePlayCharacterInformations].encode(i)
          case i: ConcreteGameRolePlayHumanoidInformations => Codec[ConcreteGameRolePlayHumanoidInformations].encode(i)
          case i: GameRolePlayMountInformations => Codec[GameRolePlayMountInformations].encode(i)
          case i: GameRolePlayMerchantInformations => Codec[GameRolePlayMerchantInformations].encode(i)
          case i: ConcreteGameRolePlayNamedActorInformations => Codec[ConcreteGameRolePlayNamedActorInformations].encode(i)
          case i: GameRolePlayPrismInformations => Codec[GameRolePlayPrismInformations].encode(i)
          case i: GameRolePlayTreasureHintInformations => Codec[GameRolePlayTreasureHintInformations].encode(i)
          case i: GameRolePlayTaxCollectorInformations => Codec[GameRolePlayTaxCollectorInformations].encode(i)
          case i: ConcreteGameRolePlayActorInformations => Codec[ConcreteGameRolePlayActorInformations].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
