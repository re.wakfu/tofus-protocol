package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class TreasureHuntStepFollowDirectionToPOI(
  direction: Byte,
  poiLabelId: Short
) extends TreasureHuntStep {
  override val protocolId = 461
}

object TreasureHuntStepFollowDirectionToPOI {
  implicit val codec: Codec[TreasureHuntStepFollowDirectionToPOI] =
    new Codec[TreasureHuntStepFollowDirectionToPOI] {
      def decode: Get[TreasureHuntStepFollowDirectionToPOI] =
        for {
          direction <- byte.decode
          poiLabelId <- varShort.decode
        } yield TreasureHuntStepFollowDirectionToPOI(direction, poiLabelId)

      def encode(value: TreasureHuntStepFollowDirectionToPOI): ByteVector =
        byte.encode(value.direction) ++
        varShort.encode(value.poiLabelId)
    }
}
