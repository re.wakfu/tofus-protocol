package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

final case class SymbioticObjectAssociatedMessage(
  hostUID: Int
) extends Message(6527)

object SymbioticObjectAssociatedMessage {
  implicit val codec: Codec[SymbioticObjectAssociatedMessage] =
    new Codec[SymbioticObjectAssociatedMessage] {
      def decode: Get[SymbioticObjectAssociatedMessage] =
        for {
          hostUID <- varInt.decode
        } yield SymbioticObjectAssociatedMessage(hostUID)

      def encode(value: SymbioticObjectAssociatedMessage): ByteVector =
        varInt.encode(value.hostUID)
    }
}
