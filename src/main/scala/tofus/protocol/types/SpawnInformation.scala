package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait SpawnInformation extends ProtocolType

final case class ConcreteSpawnInformation(

) extends SpawnInformation {
  override val protocolId = 575
}

object ConcreteSpawnInformation {
  implicit val codec: Codec[ConcreteSpawnInformation] =  
    Codec.const(ConcreteSpawnInformation())
}

object SpawnInformation {
  implicit val codec: Codec[SpawnInformation] =
    new Codec[SpawnInformation] {
      def decode: Get[SpawnInformation] =
        ushort.decode.flatMap {
          case 575 => Codec[ConcreteSpawnInformation].decode
          case 573 => Codec[SpawnCompanionInformation].decode
          case 581 => Codec[SpawnScaledMonsterInformation].decode
          case 572 => Codec[SpawnMonsterInformation].decode
          case 582 => Codec[ConcreteBaseSpawnMonsterInformation].decode
          case 574 => Codec[SpawnCharacterInformation].decode
        }

      def encode(value: SpawnInformation): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteSpawnInformation => Codec[ConcreteSpawnInformation].encode(i)
          case i: SpawnCompanionInformation => Codec[SpawnCompanionInformation].encode(i)
          case i: SpawnScaledMonsterInformation => Codec[SpawnScaledMonsterInformation].encode(i)
          case i: SpawnMonsterInformation => Codec[SpawnMonsterInformation].encode(i)
          case i: ConcreteBaseSpawnMonsterInformation => Codec[ConcreteBaseSpawnMonsterInformation].encode(i)
          case i: SpawnCharacterInformation => Codec[SpawnCharacterInformation].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
