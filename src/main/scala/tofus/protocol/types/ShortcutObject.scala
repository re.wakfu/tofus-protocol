package tofus.protocol.types

import cats.syntax.all._
import tofus.protocol._
import tofus.protocol.Codec._
import scodec.bits.ByteVector

trait ShortcutObject extends Shortcut

final case class ConcreteShortcutObject(
  slot: Byte
) extends ShortcutObject {
  override val protocolId = 367
}

object ConcreteShortcutObject {
  implicit val codec: Codec[ConcreteShortcutObject] =  
    new Codec[ConcreteShortcutObject] {
      def decode: Get[ConcreteShortcutObject] =
        for {
          slot <- byte.decode
        } yield ConcreteShortcutObject(slot)

      def encode(value: ConcreteShortcutObject): ByteVector =
        byte.encode(value.slot)
    }
}

object ShortcutObject {
  implicit val codec: Codec[ShortcutObject] =
    new Codec[ShortcutObject] {
      def decode: Get[ShortcutObject] =
        ushort.decode.flatMap {
          case 367 => Codec[ConcreteShortcutObject].decode
          case 370 => Codec[ShortcutObjectPreset].decode
          case 371 => Codec[ShortcutObjectItem].decode
          case 492 => Codec[ShortcutObjectIdolsPreset].decode
        }

      def encode(value: ShortcutObject): scodec.bits.ByteVector = {
        val encoded = value match {
          case i: ConcreteShortcutObject => Codec[ConcreteShortcutObject].encode(i)
          case i: ShortcutObjectPreset => Codec[ShortcutObjectPreset].encode(i)
          case i: ShortcutObjectItem => Codec[ShortcutObjectItem].encode(i)
          case i: ShortcutObjectIdolsPreset => Codec[ShortcutObjectIdolsPreset].encode(i)
        }
        ushort.encode(value.protocolId) ++ encoded
      }
    }
    
}
