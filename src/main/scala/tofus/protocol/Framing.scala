package tofus.protocol

import fs2.{Stream, Pull, Pipe}
import scodec.bits.ByteVector
import cats.effect.Sync
import tofus.protocol.types.{Message, Receiver}

object Framing {
  def decodeClient[F[_]: Sync]: Pipe[F, Byte, Message] =
    decodeFrames[F](4L).andThen(decodeMessages[F])

  def decodeServer[F[_]: Sync]: Pipe[F, Byte, Message] =
    decodeFrames[F](0L).andThen(decodeMessages[F])

  def decodeMessages[F[_]: Sync]: Pipe[F, Payload, Message] =
    _.evalMap { payload =>
      Sync[F].fromOption(
        Receiver.receive(payload.id)(payload.body).map(_._1),
        new RuntimeException(s"Invalid message payload: $payload")
      )
    }

  def decodeFrames[F[_]](skipHeaderBytes: Long): Pipe[F, Byte, Payload] = {
    def go(stream: Stream[F, Byte]): Pull[F, Payload, Unit] =
      stream.pull.unconsN(2).flatMap {
        case Some((chunk, rem)) =>
          val header      = chunk.toByteBuffer.getShort() & 0xFFFF
          val messageId   = header >> 2
          val lengthBytes = header & 0x3
          rem.drop(skipHeaderBytes).pull.unconsN(lengthBytes).flatMap {
            case Some((chunk, rem)) if chunk.size == 0 => go(rem)
            case Some((chunk, rem)) =>
              val len = chunk.size match {
                case 1 => chunk.toByteBuffer.get() & 0xFF
                case 2 => chunk.toByteBuffer.getShort() & 0xFFFF
                case 3 => ((chunk(0) & 0xFF) << 16) + ((chunk(1) & 0xFF) << 8) + (chunk(2) & 0xFF)
              }
              rem.pull.unconsN(len).flatMap {
                case Some((chunk, rem)) => Pull.output1(Payload(messageId, chunk.toByteVector)) >> go(rem)
                case None               => Pull.done
              }
            case None =>
              Pull.done
          }
        case None => Pull.done
      }

    go(_).stream
  }

  def encode[A <: Message: Codec](message: A): ByteVector = {
    val payload = Codec[A].encode(message)
    val lengthBytes = payload.size match {
      case size if size > 65535 => 3
      case size if size > 255   => 2
      case size if size > 0     => 1
      case _                    => 0
    }
    val header = message.messageTypeId << 2 | lengthBytes
    val lengthTag = lengthBytes match {
      case 3 =>
        ByteVector.fromByte((payload.size >> 16 & 255).toByte) ++
          ByteVector.fromShort((payload.size & 65535).toShort)
      case 2 => ByteVector.fromShort(payload.size.toShort)
      case 1 => ByteVector.fromByte(payload.size.toByte)
      case _ => ByteVector.empty
    }
    (ByteVector.fromShort(header.toShort) ++ lengthTag ++ payload)
  }

  final case class Payload(id: Int, body: ByteVector)
}
