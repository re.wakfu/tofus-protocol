package tofus.protocol

import java.nio.ByteBuffer

import cats.implicits._
import cats.{InvariantMonoidal, Monad, StackSafeMonad}
import scodec.bits.ByteVector
import scodec.interop.cats._

trait Encoder[A] {
  def encode(a: A): ByteVector
}

trait Decoder[A] {
  def decode: Get[A]
}

trait Codec[A] extends Encoder[A] with Decoder[A]

trait Get[+A] {
  def apply(byteVector: ByteVector): Option[(A, ByteVector)]
}

object Get {
  implicit val monad: Monad[Get] =
    new StackSafeMonad[Get] {
      override def flatMap[A, B](fa: Get[A])(f: A => Get[B]): Get[B] =
        vec =>
          for {
            res1 <- fa(vec)
            res2 <- f(res1._1)(res1._2)
          } yield res2

      override def pure[A](x: A): Get[A] = vec => (x, vec).some
    }
}

object Codec extends CodecInstances {
  def apply[A: Codec]: Codec[A] = implicitly[Codec[A]]

  def read[A: Codec](vec: ByteVector): Option[A] =
    Codec[A].decode(vec).map(_._1)

  def const[A](value: A): Codec[A] =
    instance(vec => (value, vec).some, _ => ByteVector.empty)

  def instance[A](get: Get[A], pack: A => ByteVector): Codec[A] =
    new Codec[A] {
      override def decode: Get[A]           = get
      override def encode(a: A): ByteVector = pack(a)
    }

  val varLong: Codec[Long] =
    instance(Varint.decodeToLong(_).some, Varint.encodeLong)

  val varInt: Codec[Int] =
    instance(Varint.decodeToInt(_).some, Varint.encodeInt)

  val varShort: Codec[Short] =
    instance(Varint.decodeToShort(_).some, Varint.encodeShort)

  val byte: Codec[Byte] =
    bytesC(1).imap(_.toByte())(ByteVector.fromByte)

  val ubyte: Codec[Short] =
    byte.imap(i => (i & 0xFF).toShort)(_.toByte)

  val short: Codec[Short] =
    bytesC(2).imap(_.toShort())(ByteVector.fromShort(_))

  val ushort: Codec[Int] =
    short.imap(_ & 0xFFFF)(_.toShort)

  val int: Codec[Int] =
    bytesC(4).imap(_.toInt())(ByteVector.fromInt(_))

  val uint: Codec[Long] =
    int.imap(_ & 0xFFFFFFFFL)(_.toInt)

  val long: Codec[Long] =
    bytesC(8).imap(_.toLong())(ByteVector.fromLong(_))

  val bool: Codec[Boolean] =
    byte.imap(_ != 0)(if (_) 1 else 0)

  val float: Codec[Float] =
    bytesC(4).imap(_.toByteBuffer.getFloat()) { f =>
      val bb = ByteBuffer.allocate(4)
      bb.putFloat(f).flip()
      ByteVector.view(bb)
    }

  val double: Codec[Double] =
    bytesC(8).imap(_.toByteBuffer.getDouble()) { f =>
      val bb = ByteBuffer.allocate(8)
      bb.putDouble(f).flip()
      ByteVector.view(bb)
    }

  def list[I: Integral, A](prefix: Codec[I], codec: Codec[A]): Codec[List[A]] =
    new Codec[List[A]] {
      override def decode: Get[List[A]] =
        prefix.decode.flatMap { i => codec.decode.replicateA(Integral[I].toInt(i)) }

      override def encode(l: List[A]): ByteVector =
        prefix.encode(Integral[I].fromInt(l.length)) ++ l.foldMap(codec.encode)
    }

  def listC[A](size: Int, codec: Codec[A]): Codec[List[A]] =
    instance(codec.decode.replicateA(size), _.foldMap(codec.encode))

  def utf8[I: Integral](prefix: Codec[I]): Codec[String] =
    bytes(prefix).imap(b => new String(b.toArray))(str => ByteVector.view(str.getBytes))

  def utf8C(size: Int): Codec[String] =
    bytesC(size).imap(b => new String(b.toArray))(str => ByteVector.view(str.getBytes))

  def bytes[I: Integral](prefix: Codec[I]): Codec[ByteVector] =
    new Codec[ByteVector] {
      override def decode: Get[ByteVector] =
        prefix.decode.flatMap { i => vec => vec.splitAt(Integral[I].toLong(i)).some }

      override def encode(vec: ByteVector): ByteVector =
        prefix.encode(Integral[I].fromInt(vec.length.toInt)) ++ vec
    }

  def bytesC(size: Int): Codec[ByteVector] =
    instance(_.splitAt(size).some, identity)
  
  def remaining: Codec[ByteVector] =
    instance((_, ByteVector.empty).some, identity)
}

trait CodecInstances {
  implicit val invariant: InvariantMonoidal[Codec] =
    new InvariantMonoidal[Codec] {
      override val unit: Codec[Unit] =
        new Codec[Unit] {
          override def decode: Get[Unit]           = _ => ((), ByteVector.empty).some
          override def encode(a: Unit): ByteVector = ByteVector.empty
        }

      override def product[A, B](fa: Codec[A], fb: Codec[B]): Codec[(A, B)] =
        new Codec[(A, B)] {
          override def decode: Get[(A, B)]           = fa.decode.product(fb.decode)
          override def encode(a: (A, B)): ByteVector = fa.encode(a._1) ++ fb.encode(a._2)
        }

      override def imap[A, B](fa: Codec[A])(f: A => B)(g: B => A): Codec[B] =
        new Codec[B] {
          override def decode: Get[B]           = fa.decode.map(f)
          override def encode(a: B): ByteVector = fa.encode(g(a))
        }
    }
}
